#ifndef MATRICE_H
#define MATRICE_H

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

typedef struct matrice_s {
	int p; // Lignes
	int q; // Colonnes
	float** m;
} matrice;

static matrice matrice_nulle = {
	0,
	0,
	NULL
};

matrice creer_matrice(int p, int q);
matrice creer_matrice_user();
matrice creer_matrice_rand(int p, int q);
matrice creer_matrice_hilbert(int p, int q);
matrice creer_matrice_stochastique(int p, int q);
matrice creer_matrice_circulante(int p, int q);
matrice matrice_copie(matrice m);
void matrice_affichage(matrice m);
void matrice_free(matrice m);

#endif
