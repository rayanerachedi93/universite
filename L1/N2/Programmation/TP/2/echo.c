#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]) {
	while (*(++argv) != NULL) {
		while (**argv != 0) {
			putchar(**argv);
			++(*argv);
		}

		if (*(argv + 1) != NULL) {
			putchar(' ');
		}
	}
	putchar('\n');
	return 0;
}
