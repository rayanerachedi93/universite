#include <stdlib.h>
#include <stdio.h>

extern char **environ;

int main() {
	while (*(++environ) != NULL) {
		while (**environ != '\0') {
			putchar(**environ);
			(*environ)++;
		}
		putchar('\n');
	}
}
