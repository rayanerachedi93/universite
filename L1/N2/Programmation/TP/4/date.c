#include "date.h"
#include <stdio.h> /* pour la fonction printf */
#include <stdlib.h> /* pour malloc, free */

/** Fonction qui crée et initialise une date champ par champ */
/** et renvoie l'adresse de la date ainsi créée et initialisée */
date * creer_date(unsigned int j, unsigned int m, unsigned int a){
	date * res = malloc(sizeof(date));
	if (res == NULL) {
		perror("Échec allocation date");
		exit(2);
	}
	res->jour = j;
	res->mois = m;
	res->annee = a;
	return res;
}

/** Fonction qui affiche les valeurs des champs de la date passée en argument */
void afficher_date(const date * d) {
	printf("%u/%u/%u", d->jour, d->mois, d->annee);
}

/** Fonction qui renvoie -1 si d1 est antérieure à d2, */
/**          qui renvoie 1 si d1 est postérieure à d2, */
/**          qui renvoie 0 si les dates d1 et d2 sont identiques */
int comparer_date(const date * d1, const date * d2) {
	if(d1->annee < d2->annee)
		return -1;
	if (d1->annee > d2->annee)
		return 1;
	if (d1->mois < d2->mois)
		return -1;
	if (d1->mois > d2->mois)
		return 1;
	if (d1->jour < d2->jour)
		return -1;
	if (d1->jour > d2->jour)
		return 1;
	return 0;
}

/** Fonction qui libère tout l'espace occupé sur le tas par la date pointée par d */
void detruire_date (date * d) {
	free(d);
}
