#include "opera.h"
#include "date.h"
#include "individu.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define TAILLE_MAX 50 /* pour la fonction initialiser_tab_operas_fichier */

/** Fonction qui alloue de l'espace sur le tas pour une structure opéra */
/* et renvoie l'adresse de l'opéra ainsi créé */
opera * creer_opera (void) {
		opera * res = malloc(sizeof(opera));
		if (res == NULL) {
			perror("Échec allocation opéra");
			exit(2);
		}
		res->titre = NULL;
		res->date_creation = NULL;
		res->ville_creation = NULL;
		res->compositeur = NULL;
		return res;
}
/** Fonction qui initialise les champs de l'opéra pointé par op aux valeurs des autres arguments */
void initialiser_opera (opera * op, const char * titre, const date * d, const char * ville, const individu * compositeur) {
	op->titre = malloc(strlen(titre) + 1);
	strcpy(op->titre, titre);

	op->date_creation = malloc(sizeof(date));
	op->date_creation = creer_date(d->jour, d->mois, d->annee);

	op->ville_creation = malloc(strlen(ville) + 1);
	strcpy(op->ville_creation, ville);

	op->compositeur = malloc(sizeof(individu));
	op->compositeur = creer_init_individu(compositeur->nom, compositeur->prenom, compositeur->naissance);
}

/** Fonction qui libère toute la mémoire occupée par l'opéra pointé par op */
void detruire_opera (opera * op) {
	if (op->titre != NULL)
		free(op->titre);
	if (op->ville_creation != NULL)
		free(op->ville_creation);
	if (op->date_creation)
		detruire_date(op->date_creation);
	if (op->compositeur != NULL)
		detruire_individu(op->compositeur);
	free(op);
}

/** Fonction qui affiche le prénom et le nom du compositeur (sa date de naissance), */
/* le titre (la date et la ville de création) de l'opéra op */
void afficher_opera (const opera * op) {
	afficher_individu(op->compositeur);
	printf(", %s (", op->titre);
	afficher_date(op->date_creation);
	printf(", %s)", op->ville_creation);
	printf("\n");
}

/** Fonction qui échange les adresses d'opéra pointées par les deux arguments */
/* N.B. CETTE FONCTION N'EST PAS UTLISÉE DANS LE TP N°4 */
void echanger_opera (opera **op1, opera **op2) {
	opera *temp = *op1;
	*op1 = *op2;
	*op2 = temp;
}
