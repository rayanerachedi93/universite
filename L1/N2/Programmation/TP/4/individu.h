#ifndef INDIVIDU_H
#define INDIVIDU_H

#include "date.h"

typedef struct individu_s {
		char * nom;
		char * prenom;
		date * naissance;
} individu;

/** Fonction qui crée et initialise un individu champ par champ */
/** et renvoie l'adresse de l'individu ainsi créé et initialisé */
individu * creer_init_individu (const char *, const char *, const date *);

/** Fonction qui libère toute la mémoire occupée par l'individu pointé par ind */
void detruire_individu (individu *);

/** Fonction qui calcule le nombre d'années révolues de l'individu pointé par à la date d pointée par d */
/** Par exemple, un individu né le 7 avril 1950 a 49 ans révolus le 7 avril 2000 et en a 50 le 8 avril 2000 */
int age_revolu (const individu *, const date *);

/* Fonction qui affiche le prénom, le nom et la date de naissance de l'individu pointé par l'argument */
void afficher_individu (const individu *);

#endif
