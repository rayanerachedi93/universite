#ifndef DATE_H
#define DATE_H

typedef struct date_s {
	unsigned int jour;
	unsigned int mois;
	unsigned int annee;
} date;

/** Fonction qui crée et initialise une date champ par champ */
/** et renvoie l'adresse de la date ainsi créée et initialisée */
date * creer_date(unsigned int j, unsigned int m, unsigned int a);

/** Fonction qui affiche les valeurs des champs de la date passée en argument */
void afficher_date(const date * d);

/** Fonction qui renvoie -1 si d1 est antérieure à d2, */
/**          qui renvoie 1 si d1 est postérieure à d2, */
/**          qui renvoie 0 si les dates d1 et d2 sont identiques */
int comparer_date(const date * d1, const date * d2);

/** Fonction qui libère tout l'espace occupé sur le tas par la date pointée par d */
void detruire_date (date * d);

#endif
