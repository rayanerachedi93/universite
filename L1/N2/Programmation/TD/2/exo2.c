#include <stdlib.h>
#include <stdio.h>

#define N 51

typedef unsigned long long p_int;

p_int fibo_rec(p_int n);
p_int Crec(p_int n);
p_int fibo_mem(p_int n, p_int tab[]);
p_int fibo_rec_ter(p_int n, p_int n_1, p_int n_2);
p_int fibo_iter(p_int n);

int main() {
    p_int tab[N] = {0};
    
    printf("%llu\n", fibo_iter(50));
    printf("%llu\n", fibo_rec_ter(50, 0, 1));
    printf("%llu\n", fibo_mem(50, tab));
//    printf("%llu\n", fibo_rec(50));
    
    return EXIT_SUCCESS;
}

// Crec(0) = 1
// Crec(1) = 1
// Crec(2) = 3
// Crec(3) = 5
// Crec(4) = 9
// Crec(5) = 15
// Crec(n) = Crec(n - 1) + Crec(n - 2) + 1
p_int fibo_rec(p_int n) {
    if (n <= 1)
        return n;
    else
        return fibo_rec(n - 1) + fibo_rec(n - 2);
}

p_int Crec(p_int n) {
    if (n <= 1)
        return 1;
    else
        return Crec(n - 1) + Crec(n - 2) + 1;
}

// Cmem(n) = Cmem(n - 1) + 2
p_int fibo_mem(p_int n, p_int tab[]) {
    if (n <= 1)
        return n;
    else if (tab[n] != 0)
        return tab[n];
    else {
        tab[n] = fibo_mem(n - 1, tab) + fibo_mem(n - 2, tab);
        return tab[n];
    }
}

// Crec_ter(n) = n
p_int fibo_rec_ter(p_int n, p_int n_1, p_int n_2) {
    if (n == 0)
        return n_1;
    else
        return fibo_rec_ter(n - 1, n_2, n_1 + n_2);
}

p_int fibo_iter(p_int n) {
    p_int n_1 = 0;
    p_int n_2 = 1;
    p_int t = 0;
    
iter:
    t = n_2;
    n_2 += n_1;
    n_1 = t;
    
    if (n-- > 1)
        goto iter;
    
    return n_1;
}
