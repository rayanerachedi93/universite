#include <stdlib.h>
#include <stdio.h>

typedef struct maillon {
	float valeur;
	struct maillon* suivant;
} maillon;

typedef struct {
	maillon* debut;
	unsigned taille;
} liste;

void reverse(liste* l);
maillon* reverse_maillon(maillon* actuel, int n);

int main() {
	maillon m3 = { -1.2, NULL };
	maillon m2 = { 2.7, &m3 };
	maillon m1 = { 1.3, &m2 };
	liste l = { &m1, 3 };

	maillon* cur = l.debut; 
	for (int x = l.taille; x > 0 && cur != NULL; x--) {
		printf("%f\n", cur->valeur);
		cur = cur->suivant;
	}

	reverse(&l);

	cur = l.debut; 
	for (int x = l.taille; x > 0 && cur != NULL; x--) {
		printf("%f\n", cur->valeur);
		cur = cur->suivant;
	}

	return EXIT_SUCCESS;
}

void reverse(liste* l) {
	l->debut = reverse_maillon(l->debut, l->taille);
}

maillon* reverse_maillon(maillon* actuel, int n) {
	if (actuel->suivant == NULL || n <= 1)
		return actuel;

	maillon* new_head = reverse_maillon(actuel->suivant, n - 1);

	maillon** s = &actuel->suivant;
	maillon* s_val = actuel->suivant;
	maillon** ss = &actuel->suivant->suivant;
	maillon* ss_val = actuel->suivant->suivant;

	*s = ss_val;
	*ss = s_val;

	return new_head;
}
