// Ici, on utilise les matrices 1D : m[i][j] = m[i * n + j]

#include <stdlib.h>
#include <stdio.h>

float* multiplication_rec(float* A, float* B, int n);
float* strassen(float* A, float* B, int n);
void addition(float* R, float* A, float* B, int n);
void soustraction(float* R, float* A, float* B, int n);
void dcomp(float* M, int n, float* A, float* B, float* C, float* D);
void comp(float* M, int n, float* A, float* B, float* C, float* D);

int main() {
	int n = 2;
	float* A = malloc(n * n * sizeof(float));
	float* B = malloc(n * n * sizeof(float));
	A[0] = 1.;
	A[1] = 2.;
	A[2] = 3.;
	A[3] = 4.;
	B[0] = 4.;
	B[1] = 3.;
	B[2] = 2.;
	B[3] = 1.;
	float* R;

	R = strassen(A, B, n);

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			printf("\t%.2f", R[i * n + j]);
		}
		printf("\n");
	}

	free(A);
	free(B);
	free(R);
}

void dcomp(float* M, int n, float* A, float* B, float* C, float* D) {
	int i, j;

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			A[i * n + j] = M[i * 2 * n + j];
			B[i * n + j] = M[i * 2 * n + j + n];
			C[i * n + j] = M[(i + n) * 2 * n + j];
			D[i * n + j] = M[(i + n) * 2 * n + j + n];
		}
	}
}

void comp(float* M, int n, float* A, float* B, float* C, float* D) {
	int i, j;

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			M[i * 2 * n + j] = A[i * n + j];
			M[i * 2 * n + j + n] = B[i * n + j];
			M[(i + n) * 2 * n + j] = C[i * n + j];
			M[(i + n) * 2 * n + j + n] = D[i * n + j];
		}
	}
}

void addition(float* R, float* A, float* B, int n) {
	for (int i = 0; i < n * n; i++) {
		R[i] = A[i] + B[i];
	}
}

void soustraction(float* R, float* A, float* B, int n) {
	for (int i = 0; i < n * n; i++) {
		R[i] = A[i] - B[i];
	}
}

float* multiplication_rec(float* A, float* B, int n) {
	float* R = malloc(n * n * sizeof(float));

	if (n == 1)
		R[0] = A[0] * B[0];
	else {
		int m = n / 2;
		float* a = malloc(m * m * sizeof(float));
		float* b = malloc(m * m * sizeof(float));
		float* c = malloc(m * m * sizeof(float));
		float* d = malloc(m * m * sizeof(float));
		float* x = malloc(m * m * sizeof(float));
		float* y = malloc(m * m * sizeof(float));
		float* z = malloc(m * m * sizeof(float));
		float* t = malloc(m * m * sizeof(float));
		float* ax;
		float* bz;
		float* ay;
		float* bt;
		float* cx;
		float* dz;
		float* cy;
		float* dt;
		float* R1 = malloc(m * m * sizeof(float));
		float* R2 = malloc(m * m * sizeof(float));
		float* R3 = malloc(m * m * sizeof(float));
		float* R4 = malloc(m * m * sizeof(float));

		dcomp(A, m, a, b, c, d);
		dcomp(B, m, x, y, z, t);

		ax = multiplication_rec(a, x, m);
		bz = multiplication_rec(b, z, m);
		ay = multiplication_rec(a, y, m);
		bt = multiplication_rec(b, t, m);
		cx = multiplication_rec(c, x, m);
		dz = multiplication_rec(d, z, m);
		cy = multiplication_rec(c, y, m);
		dt = multiplication_rec(d, t, m);

		addition(R1, ax, bz, m);
		addition(R2, ay, bt, m);
		addition(R3, cx, dz, m);
		addition(R4, cy, dt, m);

		comp(R, m, R1, R2, R3, R4);

		free(a);
		free(b);
		free(c);
		free(d);
		free(x);
		free(y);
		free(z);
		free(t);
		free(ax);
		free(bz);
		free(ay);
		free(bt);
		free(cx);
		free(dz);
		free(cy);
		free(dt);
		free(R1);
		free(R2);
		free(R3);
		free(R4);
	}

	return R;
}

float* strassen(float* A, float* B, int n) {
	float* R = malloc(n * n * sizeof(float));

	if (n == 1)
		R[0] = A[0] * B[0];
	else {
		int m = n / 2;
		float* a = malloc(m * m * sizeof(float));
		float* b = malloc(m * m * sizeof(float));
		float* c = malloc(m * m * sizeof(float));
		float* d = malloc(m * m * sizeof(float));
		float* x = malloc(m * m * sizeof(float));
		float* y = malloc(m * m * sizeof(float));
		float* z = malloc(m * m * sizeof(float));
		float* t = malloc(m * m * sizeof(float));
		float* P1;
		float* P2;
		float* P3;
		float* P4;
		float* P5;
		float* P6;
		float* P7;
		float* R1 = malloc(m * m * sizeof(float));
		float* R2 = malloc(m * m * sizeof(float));
		float* R3 = malloc(m * m * sizeof(float));
		float* R4 = malloc(m * m * sizeof(float));

		dcomp(A, m, a, b, c, d);
		dcomp(B, m, x, y, z, t);

		// P1
		float* p1t1 = malloc(m * m * sizeof(float));
		float* p1t2 = malloc(m * m * sizeof(float));
		addition(p1t1, a, d, m);
		addition(p1t2, x, t, m);
		P1 = strassen(p1t1, p1t2, m);
		free(p1t1);
		free(p1t2);

		// P2
		float* p2t1 = malloc(m * m * sizeof(float));
		addition(p2t1, c, d, m);
		P2 = strassen(p2t1, x, m);
		free(p2t1);

		// P3
		float* p3t1 = malloc(m * m * sizeof(float));
		soustraction(p3t1, y, t, m);
		P3 = strassen(a, p3t1, m);
		free(p3t1);

		// P4
		float* p4t1 = malloc(m * m * sizeof(float));
		soustraction(p4t1, z, x, m);
		P4 = strassen(d, p4t1, m);
		free(p4t1);

		// P5
		float* p5t1 = malloc(m * m * sizeof(float));
		addition(p5t1, a, b, m);
		P5 = strassen(p5t1, t, m);
		free(p5t1);

		// P6
		float* p6t1 = malloc(m * m * sizeof(float));
		float* p6t2 = malloc(m * m * sizeof(float));
		soustraction(p6t1, c, a, m);
		addition(p6t2, x, y, m);
		P6 = strassen(p6t1, p6t2, m);
		free(p6t1);
		free(p6t2);

		// P7
		float* p7t1 = malloc(m * m * sizeof(float));
		float* p7t2 = malloc(m * m * sizeof(float));
		soustraction(p7t1, b, d, m);
		addition(p7t2, z, t, m);
		P7 = strassen(p7t1, p7t2, m);
		free(p7t1);
		free(p7t2);

		// R1
		addition(R1, P1, P4, m);
		soustraction(R1, R1, P5, m);
		addition(R1, R1, P7, m);

		// R2
		addition(R2, P3, P5, m);

		// R3
		addition(R3, P2, P4, m);

		// R4
		soustraction(R4, P1, P2, m);
		addition(R4, R4, P3, m);
		addition(R4, R4, P6, m);

		comp(R, m, R1, R2, R3, R4);

		free(a);
		free(b);
		free(c);
		free(d);
		free(x);
		free(y);
		free(z);
		free(t);
		free(P1);
		free(P2);
		free(P3);
		free(P4);
		free(P5);
		free(P6);
		free(P7);
		free(R1);
		free(R2);
		free(R3);
		free(R4);
	}

	return R;
}
