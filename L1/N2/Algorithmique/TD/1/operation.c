#include "operation.h"
#include "matrice.h"

matrice operation_dilatation(float x, matrice m) {
	int i, j;
	matrice r = matrice_copie(m);

	for (i = 0; i < m.p; i++) {
		for (j = 0; j < m.q; j++) {
			r.m[i][j] *= x;
		}
	}

	return r;
}

matrice operation_addition(matrice m1, matrice m2) {
	int i, j;

	if (m1.p != m2.p || m1.q != m2.q) {
		return MATRICE_NULLE;
	}

	matrice m = matrice_copie(m1);

	for (i = 0; i < m.p; i++) {
		for (j = 0; j < m.q; j++) {
			m.m[i][j] += m2.m[i][j];
		}
	}

	return m;
}

matrice operation_evaluation(matrice m, matrice u) {
	int i, j;

	if (m.q != u.p || u.q != 0) {
		return MATRICE_NULLE;
	}

	matrice r = matrice_copie(m);

	for (i = 0; i < m.p; i++) {
		for (j = 0; j < m.q; j++) {
			r.m[i][j] *= u.m[j][0];
		}
	}

	return r;
}

matrice operation_produit(matrice m1, matrice m2) {
	int i, j, k;

	if (m1.q != m2.p) {
		return MATRICE_NULLE;
	}

	matrice m = creer_matrice(m1.p, m2.q);

	for (i = 0; i < m.p; i++) {
		for (j = 0; j < m.q; j++) {
			m.m[i][j] = 0;

			for (k = 0; k < m1.q; k++) {
				m.m[i][j] += m1.m[i][k]*m2.m[k][j];
			}
		}
	}

	return m;
}
