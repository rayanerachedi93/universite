#ifndef PIVOT_H
#define PIVOT_H

#include "matrice.h"

int pivot_recherche_pivot(matrice* m, int lp, int cp);
void pivot_addition(matrice* m, int l1, float x, int l2);
void pivot_multiplication(matrice* m, int l1, float x);
void pivot_echange(matrice* m, int l1, int l2);
void pivot_annule(matrice* m, int c);
void pivot_gauss_simple(matrice* m);
void pivot_gauss_complet(matrice* m);
void pivot_solution(matrice* m);

#endif
