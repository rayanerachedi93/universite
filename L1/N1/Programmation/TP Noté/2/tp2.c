#include <stdio.h> // printf & scanf
#include <stdlib.h> // EXIT_SUCCESS

// On définit une taille maximale au tableau.
#define MAX 256

/**
 * Demande à l'utilisateur les éléments d'un tableau d'entier puis recherche et
 * affiche le maximum et le minimum.
 */
int main() {
	// i   : itérateur
	// len : taille du tableau
	// max : maximum du tableau
	// min : minimum du tableau
	// tab : notre tableau de taille maximale MAX
	int i, len, max, min, tab[MAX];

	do {
		// On demande à l'utilisateur la taille du tableau d'entier.
		printf("Donnez la taille de votre tableau d'entiers (max : 256) : ");
		scanf("%d", &len);
	// On s'assure que la longueur du tableau n'excède pas la taille maximale
	// et est supérieure à 0.
	} while(len > MAX || len <= 0);

	// On parcours notre tableau et on demande à l'utilisateur d'entrer chaque
	// élément.
	for (i = 0; i < len; i++) {
		printf("Entrez l'entier n°%d de votre tableau tab[%d] = ", i + 1, i);
		scanf("%d", &tab[i]);
	}

	// On définit une variable pour stocker la valeur maximale du tableau.
	// On la définit à la première valeur du tableau (cette dernière est soit
	// maximale ou inférieure à la valeur maximale).
	max = tab[0];

	// On parcours le tableau et on modifie max si on trouve une valeur plus
	// grande que celle déjà trouvée.
	// On commence depuis l'index 1, vu qu'on sait déjà qu'il n'est pas
	// supérieur à lui-même.
	for (i = 1; i < len; i++) {
		if (tab[i] > max) {
			max = tab[i];
		}
	}

	// On définit une variable pour stocker la valeur minimale du tableau.
	// On la définit à la première valeur du tableau (cette dernière est soit
	// minimale ou supérieure à la valeur minimale).
	min = tab[0];

	// On parcours le tableau et on modifie max si on trouve une valeur plus
	// petite que celle déjà trouvée.
	// On commence depuis l'index 1, vu qu'on sait déjà qu'il n'est pas
	// inférieur à lui-même.
	for (i = 1; i < len; i++) {
		if (tab[i] < min) {
			min = tab[i];
		}
	}

	// On affiche le maximum et le minimum du tableau d'entier.
	printf("\n");
	printf("L'entier le plus grand du tableau est %d.\n", max);
	printf("L'entier le plus petit du tableau est %d.\n", min);

	// Tout s'est bien passé, on renvoit EXIT_SUCCESS.
	return EXIT_SUCCESS;
}
