// Binôme : Lyes Saadi (12107246) & Achraf Taha Chadly (12112688)

// On inclue les bibliothèques standard.
#include <stdlib.h> // EXIT_SUCCESS
#include <stdio.h> // printf() & scanf()

// Fonction principale.
/**
 * Demande un entier strictement positif et affiche tous les multiples de 3 et
 * de 11 qui lui sont inférieur.
 */
int main() {
	// On déclare i et n. i est un itérateur pour la boucle for.
	int i, n = 0;

	// On demande à l'utilisateur la valeur de n jusqu'à ce que n soit
	// strictement supérieur à 0.
	while (n <= 0) {
		printf("Entrez un entier supérieur strictement à 0 : ");
		// On récupère la valeur entière depuis l'entrée standard.
		scanf("%d", &n);
	}
	
	// On teste chaque valeur entre 3 et n qui est un multiple de 3 ou de 11.
	// On commence à 3 puiqu'il n'existe pas de multiple de 3 ou de 11
	// inférieur à 3 et supérieur à 0.
	for (i = 3; i <= n; i++) {
		// On calcule le reste de la division de i par 3 et par 11, si l'un
		// des restes est égal à 0, alors, i divise soit 3, soit 11, et i
		// est un multiple de 3 ou de 11.
		if (i % 3 == 0 || i % 11 == 0) {
			// On affiche i.
			printf("%d\n", i);
		}
	}

	// On retourne EXIT_SUCCESS. Le programme a terminé avec succès.
	return EXIT_SUCCESS;
}
