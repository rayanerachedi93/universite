#include <stdlib.h>
#include <stdio.h>
#include <math.h>

void recursive();

void recursive() {
	printf("%p\n", recursive);
	recursive();
}

int one() {
	return 1;
}

int three = 3;

int factorielle(int n) {
	if (n < 2) {
		return 1;
	}
	return n * factorielle(n - 1);
}

int multiply(int n, ...) {
	return 1;
}

int main() {
	int two = one;

	printf("%d %p %p\n", one, two, &three);

	printf("%i\n", multiply(2, 0, 5, 8));

	//recursive();

	return EXIT_SUCCESS;
}
