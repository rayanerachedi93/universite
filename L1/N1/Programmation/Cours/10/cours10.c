#include <stdio.h>

enum Meteo {
	PLUIE,
	SOLEIL,
	NUAGE
};

typedef enum Meteo Meteo;
typedef struct Environnement Env;

struct Environnement {
	Meteo meteo;
	short int temperature;
	short int humidite;
};

int main() {
	Env env = {
		.meteo = PLUIE,
		.temperature = 18,
		.humidite = 90,
	};

	char c;

	do {
		printf("Voulez-vous un bulletin météo (Y/N) : ");
		scanf(" %c", &c);
	} while (c != 'Y' && c != 'N');

	if (c == 'N') goto sortie;

	switch (env.meteo) {
		case PLUIE:
			printf("Il pleut et il fait %d° avec %d%% d'humidité\n", env.temperature, env.humidite);
			break;
		case SOLEIL:
			printf("Il est ensolleilé et il fait %d° avec %d%% d'humidité\n", env.temperature, env.humidite);
			break;
		case NUAGE:
			printf("Il y a des nuages et il fait %d° avec %d%% d'humidité\n", env.temperature, env.humidite);
			break;
	}

sortie:
	return 0;
}
