#include <stdlib.h>
#include <stdio.h>

int main() {
	int un = 1;
	int un_1 = 1;
	int n = 2;
	int m, tmp;

	printf("n = ");
	scanf("%d", &m);

	while (n < m) {
		tmp = un;
		un += un_1;
		un_1 = tmp;
		n++;
	}

	printf("u_%d = %d\n", n, un);

	return EXIT_SUCCESS;
}
