#include <stdlib.h>
#include <stdio.h>

int main() {
	int p;

	printf("p = ");
	scanf("%d", &p);

	for (int i = 2; i < p; i++) {
		if (p % i == 0) {
			printf("p = %d n'est pas premier. En effet, %d divise p.\n", p, i);

			return EXIT_SUCCESS;;
		}
	}

	printf("p = %d est premier.\n", p);

	return EXIT_SUCCESS;
}
