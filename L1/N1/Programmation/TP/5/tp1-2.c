#include <stdlib.h>
#include <stdio.h>

int main() {
	int mots = 1;
	int last = 0, i;
	int len = 0;
	char phrase[500] = " Bonjour,  mon petit.  ";

	if (phrase[0] == ' ')
		last = 1;

	do {
		len++;
		if ((phrase[len] == ' ' || phrase[len] == '\0') && phrase[len - 1] != ' ') {
			mots++;
			for (i = last; i < len; i++) {
				printf("%c", phrase[i]);
			}
			printf(" a %d charactères.\n", len - last);
		}
		if (phrase[len] == ' ')
			last = len + 1;
	} while (phrase[len] != '\0');

	return EXIT_SUCCESS;
}
