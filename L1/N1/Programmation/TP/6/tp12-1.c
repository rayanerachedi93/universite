#include <stdlib.h>
#include <stdio.h>

#define TRUE 1
#define FALSE 0

#define MAX 500

void bubble_sort(int *tab, int t, int taille);
int recherche_dichotomie(int recherche, int *tab, int taille);

int main() {
	int t;
	do {
		printf("La taille du tableau = ");
		scanf("%d", &t);
	} while (t > MAX);

	int tab[MAX];

	for (int i = 0; i < t; i++) {
		printf("t[%d] = ", i);
		scanf("%d", &tab[i]);
	}

	bubble_sort(tab, 0, t);

	printf("---------------------------\n");

	for (int i = 0; i < t; i++) {
		printf("t[%d] = %d\n", i, tab[i]);
	}

	int e;

	printf("Élément souhaité : ");
	scanf("%d", &e);

	int position = recherche_dichotomie(e, tab, t - 1);

	if (!position)
		printf("L'élément %d n'est pas dans le tableau.\n", e);
	else
		printf("L'élément %d est dans la position : %d\n", e, position);


	return EXIT_SUCCESS;
}

void bubble_sort(int *tab, int debut, int fin) {
	int perm = FALSE;

	for (int i = fin - 1; i > debut; i--) {
		if (tab[i] < tab[i - 1]) {
			int temp = tab[i];
			tab[i] = tab[i - 1];
			tab[i - 1] = temp;
			perm = TRUE;

			for (int i = 0; i < fin; i++) {
				printf("%d ", tab[i]);
			}
			printf("\n");
		}
	}

	if (perm && debut != fin - 1)
		bubble_sort(tab, debut + 1, fin);
}

/*
int recherche_dichotomie(int recherche, int *tab, int taille, int padding) {
	if (taille == 1 && tab[0] != recherche)
		return -1;
	else
		return padding;

	if (recherche <= tab[taille / 2]) {
		return recherche_dichotomie(recherche, tab, taille / 2 + 1, padding);
	} else {
		return recherche_dichotomie(recherche, &tab[taille / 2 + 1], taille - (taille / 2 + 1), padding + taille / 2 + 1);
	}
}
*/

int recherche_dichotomie(int recherche, int *tab, int taille) {
	int debut = 0;
	int fin = taille - 1;
	int i;

	while (tab[debut] != recherche && tab[fin] != recherche && fin - debut > 1) {
		i = (debut + fin) / 2;
		if (tab[i] > recherche)
			fin = i;
		else if (tab[i] < recherche)
			debut = i;
	}

	if (tab[debut] == recherche)
		return debut + 1;
	if (tab[fin] == recherche)
		return fin + 1;
	else
		return 0;
}
