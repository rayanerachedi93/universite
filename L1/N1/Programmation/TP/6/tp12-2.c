#include <stdlib.h>
#include <stdio.h>

int factorielle(int x) {
	if (x > 1)
		return x * factorielle(x - 1);
	if (x == 0)
		return 1;
	return x;
}

int main() {
	int n;
	double e = 0;

	printf("Entrez la précision n = ");
	scanf("%d", &n);

	for (int i = 0; i <= n; i++)
	{
		e += 1. / factorielle(i);
	}

	printf("e = %lf\n", e);
}
