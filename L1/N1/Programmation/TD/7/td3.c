#include <stdlib.h>
#include <stdio.h>

int main() {
	int tab[256];
	int somme = 0;
	int t, i;

	printf("Combien d'éléments (t < 256) a votre suite : ");
	scanf("%d", &t);

	for (i = 0; i < t; i++) {
		printf("Entrez l'élément n°%d de la suite = ", i + 1);
		scanf("%d", &tab[i]);
	}

	for (i = 0; i < t; i++) {
		somme += tab[i];
	}

	printf("La somme des éléments de la suite est = %d\n", somme);

	return EXIT_SUCCESS;
}
