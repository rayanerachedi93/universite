#include <stdlib.h>
#include <stdio.h>

int main() {
	int tab[5] = {5, 2, 4, 3, 0};

	for (int i = 0; i < 5; i++) {
		printf("tab[%d] = %d\n", i, tab[i]);
	}

	return EXIT_SUCCESS;
}
