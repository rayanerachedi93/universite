#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main() {
	char chaine1[] = "UwU";
	char chaine2[] = " Hewwo Onii-chan :3 !";
	char resultat1[30];
	char resultat2[30];

	printf("La longueur de « %s » est de %lu.\n", chaine1, strlen(chaine1));
	printf("La longueur de « %s » est de %lu.\n", chaine2, strlen(chaine2));

	strcpy(resultat1, chaine1);
	printf("%s\n", resultat1);

	strcpy(resultat2, chaine1);
	strcat(resultat2, chaine2);
	printf("%s\n", resultat2);

	if (strcmp(chaine1, chaine2) == 0)
		printf("« %s » = « %s »\n", chaine1, chaine2);
	else if (strcmp(chaine1, chaine2) > 0)
		printf("« %s » > « %s »\n", chaine1, chaine2);
	else
		printf("« %s » < « %s »\n", chaine1, chaine2);

	if (strcmp(chaine1, resultat1) == 0)
		printf("« %s » = « %s »\n", chaine1, resultat1);
	else if (strcmp(chaine1, resultat1) > 0)
		printf("« %s » > « %s »\n", chaine1, resultat1);
	else
		printf("« %s » < « %s »\n", chaine1, resultat1);

	return EXIT_SUCCESS;
}
