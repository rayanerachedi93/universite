#include <stdlib.h>
#include <stdio.h>

int main() {
	int i, j;

	for (i = 0; i < 5; i++) {
		printf("i = %d\n", i);
	}

	printf("i vaut %d après l'exécution de la boucle.\n", i);

	for (i = 0; i < 5; i++) {
		printf("%d ", i);
	}

	printf("\n");

	for (i = 1; i < 5; i++) {
		printf("%d ", i);
	}

	printf("\n");

	for (i = 1; i <= 5; i++) {
		printf("%d ", i);
	}

	printf("\n");

	for (i = 1; i <= 5; i += 2) {
		printf("%d ", i);
	}

	printf("\n");

	for (i = 0; i <= 2; i += 1) {
		printf("(%d, %d) ", i, i);
	}

	printf("\n");

	for (i = 2; i <= 3; i++) {
		for (j = 0; j <= 2; j++) {
			printf("%d ", j);
		}

		for (j = 0; j <= i; j++) {
			printf("%d ", j);
		}
		printf("\n");
	}

	for (i = 0; i <= 2; i++) {
		for (j = 0; j <= 2; j++) {
			printf("(%d, %d) ", i, j);
		}
	}

	printf("\n");

	return EXIT_SUCCESS;
}
