#include <stdlib.h>
#include <stdio.h>

int main() {
	int n, i;
	int somme = 0;

	printf("n = ");
	scanf("%d", &n);

	for (i = 1; i <= n; i++) {
		somme += i;
	}

	printf("somme = %d\n", somme);

	return EXIT_SUCCESS;
}
