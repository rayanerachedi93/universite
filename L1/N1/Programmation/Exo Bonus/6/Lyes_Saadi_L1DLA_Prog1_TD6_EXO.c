/** Algorithme
 *
 * On déclare et initialise deux variables n et f, respectivement à -1 et 1.
 * On demande à l'utilisateur l'entier positif n jusqu'à ce qu'il nous donne une
 * valeur valide >= 0 (d'où l'initialisation de n à -1, une valeur invalide).
 *
 * La factorielle est définie par le produit de tous les nombres de 1 à n (ou 1
 * si n = 0). Donc, on peut donner n! comme étant égal à :
 * n! = n(n - 1)(n - 2)(n - 3)...1
 * On reproduit cette somme grâce à une boucle while qui multiplie n par une
 * variable qui contient le résultat, f, puis on décrémente n, et on répète
 * jusqu'à ce que n soit égal à 1.
 *
 * Le résultat est donc aussi valide si n = 0 ou n = 1, vu qu'aucune itération
 * n'a lieu, et f est initialisé à 1.
 *
 * On affiche enfin le résultat de n!, et on retourne EXIT_SUCCESS.
 */
#include <stdlib.h> // EXIT_SUCCESS
#include <stdio.h> // printf() & scanf()

/**
 * Demande à l'utilisateur un nombre n et calcule son factoriel.
 */
int main() {
	// On déclare et initialise les variables n et f.
	// On initialise n à -1 pour pouvoir demander à l'utilisateur n au moins
	// une fois.
	// On initialise f à 1 pour pouvoir faire le produit sans annuler la valeur
	// et pour pouvoir renvoyer une valeur valide si n = 0 ou n = 1.
	int n = -1, f = 1;

	// On demande à l'utilisateur une valeur n jusqu'à ce que ce dernier soit un
	// entier positif.
	while (n < 0) {
		printf("Entrez un entier n positif : ");
		scanf("%d", &n);
	}

	// On fait le produit de tous les nombres entre n et 1. Pour cela, on
	// multiplie, puis on le décrémente, tant que n est strictement supérieur à
	// 1. Si n = 0 ou n = 1, la valeur reste valide vu que f est initialisé à 1.
	while (n > 1) {
		f *= n;
		n--;
	}

	// On affiche le résultat de n! à l'utilisateur.
	printf("n! = %d\n", f);

	// Tout s'est bien passé, on retourne EXIT_SUCCESS.
	return EXIT_SUCCESS;
}
