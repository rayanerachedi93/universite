// `fprintf()`, `printf()` et `stderr`.
#include <stdio.h>
// `atoi()`.
#include <stdlib.h>

/*
Les arguments additionnels dans la signature de la fonction main correspondent
aux arguments avec lesquels est exécuté le programme et sont fourni
automatiquement par le système d'exploitation.
*/

/**
 * Permutter trois variables.
 **/
int main(int argc, char const *argv[]) 
{
    // Vérification du nombre d'arguments :
    // Pour que l'exécution soit valide, il faut que le nombre d'arguments soit
    // de 4, étant donné que l'exécutable est considéré lui-même comme un
    // argument.
    if (argc != 4) {
        // On affiche dans ce cas là une erreur dans `stderr` et on arrête le
        // programme.
        fprintf(stderr, "Vous devez fournir 3 arguments.\n");
        return 1; // Le retour 1 indique l'échec du programme.
    }

    // Déclaration des variables
    // atoi() converti les chaînes de caractères en entiers. En effet, argv
    // est un tableau de chaînes de caractères.
    int x = atoi(argv[1]);
    int y = atoi(argv[2]);
    int z = atoi(argv[3]);

    // Retour utilisateur
    // `printf()` formatte et affiche le résultat. `%d` indique à `printf()`
    // d'écrire à cet endroit là le contenu de la variable x, et que le contenu
    // de cette variable est un nombre décimal. `\n` est un caractère spécial
    // indiquant un retour à la ligne.
    printf("x = %d\n", x);
    printf("y = %d\n", y);
    printf("z = %d\n\n", z);

    // Déclaration de la variable temporaire et son initialisation avec la
    // valeur de x.
    int temp = x;

    // Permutation de valeurs
    x = y;
    y = z;
    z = temp;

    // Affichage des résultats
    printf("x = %d\n", x);
    printf("y = %d\n", y);
    printf("z = %d\n", z);

    return 0; // Le retour 0 indique le succès du programme.
}