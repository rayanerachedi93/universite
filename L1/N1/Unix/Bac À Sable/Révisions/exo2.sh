#!/bin/sh

vider=1
corbeille=$HOME/.corbeille

if [ $# -eq 0 ]; then
	ls -lA $corbeille
	exit 0
fi

case $1 in
	--help | -h)
		echo heeeelp
		exit 0
		;;
	--vider | -v)
		vider=0
		shift
esac

if ! [ -d $corbeille ]; then
	if ! mkdir -p $corbeille; then
		exit 1
	fi
fi

if ! [ -x $corbeille ] || ! [ -w $corbeille ] || ! [ -r $corbeille ]; then
	exit 2
fi

for fichier; do
	mv $fichier $corbeille
done

if $vider; then
	rm -rf $corbeille/*
fi
