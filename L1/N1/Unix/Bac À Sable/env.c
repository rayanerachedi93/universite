#include <stdlib.h>
#include <stdio.h>

extern char **environ;

int main() {
	// Il existe deux façons de récupérer l'environnement.

	for (; *environ; ++environ) {
		printf("%s\n", *environ);
	}

	printf("%s\n", getenv("LANG"));

	return EXIT_SUCCESS;
}
