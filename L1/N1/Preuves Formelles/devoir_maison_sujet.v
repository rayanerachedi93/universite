Require Import Unicode.Utf8.
Require Import Nat.
Require Import PeanoNat.
Require Import Unicode.Utf8.
Require Import Reals.
Require Import Lra.
Require Import Lia.
Require Export RealField.

(** * DEVOIR MAISON *)

(** Devoir à rendre avant le 12 novembre 23h59 *)

(** IMPORTANT !! Commencez par remplir les champs ci-dessous pour
    signer la déclaration suivante :

    "Je déclare qu'il s'agit de mon propre travail."

    NOM PRENOM : Saadi Lyes

    NUMERO ÉTUDIANT : 12107246
*)

(*
  Petit message :D :

  Votre DM, est d'une difficulté !!! Parmis tout ceux qui sont dans le groupe
  Discord qu'on a créé pour se coordonner, on est, si je ne me trompe pas que ~5
  à avoir à peine réussi à faire la moitié des exos seulement :P !

  Et perso j'ai commencé le Mardi pendant les vacances ^_^' !

  C'était juste pour vous dire : Courage, vous allez voir des horreurs.

  Sérieusement, je trouve Coq très intéressant, et y a de très fortes chances
  que j'y retourne après que tout ça soit finit. Mais, je trouve que ça aurait
  mérité plus d'heures ou plus d'explications, parce qu'avec les injections et
  sur les suites dans la Partie 4, on se retrouve un peu perdu...

  La pente de difficulté était beaucoup trop raide, et la plupart se sont
  retrouvé perdu (sans même parler de ce DM, où là, tout le monde est perdu !).

  C'est aussi un peu dommage de faire tout ça pour ne plus jamais en faire.
*)


(** ** Partie 0 : mettre les choses au clair sur ce devoir. *)

(** **** Exercice : Prouver le théorème suivant. 12 points sur 20.
    Indices : utiliser la tactique [intros] puis la tactique [assumption],
    séparées par le caractère '.' comme ceci :
    [intros. assumption.]
*)

Theorem imp_refl: ∀ P : Prop, P → P.
Proof.
  intros. assumption.
Qed.


(** Maintenant on va pouvoir travailler entre adultes :

    - Le but de ce devoir (et en général de ce module) est d'apprendre
    beaucoup de mathématiques et d'informatique, pas d'avoir une note.
    - En particuler, ça ne sert à rien de copier-coller des solutions
    qui ne seraient pas de vous (en plus ce serait malhonnête).
    - Par contre, demander à un camarade de vous débloquer si vraiment
    vous l'êtes ou de vous expliquer quelque chose est permis, et même
    encouragé, mais dans ce cas, citez-le (ça ne vous enlèvera pas de
    points).
    - Vous avez des niveaux très divers à ce jour et ce devoir est
    vraiment très long. Faîtes ce que vous pouvez (vous pouvez bien
    sûr choisir). Tout ce que vous faîtes là sera très formateur et
    utile pour la suite de vos études.  
    - Amusez-vous !  
*)

(** Il y a 4 partie indépendantes : - Première partie : Composition
des fonctions, - Deuxième partie : La représentation binaire des
entiers, - Troisième partie : Fonctions et inverses, - Quatrième
partie : Suites.

    Du bon usage de votre temps :

    - Avant de faire quoi que ce soit, il vaut mieux être sûr d'être
      au clair sur Logique1.v et Naturels.v
    - La première partie est sans doute la plus simple.
    - La deuxième partie traite des entiers strictements positifs en
      binaire.  Outre le fait de vous faire travailler sur l'écriture
      en binaire des entiers, elle vous fait travailler sur un type
      inductif à trois constructeurs, donc les preuves par induction
      ont 3 étapes.
    - Avant de faire la suite, il vaut mieux être au clair sur les
      quantificateurs (fichier Quantificateurs.v)
    - La troisième partie fait travailler sur les concepts
      extrêmements importants de surjection, injection et
      bijection. Il y a des choses un peu dures et profondes à la fin
      (axiome du choix et relation fonctionnelle).
    - Ensuite, il vaut mieux être au clair sur les réels :
      CorpsOrdonné.v et début de Suites.v, UL_sequence devrait être
      bien compris.
    - Alors, seulement vous pouvez vous attaquer à la quatrième
      partie, qui est sans doute la plus formatrice.

    Au niveau de la notation, encore une fois ça ne devrait pas être ce qui vous
    motive, mais voilà :
    - Pour chaque partie, si vous avez fait une partie des exercices, vous avez
      un point (on décidera au cas par cas si ce que vous avez fait le
      justifie).
    - Si vous avez réussi des choses difficiles dans une partie ça rapporte un
      point supplémentaire.
    - Les choses très bien faites peuvent rapporter des points supplémentaires.
    - La mise en forme soignée des preuves aussi :
      - les lignes devraient être assez courtes (toujours moins de 80
        caractères) ;
      - pas de ligne vide, pas de Check ou Search dans la preuve ;
      - pas de preuve qui fait 20 lignes si elle peut en faire 4 ;
      - des commentaires de temps en temps quand ça aide à comprendre votre
        preuve ;
      - utiliser les "bullets" [-] [+] [*] [--] etc pour séparer les sous-cas et
        les indenter (par exemple avec 2 espaces) ;
      - bref rendre quelque chose qui est joli.
*)

(** Dans tout le devoir, vous pouvez admettre un exercice avec
    [Admitted] et l'utiliser dans le suivant. Les différentes parties
    sont indépendantes. Vous n'avez pas besoin de la fonctions
    [Search] pour la première, deuxième et troisième partie, sauf
    quand c'est demandé explicitement à la fin de la deuxième partie.
*)

(** ** Première partie : Composition des fonctions *)

Section Composition.

Definition croissante (f : nat -> nat) := forall n m, (n <= m) -> (f n <=  f m).

Definition decroissante (f : nat -> nat) := forall n m, (n <= m) -> (f n >=  f m).

(** On introduit une notation pour la composition des fonctions. C'est
comme une définition mais en moins fort, pas besoin de faire [unfold],
coq le fera tout seul.*)

Notation "g \o f" := (fun x => g (f x)) (at level 70).

Lemma croi_rond_croi (f g : nat -> nat) :
  (croissante f) /\ (croissante g) -> croissante (g \o f).
Proof.
  (* On introduit nos hypothèses *)
  intros HFetG.
  unfold croissante.
  intros n m.
  intros Hninfm.
  (* Dans HFetG on a deux hypthèses distinctes *)
  destruct HFetG as [HF HG].
  (* Vu qu'on a f croissante, alors n <= m => f(n) <= f(m) *)
  apply HF in Hninfm.
  (* Vu qu'on a g croissante, alors f(n) <= f(m) => g(f(n)) <= g(f(m)) *)
  apply HG in Hninfm.
  (* On a donc prouvé que n <= m => g\of(n) <= g\of(m) si f et g croissantes *)
  assumption.
Qed.


Lemma dec_rond_dec (f g : nat -> nat) :
  (decroissante f) /\ (decroissante g) -> croissante (g \o f).
Proof.
  (* On introduit nos hypothèses *)
  intros HFetG.
  unfold croissante.
  intros n m Hninfm.
  (* On sépare HFetG en deux hypothèses *)
  destruct HFetG as [HF HG].
  (* f décroissante, donc n <= m => f(n) >= g(m) *)
  apply HF in Hninfm.
  (* g décroissante, donc f(n) >= g(m) => g(f(n)) <= g(f(m)) *)
  apply HG in Hninfm.
  (* On a prouvé notre but *)
  assumption.
Qed.



Lemma dec_rond_croi (f g : nat -> nat) :
  (decroissante f) /\ (croissante g) -> decroissante (g \o f).
Proof.
  (* On introduit nos hypothèses *)
  intros HFetG.
  unfold decroissante.
  intros n m Hninfm.
  (* On sépare les deux hypothèses de HFetG *)
  destruct HFetG as [HF HG].
  (* f est décroissante, donc n <= m => f(n) >= f(m) *)
  apply HF in Hninfm.
  (* g est croissante, donc f(n) >= f(m) => g(f(m)) >= g(f(m)) *)
  apply HG in Hninfm.
  (* On a prouvé notre but *)
  assumption.
Qed.


Definition constante (f : nat -> nat) :=
forall n m, (f n = f m).

(** Pour prouver le lemme suivant, utiliser l'antisymétrie de la relation <= 
    et le fait qu'on a toujours n <= m ou m <= n. *)
Check Nat.le_antisymm.
Check Nat.le_ge_cases.
Lemma croi_et_dec_const (f : nat -> nat) :
  (croissante f) /\ (decroissante f) -> constante f.
Proof.
  (* On introduit nos hypothèses *)
  intros HCetD.
  unfold constante.
  intros n m.
  (* On sépare les deux hypothèses *)
  destruct HCetD as [HC HD].
  (* On sait que f(n) = f(m) => f(n) <= f(m) ∧ f(n) >= f(m) *)
  apply Nat.le_antisymm.
  (* On étudie le cas de f(n) <= f(m) *)
  -
    (* On va étudier chaque cas pour n <= m ou n >= m *)
    destruct (Nat.le_ge_cases n m).
    +
      (* n <= m => f(n) <= f(m) car f est croissante *)
      apply HC in H.
      assumption.
    +
      (* n >= m => f(n) <= f(m) car f est décroissante *)
      apply HD in H.
      assumption.
  (* On étudie le cas de f(n) >= f(m) *)
  -
    (* On va étudier chaque cas pour n <= m ou n >= m *)
    destruct (Nat.le_ge_cases n m).
    +
      (* n <= m => f(n) >= f(m) car f est décroissante *)
      apply HD in H.
      assumption.
    +
      (* n >= m => f(n) >= f(m) car f est croissante *)
      apply HC in H.
      assumption.
Qed.


End Composition.

(** ** Deuxième partie : La représentation binaire des entiers *)
Module Positive.
(** 
    Vous êtes (trop) habitué(e)s à la représentation décimale des
    nombres.  Cette représentation utilise la base 10 et les 10
    chiffres décimaux que sont
    0, 1, 2, 3, 4, 5, 6, 7, 8 et 9.
    Un entier qui s'écrit en décimal 345 vaut 3 centaines, 4 dizaines
    et 5 unités, soit

    3 * 10^2 + 4 * 10^1 + 5 * 1.

    En binaire, ou base 2, il n'y a que deux chiffres : 0 et 1, qu'on
    appelle aussi _bits_ pour _binary digits_ c'est-à-dire chiffres
    binaires.

    Le nombre écrit en binaire 1101 vaut 1 * 2^3 + 1 * 2^2 + 0 * 2^1 +
    1, c'est-à-dire 13 en décimal.

    Pour plus d'informations, voir
    https://fr.wikipedia.org/wiki/Syst%C3%A8me_binaire

    Dans ce sujet, nous donnons une définition alternative des entiers
    strictement positifs en binaire. Un des intérêts est que
    l'écriture des nombres est beaucoup plus courte. Comparer 1101
    avec S(S(S(S(S(S(S(S(S(S(S(S(S O))))))))))))). Les évaluations
    faites par coq seront aussi beaucoup plus rapides.  *)

(** Un élément de type positive est soit :
    - le nombre 1
    - le résultat d'ajouter un 0 à la fin d'un positive
    - le résultat d'ajouter un 1 à la fin d'un positive
*)

Inductive positive : Set :=
  | xI : positive -> positive (* mettre un 1 à la fin *)
  | xO : positive -> positive (* mettre un 0 à la fin *)
  | xH : positive. (* l'entier 1 *)

(* Notations pour que ce soit beaucoup plus pratique. *)
Declare Scope positive.
Notation "1" := xH
  (at level 6) : positive.
Notation "p ~ 1" := (xI p)
 (at level 7, left associativity, format "p '~' '1'") : positive.
Notation "p ~ 0" := (xO p)
 (at level 7, left associativity, format "p '~' '0'") : positive.
Local Open Scope positive.

Check 1~1~0. (* en décimal, vaut 6 *)
Check 1~1~0~1. (* en décimal, vaut 13 *)

(** **** Exercice : convertir un entier binaire positif en nat

    Rendre la définition suivante correcte (changer les deux derniers cas).
    Les tests suivants (sous la forme de théorèmes) devraient être
    automatiquement prouvés. *)

(* Parce que le niveau 6 de priorité affectée à la notation "1"
   ci-dessus est très forte, coq ne veut plus entendre parler du
   naturel 1, c'est celui de positive qui prend le dessus, donc on
   utilise S. *)

Fixpoint nat_of_positive (p : positive) :=
  match p with
    1 => S 0
  | p' ~ 0 => (nat_of_positive p') * 2
  | p' ~ 1 => S ((nat_of_positive p') * 2)
  end.

(*
Expliquation de l'algorithme :

Prenons 13, c'est 1101 en binaire.

Donc 13 = 1 * 2^3 + 1 * 2^2 + 0 * 2^1 + 1 * 2^0
D'où    = (1 * 2 + 1) * 2^2 + 0 * 2^1 + 1 * 2^0
        = ((1 * 2 + 1) * 2) * 2^1 + 1 * 2^0
        = (((1 * 2 + 1) * 2) * 2 + 1)
On peut réécrire chaque nombre en utilisant cette technique.
Et grâce à la distributivité avec les puissance de 2 on sépare donc ces choix
récursivement :
- Si le dernier nombre est un 0, on multiplie par 2 le nat_of_positive du reste
- Si le dernier nombre est un 1, on fait la même chose et on rajoute 1
- Et on s'arrête avec le premier 1 qu'on définit comme 1 en base 10 grâce à S 0.
*)

Theorem test_nop1: (nat_of_positive 1~1~0) = 6.
Proof.
  simpl.
  reflexivity.
Qed.

Theorem test_nop2: (nat_of_positive 1~1~1~1) = 15.
Proof.
  simpl.
  reflexivity.
Qed.

Theorem test_nop3: (nat_of_positive 1) = S 0.
Proof.
  simpl.
  reflexivity.
Qed.


(** **** Exercice : Prouver le théorème suivant. *)

(** Pour vous aider : *)
Check Nat.eq_add_0.

Theorem positive_not_0 : forall p : positive, ~ (nat_of_positive p = 0).
Proof.
  (* Il y a 3 constructeurs, donc les preuves par induction se font en trois
     étapes. *)
  induction p as [p' IHp'|p' IHp'|].
  - (* Cas p = p'~0 *)
    (* On réécrit notre but *)
    simpl.
    (* On sait que le Successeur d'un naturel n'est jamais 0 *)
    discriminate.
  - (* Cas p = p'~1 *)
    (* On réécrit notre but *)
    simpl.
    (* On introduit une partie du but en hypothèse *)
    intros H.
    (* On utilise un autre théorème que celui présenté avant l'exercice.
       Je pense que j'ai utilisé un algorithme différent que celui qui était
       prévu. Du coup, Nat.eq_add_0 ne convient pas, cependant, Nat.eq_mul_0
       permet de dire que a * b = 0 => a = 0 ∨ b = 0, ce qu'on peut prouver. *)
    apply Nat.eq_mul_0 in H.
    (* On sépare les cas *)
    destruct H.
    +
      (* Par induction, on sait que nat_of_positive p' ≠ 0 *)
      apply IHp' in H.
      assumption.
    +
      (* On sait que 2 ≠ 0 *)
      discriminate.
  - (* Cas p = 1 *)
    (* nat_of_positive 1 = 1 (naturel) *)
    simpl.
    (* Or 1 ≠ 0 *)
    discriminate.
Qed.

(** **** Exercice : définition du successeur.

    Rendre la définition du successeur correct, changer tous les cas. Les tests
    suivants (sous la forme de théorèmes) devraient passer. *)

Fixpoint succ x :=
  match x with
    | p~1 => (succ p)~0
    | p~0 => p~1
    | 1 => 1~0
  end.

(*
Expliquation de l'algorithme :

On fait une incrémentation simple en définissant une idée simple de la retenue.
Si le nombre finit par 0, c'est simple, on le remplace par un 1.
Sinon, s'il finit par un 1, on le remplace par 0 et on fait le successeur du
reste (comme si en base 10, on remplace le 9 par 0 et on rajoute un 1 en
retenue).
*)

Theorem test_succ1 : nat_of_positive (succ (succ (succ (succ (succ 1))))) = 6.
Proof.
  simpl.
  reflexivity.
Qed.

Theorem test_succ2 : nat_of_positive (succ 1) = 2.
Proof.
  simpl.
  reflexivity.
Qed.

Theorem test_succ3 : nat_of_positive (succ (succ 1)) = 3.
Proof.
  simpl.
  reflexivity.
Qed.


(** **** Exercice : prouver le théorème suivant.

    Vous ferez une preuve par cas avec destruct.
    Rappelez-vous que [discriminate] cherche dans les hypothèses des égalités
    contradictoires entre constructeurs.
*)

Lemma succ_discr : forall p : positive, p <> succ p.
Proof.
  (* On introduit la variable p de type positif *)
  intros.
  (* On fait une démonstration par induction en 3 étapes *)
  induction p as [p' HI|p' HI|].
  - (* cas p'~1 ≠ succ p'~1 *)
    (* On peut réécrire succ p'~1 en (succ p')~0 *)
    simpl.
    (* Deux nombres positifs égaux ne peuvent pas finir par deux unités
    différentes, d'où p'~1 ≠ (succ p')~0 *)
    discriminate.
  - (* cas p'~0 ≠ succ p'~0 *)
    (* succ p'~0 = p'~1 *)
    simpl.
    (* On a p'~0 ≠ p'~1 *)
    discriminate.
  - (* cas 1 ≠ succ 2 *)
    (* succ 1 = 1~0 = 2 *)
    simpl.
    (* 1 ≠ 1~0 *)
    discriminate.
Qed.



(** Pour la preuve du théorème suivant, vous aurez sans doute envie d'utiliser la
    tactique [f_equal]. Cette tactique très simple, dit juste que pour prouver une
    égalité du type f(a) = f(b), où f est une fonction quelconque, _il suffit_
    de prouver que a = b.

    Exemple trivial : *)

Lemma exple_f_equal : forall p q : positive, p = q → succ(p) = succ(q).
Proof.
  (* Soient p et q deux positives. *)
  intros p q.
  (* On suppose (hyp. H) que p = q. *)
  intros H.
  (* Pour montrer que succ(p) = succ(q), il suffit de prouver que p = q. *)
  f_equal.
  (* Or, ceci est vrai par hypothèse. *)
  assumption.
Qed.

(** Autre rappel, pour une égalité entre termes construits avec le
    même constructeur dans une hypothèse H, utiliser [injection H] ou
    [injection H as H'] permet de simplifier par ce constructeur.

    Exemple trivial : *)
Lemma exple_injection : forall p q : positive, p~1 = q~1 → p = q.
Proof.
  intros p q.
  intros H.
  (* Par injectivité de l'application qui à p associe p ~ 1, on a, d'après
     l'hypothèse H, que p = q. *)
  injection H as H'.
  assumption.
Qed.

(** **** Exercice : Prouver le théorème suivant.

    C'est assez long et un peu plus dur qu'on pourrait le penser.
*)







(* À partir de ce point, j'ai pété un cable T-T.
Au secours.....


Sinon, pour vous faire gagner du temps, je n'ai réussi à prouver que succ_S
(Partie 2), fun_graphe, surj_inverse_d (Partie 3) et n_to_infty (Partie 4).

Vous pouvez voir mes tentatives et echecs aussi si vous voulez avoir pitié de
moi... *)






Lemma succ_inj : forall p q : positive, succ p = succ q -> p = q.
Proof.
  induction p as [p'|p'|].
  -
    intros q.
    simpl.
    destruct q as [q'|q'|].
    +
      simpl.
      intros.
      f_equal.
      apply IHp'.
      injection H.
      intros.
      assumption.
    +
      simpl.
      intros.
      discriminate.
    +
      simpl.
      intros.
      injection H as H'.
      exfalso.
      destruct p'; simpl in H'; discriminate.
  -
    intros q.
    simpl.
    destruct q as [q'|q'|].
    +
      simpl.
      intros.
      discriminate.
    +
      simpl.
      intros.
      injection H as H'.
      f_equal.
      assumption.
    +
      simpl.
      intros.
      discriminate.
  -
    intros.
    destruct q as [q'|q'|].
    +
      destruct q'; simpl; discriminate.
    +
      simpl in H.
      discriminate.
    +
      reflexivity.
Qed.


(** *** Partie bonus : les tacticielles.

    La preuve précédente comporte de nombreuses redites. Le système coq comporte
    un système de _tacticielles_ (en anglais tacticals) permettant justement
    d'éviter de dire la même chose plein de fois.

    Vous pouvez voir les plus fréquentes sur cette _Cheatsheet_ (à la toute
    fin) : en particulier, regarder le point-virgule [;] et [try]
    https://www.cs.cornell.edu/courses/cs3110/2018sp/a5/coq-tactics-cheatsheet.html
*)

(** **** Exercice bonus : essayer de rendre la preuve précédente
    aussi courte que possible en utilisant des tacticielles. *)

Lemma succ_inj' : forall p q : positive, succ p = succ q -> p = q.
Proof.
  induction p as [p'|p'|].
Admitted. (* Remplacer cette ligne par Qed. *)


(** **** Exercice : Prouver le théorème suivant.

    Vous pouvez faire votre petit marché dans les théorème déjà prouvés sur les
    nat de la bibliothèque standard : *)

Search Nat.add.

Theorem succ_S : forall p : positive,
  nat_of_positive (succ p) = S (nat_of_positive p).
Proof.
  intros.
  (* On essaye pour tous les cas de p *)
  induction p as [p' HI|p' HI|].
  - (* Si p = p'~1 *)
    (* On calcule succ p'~1 = (succ p') * 2 *)
    simpl.
    (* On remplace le S (nat_of_positive p') *)
    rewrite HI.
    (* On calcule (nat_of_positive p' + 1) * 2 = (nat_of_positive p') * 2 + 2 *)
    simpl.
    (* On a notre égalité *)
    reflexivity.
  - (* Si p = p'~0 *)
    (* On calcule succ et nat_of_positive *)
    simpl.
    (* On a directement notre égalité *)
    reflexivity.
  - (* Si p = 1 *)
    (* On calcule *)
    simpl.
    (* C'est directement une égalité *)
    reflexivity.
Qed.

End Positive.
  
(** ** Troisième partie : surjections, injections et bijections *)

Section Fonctions.
Definition surjective {X Y} (f : X -> Y) := forall (y : Y), exists (x : X), (f x = y).

Definition injective {X Y} (f : X -> Y) := forall x1, forall  x2, (f x1 = f x2 -> x1=x2).

Definition bijective {X Y} (f : X → Y) := (injective f) /\ (surjective f).

(** On commence par quelques exemples simples qui vous permettent de
maitriser les définitions *)

Fixpoint double (n : nat) :=
  match n with
    0 => 0
  | S n' => S (S (double n'))
  end.


(* Merci à Rayane Amara qui m'a confirmé l'utilisation de l'induction quand cet
exo me rendais fou...
Malheureusement, ça n'aura pas suffit :') *)
Theorem double_inj : injective double.
Proof.
  unfold injective.
  induction x1 as [|x1']; destruct x2; intros.
  -
    reflexivity.
  -
    discriminate.
  -
    discriminate.
  -
    injection H as H'.
    f_equal.
    apply IHx1'.
    assumption.
Qed.



Theorem double_non_surj : ¬ (surjective double).
Proof.
  unfold surjective, not.
  intros H.
  destruct (H 1) as [x_faux H_faux].
  destruct x_faux as [|x_faux_pred].
  -
    simpl in H_faux.
    discriminate.
  -
    simpl in H_faux.
    discriminate.
Qed.



Fixpoint div_par_deux (n : nat) :=
  match n with
    0 => 0
  | 1 => 0
  | S (S n') => S (div_par_deux n')
  end.

Theorem div_par_deux_surj : surjective div_par_deux.
Proof.
  unfold surjective.
  induction y as [|y'].
  -
    exists 0.
    simpl.
    reflexivity.
  -
    destruct (IHy') as [x0 H0].
    exists (S (S x0)).
    simpl.
    f_equal.
    assumption.
Qed.



Theorem div_par_deux_non_inj : ¬ (injective div_par_deux).
Proof.
  unfold not, injective.
  intros.
  assert (E: div_par_deux 0 = div_par_deux 1). {
    simpl. reflexivity.
  }
  apply H in E.
  discriminate.
Qed.


(** On vous propose de montrer le fait suivant: f est bijective ssi
tout (y : Y) admet un unique antécédent.
Attention, la preuve est un peu longue. Nous vous conseillons de bien
structurer le code. *)

(** Le connecteur [exists!] signifie "il existe un unique". C'est une
notation qui se déplie automatiquement quand vous faites la preuve
d'un énoncé, comme ci-dessous.  *)

Lemma bij_exuniq {X Y} (f : X → Y) : (bijective f) ↔ (∀ (y : Y), (exists! x, f x = y)) .
Proof.
  split.
  -
    intros.
    unfold bijective in H.
    destruct H as [HFinj HFsurj].
    unfold unique.
    unfold injective in HFinj.
    unfold surjective in HFsurj.
Admitted. (* Remplacer cette ligne par Qed. *)



Definition inverse_gauche {X Y} (g : Y → X) (f : X → Y) := ∀ (x : X), g (f x) = x.

Definition inverse_droite {X Y} (g : Y → X) (f : X → Y) := ∀ x, f (g x) = x.

(** Pour parler facilement de fonctions, on va avoir besoin d'un axiome,
qui transforme les relations en fonctions. On l'appelle l'axiome du
choix, il permet de construire une fonction décrivant la propriété
que doivent vérifier ses valeurs *)

(* from Coq.Logic.ClassicalChoice *)
Axiom choice : ∀ (A B : Type) (R : A→B→Prop),
  (∀ x : A, exists y : B, R x y) →
  exists f : A→B, (∀ x : A, R x (f x)).

Definition graphe {X Y} (f : X → Y) y x := f x = y.

(* Prouvez le lemme ci-dessous *)
Lemma fun_graphe {X Y} f : surjective f -> (∀ y : Y, ∃ x : X, graphe f y x).
Proof.
  (* AHHHH, ENFIN UN THÉORÈME QUE J'ARRIVE À PROUVER !!! *)
  intros HFsurj.
  (* On déplie les définitions de graphe est surjective *)
  unfold graphe.
  unfold surjective in HFsurj.
  (* On implique la surjectivité *)
  apply HFsurj.
Qed.


(** Par exemple, si f est une fonction, (choice Y X (graphe f)) nous
dit que si " (∀ y : Y, ∃ x : X, graphe f y x)" est vérifié, alors on
peut construire une fonction g tel que pour tout y, on ai "graphe f y
g(y)", c'est à dire " f (g y) = y)".  Ici, la relation "R" en
paramètre de l'axiome choice est instanciée par "graphe f", c'est à
dire que y est x sont en relation ssi "f x = y" est vérifié *)


(** La preuve de ce lemme vous demande d'utiliser l'axiome du choix
appliqué au graphe de la bonne fonction. Faites usage de Check pour
vérifier que vous l'appliquez bien, avec de l'utiliser avec un
destruct. C'est un peu plus difficile, n'hésitez pas à faire les
autres et à revenir dessus plus tard. *)

Lemma surj_inverse_d {X Y} (f : X -> Y) :
  surjective f -> exists g, inverse_droite g f.
Proof.
  intros HFsurj.
  apply (choice Y X (graphe f)).
  apply fun_graphe.
  assumption.
Qed.


(* Preuve perso pour simplifier *)
Lemma inj_inverse_g {X Y} (f : X -> Y) :
  injective f -> exists g, inverse_gauche g f.
Proof.
  intros HFinj.
  unfold injective in HFinj.
  unfold inverse_gauche.
  assert (E: (∃ g : Y → X, ∀ x : X, f (g (f x)) = f x) -> ∃ g : Y → X, ∀ x : X, g (f x) = x).
  {
    intros H.
    destruct H as [g H].
    exists g.
    intros.
    apply HFinj.
    apply H.
  }
  apply E.
Admitted.


Lemma bij_inversible {X Y} (f : X -> Y) :
  bijective f -> exists g, inverse_droite g f /\ inverse_gauche g f.
Proof.
  intros HFbij.
  unfold bijective in HFbij.
  destruct HFbij as [HFinj HFsurj].
  assert (E: (exists g, inverse_droite g f) ∧ (exists g, inverse_gauche g f)).
  {
    intros.
    split.
    -
      apply surj_inverse_d.
      assumption.
    -
      apply inj_inverse_g.
      assumption.
  }
Admitted.


(* Je suis sûr que c'est simple à prouver, mais j'arrive plus à réfléchir là XD!
Il est 20:30 le Vendredi..... *)
Lemma inversible_bijective {X Y} (f : X -> Y) :
  (exists g, inverse_droite g f /\ inverse_gauche g f) -> bijective f.
Proof.
  intros.
  destruct H.
  destruct H as [Hid Hig].
  unfold bijective.
  split.
  -
    unfold injective.
    intros.
    unfold inverse_gauche in Hig.
(*
    rewrite H in Hig.
    apply fun_graphe in Hig.
    apply (choice Y X (graphe f)) in Hig.
  -
    unfold inverse_droite in Hid.
    unfold surjective.
    apply fun_graphe.
*)
Admitted.


End Fonctions.

Module Suites.

Open Scope R_scope.
(** En vous inspirant de la preuve de UL_sequence, prouvez
    que la somme de deux suites convergentes converge vers la somme des limites.
 *)




(*
!!!
Question, pour ceci, y a pas des théorèmes qui manquent du fichier Suites ?
!!!
*)





Theorem CV_plus (An Bn : nat -> R) (l1 l2 : R) :
  let Cn := (fun n => An n + Bn n) in
  Un_cv An l1 -> Un_cv Bn l2 -> Un_cv Cn (l1 + l2).
Proof.
  unfold Un_cv.
  intros.
  exists O.
  intros.
Admitted.


(** Maintenant, à vous de faire la preuve qu'en multipliant une suite
    convergente vers l par une constante lambda, on obtient une suite qui
    converge vers lambda * l.
    Attention, il y a deux cas lambda = 0 ou non.
    Faire un [destruct Req_dec lambda 0] pour séparer ces deux cas.
    Quelques lemmes, non rencontrés jusqu'à maintenant que nous avons utilisé :
*)
Check Req_dec.
Check R_dist_eq.
Check Rabs_pos_lt.
Check Rdiv_lt_0_compat.
Check Rmult_lt_compat_l.
Check R_dist_mult_l.
(** Ça ne veut pas dire que ce soit indispensable de les utiliser (il y a
    de nombreuses façons de faire). Assurez-vous, avant de vous lancer dans la
    preuve formelle de savoir le faire en maths. *)
Lemma CV_mult_const (Un : nat → R) (lambda : R) (l : R) :
  let Vn := fun n => lambda * Un n in
  Un_cv Un l → Un_cv Vn (lambda * l).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


(** Pour se reposer un peu, on peut prouver ce petit lemme, qui peut servir ! *)
Theorem CV_An_minus_a_0 (An : nat -> R) (a : R) :
  let Cn (n : nat) := (An n - a) in
  Un_cv An a <-> Un_cv Cn 0.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


(** On va maintenant vers un résultat très important : les suites convergences
    sont bornées. Pour ce faire on a besoin de l'opération suivante : *)
Fixpoint running_max (Un : nat → R) (n : nat) :=
  match n with
    0%nat => (Un 0%nat)
  | S(n) => Rmax (running_max Un n) (Un (S n))
  end.
(** En français, running_max Un n est le maximum des n + 1 premiers termes de la
    suite Un. *)

Lemma Un_le_running_max (Un : nat → R) (n : nat) :
  forall i, (i <= n)%nat → Un i <= (running_max Un n).
Proof.
Admitted.


(** On a maintenant ce qu'il faut pour le montrer. Assurez-vous encore de savoir
    écrire la preuve mathématique avant de vous lancer. *)

Theorem CV_impl_bounded (Un : nat -> R) (l : R) :
  Un_cv Un l -> exists m : R, 0 < m /\ (forall n : nat, Rabs (Un n) <= m).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


(** **Un exemple de suite qui diverge vers l'infini
    
    Il n'y a pas que les suites convergentes dans la vie. Il y a aussi
    - des suites qui divergent vers + l'infini;
    - des suites qui divergent vers - l'infini;
    - des suites qui divergent tout court (n'admettent ni limite finie ni limite
      infinie).

    On va ici prouver la suite définir par Un = n pour tout n diverge vers
    l'infini.
    D'abord la définition, dans cette bibliothèque, cela s'écrit [cv_infty].
*)
Print cv_infty.

(** En mathématiques, on fait souvent (et pour de bonnes raisons) l'abus que
    l'ensemble des naturels est inclus dans l'ensemble des réels. En coq, ce
    n'est pas possible : nat et R sont deux types différents.
    La fonction INR (pour injection de N dans R) permet de passer du naturel n
    au réel 1 + 1 + ... + 1 (n fois). *)
Print INR.

(** Nous allons prouver que la suite INR diverge vers l'infini. Pour ceci, nous
    aurons besoin de la propriété suivante des réels : ils forment un corps
    archimédien : *)
Axiom archimed' :
  forall eps A : R, eps > 0 -> A > 0 -> exists n : nat, (INR n) * eps > A.
(** Cet axiome dit en substance que
    - pour un eps > 0 aussi petit qu'on veut;
    - pour un A > 0 aussi grand qu'on veut;
    On peut toujours trouver un entier naturel n tel que n * eps est plus grand
    que A.
    
    Remarque : l'axiome [archimed] de cette bibliothèque est différent, car plus
    puissant, mais plus difficile, dans un premier temps, à utiliser.
*)

(** À vous maintenant, vous aurez à
    [destruct (archimed' preuve1 preuve2 val_eps val_A)]
    avec :
    - val_eps : ce que vous avez choisi comme eps (n'allez pas chercher trop
    loin...)
    - val_A : ce que vous avez choisi comme A
    - preuve1 : une preuve que votre eps > 0
    - preuve2 : une preuve que votre A > 0

    Petit point embêtant : comme il n'y a pas d'hypothèse sur M dans
    cv_infty, il faut gérer les cas M < 0 et M = 0 (qui pourtant ne servent à
    rien dans la preuve), [lra] est votre ami.

    Enfin, vous aurez sans doute besoin de résultats sur INR.
*)
Search "INR".
Check le_INR.


Theorem n_to_infty :
  let Un n := INR n in cv_infty Un.
Proof.
  intros.
  unfold cv_infty.
  intros.
  (* Je remercie chaleureusement l'équipe de la doc de Coq <3 *)
  destruct (Req_dec M 0).
  -
    rewrite H.
    exists 1%nat.
    unfold Un.
    intros.
    apply le_INR in H0.
    simpl in H0.
    lra.
  -
    destruct (Rdichotomy M 0).
    +
      assumption.
    +
      exists 1%nat.
      unfold Un.
      intros.
      apply le_INR in H1.
      simpl in H1.
      lra.
    +
      destruct (archimed' 1 M).
      *
        lra.
      *
        assumption.
      *
        exists x.
        unfold Un.
        intros.
        apply le_INR in H2.
        lra.
Qed.

(* OUIIIIIIIIIIIIIIIIIIIIIIIIII. OUIIIIIIIIIIIIIIIIIIIIIIIIIII.

OUIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII!!!

Ah, j'ai cherché partout dans stdlib pour trouver les bons Lemmes pour diviser
les cas de M XD !

Il est 23:04 le vendredi là, donc j'ai pas le temps de rédiger avec des
commentaires :P ! *)


(** archimed' n'est pas définit dans la stdlib. Vous pouvez utiliser archimed de 
    la stdlib, mais c'est plus difficile (passages de N à Z à R). Pas exemple,
    si vous souhaitez vous entrainer un peu plus (optionnel), vous pouvez
    démontrer le lemme suivant (avec archimed ou archimed'): *)

Lemma cv_speed_1_Sn :
  Un_cv (fun n:nat => / INR (S n)) 0.
Proof.
(* On utilise le fait que si une suite Un tend vers +infini alors 
la suite 1/Un tend vers 0 *)
(* On peut utiliser le lemme cv_infty_cv_R0 défini dans la stdlib. *)
(* ... ici ... *)
(* Dans notre cas, il faudra par la ensuite montrer que (S n) tend vers
+infini. *)
(* Une suite Un tend vers +infini si:
∀ M : R, ∃ N : nat, ∀ n : nat, N ≤ n → M < Un n *)
(* Pour cela, il faut considérer 3 cas selon que M est négatif, égal à 0 
ou positif *)
(* On peut utiliser le lemme total_order_T défini dans la stdlib. *)
(* ... ici ... *)
(* Cas M < 0: N = 0 et vous pourrez utiliser Rlt_trans, lt_INR_0 et lt_O_Sn. *)
(* Cas M = 0: N = 0 également. *)
(* Cas M > 0: N = l'entier supérieur à M. 
Vous pourrez utiliser up, archimed (ou archimed'), et les lemmes d'injection 
de N->R, Z->R, etc.*)
(* ... ici ... *)
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)



(** **Majorant, minorant, borne supérieure et borne inférieure *)

(** Rappel : en coq, une partie de R est une fonction de R vers Prop.
    Un majorant (en anglais upper bound) est une valeur qui est supérieure ou
    égale à tous les éléments de l'ensemble. *)
Print is_upper_bound. (* en français : est majorant de *)
(** On rappelle qu'il faut ici comprendre (E x) comme "x appartient à E", donc
    [is_upper_bound E m] dit que pour tout x appartenant à E, x <= m. *)

(** Un minorant d'une partie est défini de la même manière. *)
Definition is_lower_bound (E : R -> Prop) (l : R) :=
  ∀ x : R, E x → l <= x.

(** On va montrer que l'ensembles des inverses des entiers naturels est minoré
    par 0. *)
Definition ens_inverses (x:R) := exists n : nat, x = / ((INR n) + 1).

Check pos_INR.
Theorem inverses_min : is_lower_bound ens_inverses 0.
Proof.
  unfold is_lower_bound.
  unfold ens_inverses.
  intros.
Admitted. (* Remplacer cette ligne par Qed. *)


(** La borne supérieure d'un ensemble, lorsqu'elle existe est le plus petit
    majorant de cet ensemble (en anglais least upper bound), ici, [is_lub]. *)
Print is_lub.

(** Même chose pour la borne inférieure (en anglais greatest lower bound). *)
Definition is_glb (E : R -> Prop) (l : R) :=
  is_lower_bound E l ∧ (∀ b : R, is_lower_bound E b → b <= l).

(** À vous, maintenant, prouvez que la borne inférieure des inverses des entiers
    naturels est 0. *)
Theorem inverses_glb : is_glb ens_inverses 0.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


(** **Produit de deux suites convergentes. *)


(** On commence par le cas où l'une des suites converge vers 0.
    N'hésitez pas, si vous en éprouver le besoin à écrire des lemmes pour
    faciliter la preuve. *)

Theorem CV_mult_0 (An Bn : nat → R) (a : R) :
  let Cn (n:nat) := (An n * Bn n) in
  Un_cv An a → Un_cv Bn 0 → Un_cv Cn 0.
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)


(** Ensuite, le cas général *)

Theorem CV_mult (An Bn : nat → R) (a b : R) :
  let Cn (n:nat) := (An n * Bn n) in
  Un_cv An a → Un_cv Bn b → Un_cv Cn (a * b).
Proof.
  (* Remplir la preuve ici *)
Admitted. (* Remplacer cette ligne par Qed. *)

End Suites.
