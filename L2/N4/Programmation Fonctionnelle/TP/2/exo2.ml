let rec decouper e l = match l with
    [] ->
        [],[]
    | e'::ll ->
        let
            (a, b) = decouper e ll
        in
            if (e' < e) then
                (e'::a,b)
            else
                (a,e'::b);;

let rec concat l1 l2 = match l1 with
    e::ll1 -> e::(concat ll1 l2)
    | [] -> l2;;

let rec tri_rapide l = match l with
    [] -> []
    | e::ll ->
        let
            (a, b) = decouper e ll
        in
            concat (tri_rapide a) (e::(tri_rapide b));;
