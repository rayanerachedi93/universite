let rec inter l1 l2 = match l1,l2 with
    [],_ -> []
    | _,[] -> []
    | e1::ll1,e2::ll2 -> if e1 < e2
        then (inter ll1 l2)
        else if e2 < e1
        then (inter l1 ll2)
        else e1::(inter ll1 ll2);;

let rec union l1 l2 = match l1,l2 with
    [],_ -> l2
    | _,[] -> l1
    | e1::ll1,e2::ll2 -> if e1 < e2
        then e1::e2::(inter ll1 ll2)
        else if e2 < e1
        then e2::e1::(inter ll1 ll2)
        else e1::(inter ll1 ll2);;

let rec appartient e l = match l with
    [] -> false
    | f::ll -> if e=f
        then true
        else appartient e ll;;

let rec inter_2 l1 l2 = match l1 with
    [] -> []
    | e1::ll1 -> if (appartient e1 l2) then e1::(inter_2 ll1 l2) else (inter_2 ll1 l2);;

let rec union_2 l1 l2 = match l1 with
    [] -> l2
    | e1::ll1 -> if (appartient e1 l2) then (union_2 ll1 l2) else e1::(union_2 ll1 l2);;
