let rec fact x = match x with
    0 -> 1
    | n -> x * (fact (n - 1));;

print_int (fact 4);;
print_endline "";;

let rec fact_acc acc x = match x with
    0 -> acc
    | n -> (fact_acc (acc * n) (n - 1));;

let fact_bin x = fact_acc 1 x;;

print_int (fact_bin 4);;
print_endline "";;

let fact_bis n = let rec fact_acc n a
                    = if n = 1
                      then a
                      else fact_acc (n-1) (n*a)
                 in (fact_acc n 1);;

let rec append l1 l2 = match l1 with
    e::l -> e::(append l l2)
    | [] -> l2;;

let rec rev l = match l with
    [] -> []
    | e::ll -> append (rev ll) (e::[]);;

let rec rev_acc acc l = match l with
    [] -> acc
    | e::ll -> rev_acc (e::acc) ll;;

let rev_bin = rev_acc [];;
