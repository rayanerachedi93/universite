use sdl2::render::Canvas as C;
use sdl2::video::Window;
use sdl2::pixels::Color;
use sdl2::gfx::primitives::DrawRenderer;

/// Pixel n'est qu'un raccourcis de (u32, u32).
///
/// En Rust, ce type de donnée est un [tuple]](`std::tuple`), similaire aux
/// tuples de Python. C'est un couple de données. La première valeur est
/// accedée avec `x.0` et la deuxième avec `x.1`.
pub type Pixel = (u32, u32);

/// Canvas n'est qu'un raccourcis de `C<Window>`.
///
/// C n'est que [`Canvas<T>`](sdl2::render::Canvas). En gros, j'ai la flemme
/// d'écrire Canvas<Window> à chaque fois.
pub type Canvas = C<Window>;

/// Dessine un triangle de sierpinski.
///
/// Cette fonction dessinge un triangle de sierpinski de profondeur `n` sur le
/// `canvas` fourni en argument dans la zone délimité par les coordonnées `p1`,
/// `p2` et `p3`. Sachant que `p1` doit être le point inférieur gauche, `p2` le
/// point supérieur, et `p3` le point inférieur droit.
///
/// # Complexité
///
/// Comme cela a été vu en cours, la complexité de cet algorithme est de O(3^n).
pub fn sierpinski(canvas: &mut Canvas, n: u32, p1: Pixel, p2: Pixel, p3: Pixel) {
    if n == 0 {
        // Si n = 0, on dessine notre triangle.
        draw_triangle(canvas, p1, p2, p3);
    } else {
        // Sinon, on dessine 3 sous-triangles :
        // Le triangle inférieur gauche.
        sierpinski(canvas, n - 1,
                   p1,
                   ((p1.0 + p2.0)/2, (p1.1 + p2.1)/2),
                   ((p1.0 + p3.0)/2, (p1.1 + p3.1)/2));
        // Le triangle supérieur.
        sierpinski(canvas, n - 1,
                   ((p1.0 + p2.0)/2, (p1.1 + p2.1)/2),
                   p2,
                   ((p3.0 + p2.0)/2, (p3.1 + p2.1)/2));
        // Le triangle inférieur droit.
        sierpinski(canvas, n - 1,
                   ((p1.0 + p3.0)/2, (p1.1 + p3.1)/2),
                   ((p3.0 + p2.0)/2, (p3.1 + p2.1)/2),
                   p3);
    }
}

/// Dessine un triangle.
///
/// Cette fonction n'est qu'une réutilisation éhontée de la fonction
/// `filled_trigon(p1, p2, p3, color)` de SDL2_gfx. Il pourrait être
/// réinteressant de réimplémenter `filled_trigon`, mais bof.
pub fn draw_triangle(canvas: &mut Canvas, p1: Pixel, p2: Pixel, p3: Pixel) {
    // Pour une certaine raison, `filled_trigon` prend des i16.
    canvas.filled_trigon(
        p1.0 as i16, p1.1 as i16,
        p2.0 as i16, p2.1 as i16,
        p3.0 as i16, p3.1 as i16,
        Color::RGB(0, 0, 0)
    ).unwrap();
}
