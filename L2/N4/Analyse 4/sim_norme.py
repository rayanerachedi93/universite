#!/bin/python3

import pygame
import math

degree = 1

def dans_boule(degree, coords):
    return math.pow(math.pow(abs(coords[0]), degree) + math.pow(abs(coords[1]), degree), 1/degree) <= 1

pygame.init()

win = pygame.display.set_mode((600, 600))
pygame.display.set_caption("Simulation d'une boule")

font = pygame.font.SysFont(pygame.font.get_default_font(), 30)

c = True

while c:
    degree += 0.01
    pygame.draw.rect(win, (255, 255, 255), (0, 0, 600, 600))
    win.blit(font.render(str(degree), True, (0, 0, 0)), (0, 0))
    
    for x in range(-250, 250):
        for y in range(-250, 250):
            if dans_boule(degree, (x / 250, y / 250)):
                win.set_at((x + 300, y + 300), (0, 0, 0))
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            c = False
    pygame.display.flip()

pygame.quit()
