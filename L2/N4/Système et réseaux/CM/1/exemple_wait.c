#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <wait.h>
#include <time.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	pid_t enf1, p;
	srand(time(NULL));
	int duree1 = rand() % 10000, duree2 = rand() % 10000;
	switch ((enf1 = fork())) {
	case -1: perror("fork"); return 1;
	case  0: usleep(duree1); return 0;
	default:
		switch (fork()) {
		case -1: perror("fork"); return 1;
		case  0: usleep(duree2); return 0;
		}
	}

	while ((p = wait(NULL)) != -1)
		if (p == enf1)
			printf("enfant 1 a terminé\n");
		else
			printf("enfant 2 a terminé\n");
	printf("Tout le monde a terminé.\n");
	return 0;
}
