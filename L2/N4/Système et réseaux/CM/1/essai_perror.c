#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

int main()
{
	char errno_string[4];
	int i;
	for (i = 0; i < 156; ++i) {
		errno = i;
		sprintf(errno_string, "%3d", i);
		perror(errno_string);
	}
	return 0;
}
