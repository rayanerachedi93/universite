#include <stdio.h>
#include <pthread.h>

void *afficher_100_fois(void *arg);

int main()
{
	pthread_t th1, th2;

	if (pthread_create(&th1, NULL, afficher_100_fois, "A") != 0) {
		perror("pthread");
		return 1;
	}
	if (pthread_create(&th2, NULL, afficher_100_fois, "B") != 0) {
		perror("pthread");
		return 1;
	}
	pthread_join(th1, NULL);
	pthread_join(th2, NULL);
	printf("\n");

	return 0;
}
void *afficher_100_fois(void *arg)
{
	int i;
	for (i = 0; i < 100; ++i)
		printf(arg);
	return NULL;
}
