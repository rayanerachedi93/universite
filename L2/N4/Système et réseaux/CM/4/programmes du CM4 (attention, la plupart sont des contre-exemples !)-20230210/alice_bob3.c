#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>
void *alice(void *);
void *bob(void *);
void faire_des_choses();
char res;
int main()
{
	pthread_t a, b;
	srand(3);
	pthread_create(&a, NULL, alice, NULL);
	pthread_create(&b, NULL, bob, &a);

	pthread_join(b, NULL);
	return 0;
}
void faire_des_choses() { usleep(rand() % 10 * 50000); }
void *alice(void *arg)
{
	faire_des_choses();
	res = 'A';
	return NULL;
}
void *bob(void *thread)
{
	pthread_t *a = thread;
	faire_des_choses();
	pthread_join(*a, NULL);
	printf("%c\n", res);
	return NULL;
}
