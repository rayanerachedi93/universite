#include <stdio.h>
int main()
{
	char message[] = "Magie noire";

	unsigned *n = (unsigned *) message;
	unsigned short *m = (unsigned short *) message;
	void *adresse = message;

	printf("%s\n", message + 3);
	printf("%c\n", *(message + 3));
	printf("%x\n", *(message + 3));

	n += 2;
	printf("%s\n", (char *) n);
	printf("%x\n", *n);

	m += 3;
	printf("%s\n", (char *) m);
	printf("%x\n", *m);

	/* ok avec gcc sans l'option -pedantic-errors ou -Werror-pointer-arith
	 * mais non standard ! */
	adresse += 4;
	printf("%s\n", (char *) adresse);

	/* *adresse = 42; */

	n -= 2;
	*n += 32;
	printf("%s\n", message);
	return 0;
}
