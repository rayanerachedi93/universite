#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

#define NUM_ITER_DEFAUT 3

int main(int argc, char *argv[])
{
	int num_iter, i;
	num_iter = argc < 2 ? NUM_ITER_DEFAUT : atoi(argv[1]);

	for (i = 0; i < num_iter; ++i) {
		switch (fork()) {
		case -1:
			perror("fork");
			return 1;
		case 0:
			printf("plop\n");
			break;
		default:
			switch (fork()) {
			case -1:
				perror("fork");
				return 1;
			case 0:
				break;
			default:
				printf("hop\n");
				return 0;
			}
		}
	}
	printf("bloup\n");
	return 0;
}
