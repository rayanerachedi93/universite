#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

void* f(void* arg) {
	printf("child : %d", getpid());
	return NULL;
}

int main() {
	pthread_t p;
	printf("parent : %d", getpid());
	pthread_create(&p, NULL, f, NULL);
	pthread_join(p, NULL);
	return 0;
}
