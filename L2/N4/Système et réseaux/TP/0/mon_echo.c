#include <stdlib.h>
#include <stdio.h>

int main(int argc, char** argv) {
	/*
	for (int i = 1; i < argc; i++) {
		int j = 0;
		while (argv[i][j] != '\0') {
			putchar(argv[i][j++]);
		}
		if (i + 1 != argc) putchar(' ');
	}
	*/
	while (*(++argv)) {
		while(**argv) putchar(*(*argv)++);
		putchar(*(argv + 1) ? ' ' : '\n');
	}

	return 0;
}
