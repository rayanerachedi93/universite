#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <pthread.h>
#define test_err(test, msg, no) if ((test)) perror(msg), exit(no)

#ifndef N /* nombre de lignes des matrices */
#define N 10
#endif

#ifndef K /* nombre de lignes des matrices */
#define K N
#endif

void imprime(int matrice[N][N]);
void generation_aleatoire(int matrice[N][N]);
void *calcul_ligne(void* arg);

int A[N][N], B[N][N], R[N][N];

int main()
{
	struct timespec tic, toc;
	double duree;
	int i;

	srand(time(NULL));
	generation_aleatoire(A);
	generation_aleatoire(B);
	imprime(A);
	puts("");
	imprime(B);
	puts("");
	clock_gettime(CLOCK_REALTIME, &tic);

	pthread_t p[N];
	int numligne[N];

	int k = 0, c = 1;

	while (c) {
		for (i = 0; i < N; ++i) {
			numligne[i] = k*K + i;
			if (k*K + i >= N) {
				c = 0;
				break;
			}
			test_err(pthread_create(&p[i], NULL, calcul_ligne, numligne + i), "thread create", 1);
		}

		for (i = N - i - 1; i < N; ++i) {
			test_err(pthread_join(p[i], NULL), "thread join", 2);
		}
	}

	clock_gettime(CLOCK_REALTIME, &toc);
	duree = (toc.tv_sec - tic.tv_sec);
	duree += (toc.tv_nsec - tic.tv_nsec) / 1000000000.0;
	printf("durée : %g\n", duree);
	imprime(R);
	return 0;
}

void imprime(int matrice[N][N])
{
	int i, j;
	for (i = 0; i < N; ++i)
		for (j = 0; j < N; ++j)
			printf("%d%c", matrice[i][j], j == N - 1 ? '\n' : '\t');
}

void generation_aleatoire(int matrice[N][N])
{
	int i, j;
	for (i = 0; i < N; ++i)
		for (j = 0; j < N; ++j)
			matrice[i][j] = rand() % 100;
}

void *calcul_ligne(void* arg) {
	int *l = (int *) arg, j, k;

	for (j = 0; j < N; ++j) {
		R[*l][j] = 0;
		for (k = 0; k < N; ++k)
			R[*l][j] += A[*l][k] * B[k][j];
	}

	return NULL;
}
