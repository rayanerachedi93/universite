// FIXME: Deadlock & ne marche pas

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#define HAUTEUR 40
#define LARGEUR 80
#define NUM_ITER_DFT 30

int (ecran1)[HAUTEUR][LARGEUR];
int (ecran2)[HAUTEUR][LARGEUR];
int (*ecran_a_afficher)[HAUTEUR][LARGEUR];
int (*ecran_a_modifier)[HAUTEUR][LARGEUR];
int num_iter;

pthread_t th_aff;
pthread_t th_buf;
sem_t sem_aff;
sem_t sem_buf;
pthread_mutex_t maj_num_iter;

void effacer_lignes(int num_lignes);
void* buffer(void* arg);
void* affichage(void* arg);

int main(int argc, char *argv[])
{
	num_iter = argc > 1 ? atoi(argv[1]) : NUM_ITER_DFT;

	sem_init(&sem_aff, 0, 0);
	sem_init(&sem_buf, 0, 0);
	pthread_mutex_init(&maj_num_iter, NULL);

	ecran_a_afficher = &ecran1;
	ecran_a_modifier = &ecran2;

	pthread_create(&th_buf, NULL, buffer, NULL);
	pthread_create(&th_aff, NULL, affichage, NULL);

	pthread_join(th_buf, NULL);
	pthread_join(th_aff, NULL);

	puts("YOU LOSE.\n");
	return 0;
}

void* buffer(void* arg) {
	int i, j;
	pthread_mutex_lock(&maj_num_iter);
	while (num_iter) {
		pthread_mutex_unlock(&maj_num_iter);

		int (*ecran)[HAUTEUR][LARGEUR] = ecran_a_modifier;
		sem_post(&sem_buf);

		/* calcul de l'affichage suivant */
		for (i = HAUTEUR - 1; i > 0; --i)
			for (j = 0; j < LARGEUR; ++j)
				(*ecran)[i][j] = (*ecran)[i - 1][j];
		for (j = 0; j < LARGEUR; ++j)
			(*ecran)[0][j] = 0;
		(*ecran)[0][rand() % LARGEUR] = 1;

		usleep(500000);

		sem_wait(&sem_aff);
		ecran_a_afficher = ecran;

		pthread_mutex_lock(&maj_num_iter);
	}
	pthread_mutex_unlock(&maj_num_iter);
}

void* affichage(void* arg) {
	int i, j;
	pthread_mutex_lock(&maj_num_iter);
	while (num_iter) {
		pthread_mutex_unlock(&maj_num_iter);

		int (*ecran)[HAUTEUR][LARGEUR] = ecran_a_afficher;
		sem_post(&sem_aff);

		/* affichage de l'écran modifié */
		effacer_lignes(HAUTEUR);

		for (i = 0; i < HAUTEUR; ++i) {
			for (j = 0; j < LARGEUR; ++j) {
				if ((*ecran)[i][j] == 0)
					putchar(' ');
				else
					putchar('*');
			}
			putchar('\n');
		}

		sem_wait(&sem_buf);
		ecran_a_modifier = ecran;

		pthread_mutex_lock(&maj_num_iter);
		num_iter--;
	}
	pthread_mutex_unlock(&maj_num_iter);
}

void effacer_lignes(int num_lignes)
{
	printf("\033[2K");
	for(; num_lignes > 0; --num_lignes) {
		printf("\033[F");
		printf("\033[2K");
	}
}
