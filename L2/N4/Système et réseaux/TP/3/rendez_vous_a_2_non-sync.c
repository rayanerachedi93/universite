#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

#define COEFF_ATTENTE 100000
#define COEFF_DELTA   5000

void *compteur1(void *);
void *compteur2(void *);
void prendre_du_temps_pour_compter();
int main()
{
	pthread_t th1, th2;
	sem_t s[2];
	sem_init(s, 0, 1);
	sem_init(s + 1, 0, 0);

	if (pthread_create(&th1, NULL, compteur1, s) != 0) {
		perror("create th1");
		return 1;
	}
	if (pthread_create(&th2, NULL, compteur2, s) != 0) {
		perror("create th2");
		return 1;
	}

	pthread_join(th1, NULL);
	pthread_join(th2, NULL);
	sem_destroy(s);
	sem_destroy(s + 1);
	return 0;
}

void *compteur1(void *sem_tab)
{
	sem_t* s = (sem_t*) sem_tab;
	int i;
	for (i = 0; i <= 100; i = i + 1) {
		prendre_du_temps_pour_compter();
		printf("a : %d\n", i);
		sem_wait(s);
		sem_post(s + 1);
	}
	return NULL;
}

void *compteur2(void *sem_tab)
{
	sem_t* s = (sem_t*) sem_tab;
	int i;
	for (i = 0; i <= 100; i = i + 1) {
		prendre_du_temps_pour_compter();
		printf("b : %d\n", i);
		sem_wait(s + 1);
		sem_post(s);
	}
	return NULL;
}

void prendre_du_temps_pour_compter()
{
	usleep(COEFF_ATTENTE + (rand() % 10) * COEFF_DELTA );
}
