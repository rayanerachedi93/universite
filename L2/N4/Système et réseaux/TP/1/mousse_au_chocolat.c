#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>
#include <wait.h>

void casser_chocolat();
void faire_fondre_chocolat();
void separer_blanc_jaune();
void melanger_jaunes_choco();
void battre_blancs_neige();
void incorporer_blancs_neige();

int main()
{
	int i;

	if (fork() == 0) {
		for (i = 0; i < 5; ++i)
			if (fork() == 0) {
				separer_blanc_jaune();
				return 0;
			}
		separer_blanc_jaune();
		while (wait(NULL) > 0);
		battre_blancs_neige();
		return 0;
	}
	casser_chocolat();
	faire_fondre_chocolat();
	while (wait(NULL) > 0);
	melanger_jaunes_choco();
	incorporer_blancs_neige();

	printf("PID %d : la mousse est terminée !\n", getpid());
}

void casser_chocolat()
{
	printf("PID %d : je casse le chocolat.\n", getpid());
	usleep(200000);
	printf("PID %d : le chocolat est cassé !\n", getpid());
}

void faire_fondre_chocolat()
{
	printf("PID %d : je fais fondre le chocolat.\n", getpid());
	usleep(1000000);
	printf("PID %d : le chocolat est fondu !\n", getpid());
}

void separer_blanc_jaune()
{
	printf("PID %d : je sépare le blanc d'un jaune.\n", getpid());
	usleep(200000);
	printf("PID %d : un blanc est séparé d'un jaune !\n", getpid());
}

void melanger_jaunes_choco()
{
	printf("PID %d : je mélange les jaunes et le chocolat.\n", getpid());
	usleep(200000);
	printf("PID %d : le chocolat est mélangé avec les jaunes !\n", getpid());
}

void battre_blancs_neige()
{
	printf("PID %d : je bas les blancs en neige.\n", getpid());
	usleep(600000);
	printf("PID %d : les blancs sont battus en neige !\n", getpid());
}

void incorporer_blancs_neige()
{
	printf("PID %d : j'incorpore les blancs en neige.\n", getpid());
	usleep(1000000);
	printf("PID %d : les blancs sont incorporés !\n", getpid());
}
