#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>

int main(int argc, char** argv) {
	if (argc <= 2) {
		fprintf(stderr, "Pas assez d'arguments.\n");
		exit(1);
	}

	int n = atoi(argv[1]);

	for (int i = 0; i < n; i++) {
		switch (fork()) {
			case -1:
				perror("fork error");
				exit(1);
			case 0:
				execvp(argv[2], argv + 2);
				perror("command failed");
				exit(2);
		}
	}

	pid_t status;
	for (int i = 0; i < n; i++) wait(&status);

	if (WIFEXITED(status)) {
		return WEXITSTATUS(status);
	} else if (WIFSIGNALED(status)) {
		return WTERMSIG(status) + 128;
	} else {
		return -1;
	}
}
