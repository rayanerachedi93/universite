/**
 *
 *
 *
 * @invariant getNom() != null;
 * @invariant getPoids() >= 0;
 * @invariant getPeutOuvrir() != null;
 *
 */

public class Cle
{
	private String nom;
	private int poids;
	private Coffre peutOuvrir;
	
	
	
	/** Initialise une cle en prenant en arguments son nom, son poids, le nom et le poids du coffre qu'elle peut ouvrir
	 *@param nom de type String
	 *@param poids de type int
	 *@param nomCoffre de type String
	 *@param poidsCoffre de type int
	 */
	/*@
	  @requires nom != null;
	  @requires poids >= 0;
	  @requires nomCoffre != null;
	  @requires poidsCoffre >= 0;
	  @ensures this.getNom() != null;
	  @ensures this.getPoids() >= 0;
	  @ensures this.getPeutOuvrir() != null;
	  @ensures this.peutOuvrir.getPoidsAVide() > 0
	  @ensures this.peutOuvrir(this.peutOuvrir);
	  @*/
	public Cle(String nom, int poids, String descriptionCoffre, int poidsCoffre)
	{
		this.nom = nom;
		this.poids = poids;
		this.peutOuvrir = new Coffre (descriptionCoffre, poidsCoffre);
	}
	   
	/** Initialise une cle en prenant en arguments son nom, son poids, le coffre qu'elle peut ouvrir
	*@param nom de type String
	*@param poids de type int
	*@param coffre de type coffre
	*/
	/*@
	@requires nom != null;
	@requires poids >= 0;
	@requires coffre != null;
	@requires coffre.getPoidsAVide() > 0;
	@ensures this.getNom() != null;
	@ensures this.getPoids() >= 0;
	@ensures this.getPeutOuvrir() != null;
	@ensures this.peutOuvrir.getPoidsAVide() > 0
	@ensures this.peutOuvrir(this.peutOuvrir);
	@*/
	public Cle(String nom, int poids, Coffre coffre)
	{
		this.nom = nom;
		this.poids = poids;
		this.peutOuvrir = coffre;
	}
	
	/** Renvoie le nom de cette cle
	 *@return le nom de cette cle (type String) 
	 *@pure
	 */
	public String getNom()
	{
		return this.nom;
	}
		
	/** Renvoie le poids de cette cle
	 *@return le poids de cette cle (type int) 
	 *@pure
	 */
	public int getPoids()
	{
		return this.poids;
	}
	
		
	/** Renvoie le Coffre que cette cle
	 *@return le coffre de cette cle (type Coffre) 
	 *@pure
	 */
	public Coffre getPeutOuvrir()
	{
		return this.peutOuvrir;
	}

	
	   
	/*@
	  @requires coffre != null;
	  @ensures this.getPeutOuvrir().equals(coffre) && ! coffre.estOuvert() <==> \result;
	  @*/
	public boolean peutOuvrir(Coffre coffre)
	{
		return this.peutOuvrir.equals(coffre) && !coffre.estOuvert();
	}
	
	/** Trouve si une cle est egale a une autre, en comparant leurs champs
	 *@param o un Objet
	 *@return un booleen, qui est true si les objets sont egaux et false sinon (type boolean)
	 */
	public boolean equals(Object o)
	{
		if(! (o instanceof Cle))
		{
			return false;
		}
		Cle uneCle = (Cle) o;
		if (!(this.getNom().equals(uneCle.getNom())))
		{
			return false;
		}
		if (this.getPoids() != uneCle.getPoids())
		{
			return false;
		}
		if (!(this.getPeutOuvrir().equals(uneCle.getPeutOuvrir())))
		{
			return false;
		}
		return  true;
	}
	
	
}
