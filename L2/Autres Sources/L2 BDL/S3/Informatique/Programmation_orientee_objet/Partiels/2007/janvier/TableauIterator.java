import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Iterateur pour les tableaux.
 * 
 * <p>
 * L'opération <code>remove</code> procède en remplaçant l'élément supprimé
 * par le dernier élément du tableau ; l'ordre initial des éléments tel qu'il
 * était spécifié par le tableau initial n'est donc pas préservé. Il est par
 * contre garantie que tous les <code>nbElements</code> premiers éléments
 * présents dans le tableau initial seront énumérés. L'opération
 * <code>remove</code> opère sur l'instance de tableau transmise lors de
 * l'initialisation de l'itérateur.
 * </p>
 * 
 * @author Marc Champesme
 * @since 12 janvier 2005
 * @version 17 mai 2007
 */
public class TableauIterator<E> implements Iterator<E> {
	private Object[] tab;

	private int nbElements;

	private int index;

	private boolean removePermis;

	/**
	 * Initialise un itérateur sur les <code>nbElements</code> premiers
	 * éléments du tableau spécifié.
	 * 
	 * @requires tab != null;
	 * @requires nbElements >= 0;
	 * @requires tab.length >= nbElements;
	 * @ensures (nbElements > 0) <==> hasNext();
	 * 
	 * @param tab
	 *            le tableau dont cet itérateur doit retourner les éléments
	 * @param nbElements
	 *            le nombre d'éléments sur lequel ce tableau doit itérer
	 */
	public TableauIterator(Object[] tab, int nbElements) {
		this.nbElements = nbElements;
		this.tab = tab;
	}

	/**
	 * Renvoie <code>true</code> s'il reste des éléments pour cette itération.
	 * (Autrement dit, renvoie <code>true</code> si <code>next</code>
	 * renverrait un élément plutot que de lever une exception).
	 * 
	 * @return <code>true</code> s'il reste des éléments
	 */
	public boolean hasNext() {
		return index < nbElements;
	}

	/**
	 * Renvoie l'élément suivant pour cette itération.
	 * 
	 * @requires hasNext();
	 * 
	 * @return l'élément suivant pour cette itération
	 * @throws NoSuchElementException
	 *             s'il ne reste plus d'élément pour cette itération.
	 */
	public E next() {
		if (!hasNext()) {
			throw new NoSuchElementException("Plus d'éléments");
		}
		removePermis = true;
		return (E) tab[index++]; // unchecked cast
	}

	/**
	 * Supprime du tableau de cet itérateur le dernier élément retourné par cet
	 * itérateur. Cette méthode ne peut être appelée qu'une seule fois par appel
	 * à <code>next</code>. Le comportement d'un itérateur est non spécifié
	 * si le tableau parcouru par cet itérateur est modifié durant l'utilisation
	 * de cet itérateur par tout autre moyen que par un appel à cette méthode.
	 * 
	 * @requires La méthode next() a déjà été appelée depuis la création de
	 *           cette instance.
	 * @requires Cette méthode n'a pas encore été appelée depuis le dernier
	 *           appel à next().
	 * 
	 * @throws IllegalStateException
	 *             si la méthode <code>next</code> n'a pas encore été appelée,
	 *             ou que la méthode <code>remove</code> a déjà été appelée
	 *             depuis le dernier appel à la méthode <code>next</code>.
	 */
	public void remove() {
		if (!removePermis) {
			throw new IllegalStateException("Remove interdit");
		}
		// L'élément à supprimer est à l'index (index - 1)
		tab[index - 1] = tab[nbElements - 1];

		// L'élément remplaçant l'élément supprimé devient
		// le prochain élément à retourner par next
		index--;
		nbElements--;
		removePermis = false;
	}
}
