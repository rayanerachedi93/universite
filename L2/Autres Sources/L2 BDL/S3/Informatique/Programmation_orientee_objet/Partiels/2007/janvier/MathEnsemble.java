import java.util.Iterator;

/**
 *  <p>Collection non ordonnée d'éléments non <code>null</code> ne contenant pas
 *  d'éléments dupliqués. Plus formellement, un ensemble ne contient aucune
 *  paire d'éléments <code>e1</code> et <code>e2</code> telle que 
 *  <code>e1.equals(e2)</code>.</p> 
 *
 *  <p>Cette classe tente de modéliser au mieux le concept mathématique d'ensemble
 *  en définissant les opérations mathématiques usuelles sur les ensembles
 *  (cardinal, union, intersection...). </p>
 *
 * @invariant !contient(null);
 * @invariant cardinal() >= 0;
 * @invariant estVide() <==> (cardinal() == 0);
 * 
 * @author     Marc Champesme
 * @version    2 septembre 2005
 * @since      1 septembre 2005
 */

public interface MathEnsemble<E> extends Cloneable, Iterable<E> {
    /**
     *  Renvoie <code>true</code> si cet ensemble contient l'élément spécifié. Plus
     *  précisément, renvoie <code>true</code> si et seulement si cette liste
     *  contient un élément <code>e</code> tel que <code>e.equals(elt)</code>.
     *
     * @ensures (elt == null) ==> !\result;
     * 
     * @param  elt  L'élément dont on cherche &agrave; savoir s'il appartient
     *      &agrave; cet ensemble.
     * @return      <code>true</code> si l'élement spécifié est présent ; <code>false</code>
     *      sinon.
     */
    public boolean contient(Object elt);

    /**
     * Ajoute l'élément spécifié à cet ensemble s'il n'y est pas déjà présent. Renvoie
     * <code>true</code> si l'ensemble a été modifié suite à l'exécution de l'opération.
     * L'élément spcécifié doit être non <code>null</code>.
     * 
     * @requires elt != null;
     * @ensures this.contient(elt);
     * @ensures \result <==> !\old(contient(elt));
     * @ensures \result <==> (cardinal() == \old(cardinal()) + 1);
     * @ensures !\result <==> (cardinal() == \old(cardinal()));
     * 
     * @param elt L'élément à ajouter à cet ensemble.
     * @return <code>true</code> si cette opération a entrainé une modification de
     * cet ensemble ; <code>false</code> sinon.
     * @throws NullPointerException si l'élément spécifié est <code>null</code>.
     */
    public boolean ajouter(E elt);

    /**
     * Retire l'élément spécifié de cet ensemble s'il y est présent. Renvoie
     * <code>true</code> si l'ensemble a été modifié suite à l'exécution de l'opération.
     * 
     * @ensures !this.contient(elt);
     * @ensures \result <==> \old(contient(elt));
     * @ensures \result <==> (cardinal() == \old(cardinal()) - 1);
     * @ensures !\result <==> (cardinal() == \old(cardinal()));
     * 
     * @param elt L'élément à retirer de cet ensemble.
     * @return <code>true</code> si cette opération a entrainé une modification de
     * cet ensemble ; <code>false</code> sinon.
     */
    public boolean retirer(Object elt);
    
    /**
     *  Renvoie <code>true</code> si cet ensemble est inclus dans l'ensemble
     *  spécifié. Plus précisément, renvoie <code>true</code> si et seulement tout
     *  élément <code>e</code> de cet ensemble est tel que <code>ens.contient(e)</code>.
     *  
     *  @requires ens != null;
     *  @ensures \result <==> (\forall Object obj; ; this.contient(obj) ==> ens.contient(obj));
     *  @ensures \result ==> (this.cardinal() <= ens.cardinal());
     *  @ensures this.equals(ens) ==> \result;
     *  @ensures this.estVide() ==> \result;
     *
     * @param  ens  L'ensemble dont on souhaite savoir s'il contient cet ensemble.
     * @return      <code>true</code> si cet ensemble est inclus dans l'ensemble
     *      spécifié ; <code>false</code> sinon.
     */
    public boolean estInclusDans(MathEnsemble<?> ens);



    /**
     *  Modifie l'ensemble courant de sorte qu'il représente l'union ensembliste de
     *  cet ensemble avec l'ensemble spécifié.
     *  
     *  @requires ens != null;
     *  @ensures (\forall Object obj; ; contient(obj) <==>
     *                      (\old(contient(obj)) || ens.contient(obj)));
     *  @ensures ens.estInclusDans(this);
     *  @ensures cardinal() <= \old(cardinal()) + ens.cardinal();
     *  @ensures cardinal() >= \old(cardinal());
     *  @ensures cardinal() >= ens.cardinal();
     *
     * @param  ens  L'ensemble avec lequel l'union doit etre effectuée.
     * @throws NullPointerException si l'ensemble spécifié est <code>null</code>.
     */
    public void union(MathEnsemble<? extends E> ens);
        

    /**
     *  Modifie l'ensemble courant de sorte qu'il représente l'intersection
     *  ensembliste de cet ensemble avec l'ensemble spécifié.
     *  
     *  @requires ens != null;
     *  @ensures (\forall Object obj; ; contient(obj) <==> 
     *                          \old(contient(obj)) && ens.contient(obj));
     *  @ensures this.estInclusDans(ens);
     *  @ensures cardinal() <= \old(cardinal());
     *  @ensures cardinal() <= ens.cardinal();
     *  @ensures (\old(this.estVide()) || ens.estVide()) ==> this.estVide();
     *
     * @param  ens  L'ensemble avec lequel l'intersection doit etre effectuée.
     * @throws NullPointerException si l'ensemble spécifié est <code>null</code>.
     */
    public void intersection(MathEnsemble<?> ens);


    /**
     *  Modifie l'ensemble courant de sorte qu'il représente la différence
     *  ensembliste de cet ensemble avec l'ensemble spécifié.
     *  
     * @requires ens != null;
     * @ensures (\forall Object obj; ; contient(obj) <==> 
     *                          \old(contient(obj)) && !ens.contient(obj));
     * @ensures cardinal() <= \old(cardinal());
     * @ensures \old(cardinal()) - cardinal() <= ens.cardinal();
     * 
     * @param  ens  L'ensemble &agrave; soustraire de cet ensemble.
     * @throws NullPointerException si l'ensemble spécifié est <code>null</code>.
     */
    public void difference(MathEnsemble<?> ens);



    /**
     *  Renvoie <code>true</code> si cet ensemble ne contient aucun élément.
     * 
     * @ensures \result <==> (cardinal() == 0);
     *
     * @return    <code>true</code> si cet ensemble ne contient aucun élément ;
     *      <code>false</code> sinon.
     */
    public boolean estVide();



    /**
     *  Renvoie le cardinal de cet ensemble.
     *  
     * @ensures \result >= 0;
     *
     * @return    le nombre d'éléments de cet ensemble.
     */
    public int cardinal();

    /**
     * Renvoie un itérateur sur les éléments de cet ensemble.
     * 
     * @ensures \result != null;
     * 
     * @return Un itérateur sur cet ensemble.
     */
    public Iterator<E> iterator();


    /**
     *  Teste l'égalité entre l'objet spécifié et cet ensemble. Renvoie <code>true</code>
     *  si et seulement si l'objet spécifié est aussi un <code>MathEnsemble</code>, que
     *  les deux ensembles ont le m&ecirc;me nombre d'éléments et que les deux
     *  ensembles contiennent les m&ecirc;mes elements au sens de <i>equals</i> .
     *
     * @ensures !(obj instanceof MathEnsemble) ==> !\result;
     * @ensures obj instanceof MathEnsemble
     *     ==> (\result
     *         <==> (((MathEnsemble) obj).estInclusDans(this)
     *               && this.estInclusDans((MathEnsemble) obj)));
     * @ensures \result ==> (cardinal() == ((MathEnsemble)obj).cardinal());
     * @ensures \result ==> (hashCode() == ((MathEnsemble)obj).hashCode());
     * 
     * @param  obj  l'objet &agrave; comparer avec cette liste.
     * @return      <code>true</code> si l'objet spécifié est composé d'éléments
     *      <i>equals</i> &agrave; ceux de cette liste ; <code>false</code> sinon
     */
    public boolean equals(Object obj);


    /**
     *  Renvoie la valeur du code de hashage de cet ensemble. Le code de hashage
     *  d'un ensemble est définit comme étant la somme des codes de hashage de ses
     *  éléments. Ce calcul assure que <code>ens1.equals(ens2)</code> implique que
     *  <code>ens1.hashCode() == ens2.hashCode()</code> pour toute paire d'ensembles,
     *  <code>ens1</code> et <code>ens2</code>, comme le spécifie le contrat de
     *  <code>Object.hashCode()</code>.</p>
     *
     * @return    la valeur du code de hashage pour cet ensemble.
     */
    //@ pure
    public int hashCode();


    /**
     *  Renvoie une copie de cet ensemble, c'est-&agrave;-dire un nouvel ensemble
     *  contenant les mêmes éléments que cet ensemble.
     *  
     * @ensures \result != null;
     * @ensures \result != this;
     * @ensures \result.equals(this);
     * @ensures (\result.getClass()).equals(this.getClass());
     *
     * @return    Un clone de cette instance.
     */
    public MathEnsemble<E> clone();

	/**
	 *  Renvoie une représentation sous forme de chaine de caractères de cet
	 *  ensemble. Cette représentation est constituée des éléments
	 *  de cet ensemble entourée d'accolades ("{}"). Des éléments adjacents sont
	 *  séparés par les caractères ", " (virgule et espace). Les élements sont
	 *  convertis en chaines de caractères en appelant leur méthode 
	 *  <code>toString</code>.
	 *
	 * @ensures \result != null;
	 * 
	 * @return    une représentation sous forme de chaine de caractères de cette
	 *      liste.
	 */
    public String toString();
}

