/** Classe ObjetActif
 *
 *
 * @invariant getNom() != null;
 * @invariant getSeuilActivite() > 0;
 * @invariant getPointsDeVie() != 0; 
 */

public class ObjetActif
{
	/*@
	  @invariant getNom() != null;
	  @invariant getSeuilActivite() > 0;
	  @invariant getPointsDeVie() != 0; 
	  @*/

	private String nom;
	private int seuilActivite;
	private int pointsDeVie;
	
	/** Initialise un nouvel ObjetActif aux valeurs entrées en argument, son nom, une chaine de caracteres non null
	 * Un nombre d'objets positif ou nul
	 * Et un nombre de points de vie, strictement positif (non nul)
	 *@param nom, une chaine de caracteres (type String)
	 *@param seuilActivite un entier (type int)
	 *@param pointsDeVie un entier (type int)
	 */
	
	/*@
	  @requires nom != null;
	  @requires seuilActivite > 0;
	  @requires pointsDeVie != 0;
	  @ensures this.getNom() == nom;
	  @ensures this.getNom().equals(nom);
	  @ensures this.getSeuilActivite() == seuilActivite;
	  @ensures this.getSeuilActivite() > 0;
	  @ensures this.getPointsDeVie() == pointsDeVie;
	  @ensures this.getPointsDeVie() != 0;
	  @*/

	public ObjetActif (String nom, int seuilActivite, int pointsDeVie)
	{
		this.nom = nom;
		this.seuilActivite = seuilActivite;
		this.pointsDeVie = pointsDeVie;
	}

	/** Renvoie le nom d'un ObjetActif
	 *@return this.nom une chaine de caracteres (type String)
	 */
	
	/*@
	  @ensures this.getNom().equals(this.nom);
	  @pure;
	  @*/

	public String getNom()
	{
		return this.nom;
	}
	
	
	/** Renvoie le nombre d'Objets dans un ObjetActif
	 *@return this.seuilActivite un entier (type int)
	 */
	
	/*@
	  @ensures this.getSeuilActivite() == this.seuilActivite;
	  @pure;
	  @*/
	  
	public int getSeuilActivite()
	{
		return this.seuilActivite;
	}

	/** Renvoie le nombre de points de vie dans un ObjetActif
	 *@return this.pointsDeVie un entier (type int)
	 */
	
	/*@
	  @ensures this.getPointsDeVie() == this.pointsDeVie;
	  @pure;
	  @*/
	  
	public int getPointsDeVie()
	{
		return this.pointsDeVie;
	}

	/** Compare un OBjetActif a un Object o passe en parametre
	 * Verifie que o contient une instance de la classe ObjetActif
	 * Si oui, compare tous les attributs de l'ObjetActif, et renvoie vrai s'ils sont tous égaux, sinon renvoie faux
	 *@param o, un Object
	 *@return un booleen qui est vrai si les 2 objets sont egaux et faux sinon (type boolean)
	 */
	 
	 /*@
	   @requires o.getNom() != null;
	   @ensures !(o instanceof Coffre) ==> !\result;
	   @ensures \result ==> getNom().equals((ObjetActif) o.getNom());
	   @ensures \result ==> (getSeuilActivite() == ((ObjetActif) o).getSeuilActivite());
	   @ensures \result ==> (getPointsDeVie() == ((ObjetActif) o).getPointsDeVie());
	   
	   @pure
	   @*/

	public boolean equals (Object o)
	{
		if(!(o instanceof ObjetActif))
		{
			return false;
		}
		ObjetActif nouvelObjet = (ObjetActif) o;
		if (!(this.getNom().equals(nouvelObjet.getNom())))
		{
			return false;
		}
		if(this.getSeuilActivite() != nouvelObjet.getSeuilActivite())
		{
			return false;
		}
		if(this.getPointsDeVie != nouvelObjet.getPointsDeVie())
		{
			return false;
		}
		return true;
		
	}
	
	/** Renvoie le hashCode d'un ObjetActif, un entier qui depend de la valeur de ses attributs
	 *@return un entier (type int)
	 */
	
	/*@
	  @requires this.getNom() != null;
	  \(forall y ObjetActif; this.equals(y) ==> this.hashCode()==y.hashCode());



}


