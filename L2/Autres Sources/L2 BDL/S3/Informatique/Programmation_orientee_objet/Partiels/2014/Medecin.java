
import java.util.ArrayList;

/**
 * Un Medecin est un personnage caractérisé par son nom et une réserve de soins.
 * Les soins d'un Medecin sont représentés par des instances de la classe Soin.
 * Un Medecin peut accumuler un nombre illimité de soins qu'il peut ensuite
 * utiliser pour soigner un Joueur en augmentant son capital vie de la valeur du
 * Soin administré. Un Medecin peut posséder plusieurs exemplaires d'un même
 * soin (au sens de equals). Une fois un soin utilisé par un Medecin pour
 * soigner un Joueur, un exemplaire de ce soin disparait de sa réserve de soins.
 * L'ordre dans lequel un Medecin utilise ses soins n'est pas spécifié, en
 * conséquence l'utilisateur de cette classe commettrait une erreur en faisant
 * reposer le bon fonctionnement de son programme sur un ordre particulier.
 * 
 * @invariant getNom() != null;
 * @invariant getNbSoins() >= 0;
 * @invariant getPuissance() >= 0;
 * @invariant !possede(null);
 * @invariant reserveEstVide() ==> (getPuissance() == 0);
 * 
 * @author Marc Champesme
 * @version 4 mars 2014
 * @since 1 mars 2014
 */
public class Medecin {
	private final static Soin PLACEBO = new Soin("Placebo", 0);
	private String nom;
	private ArrayList<Soin> mesSoins;

	/**
	 * Initialise un Medecin dont le nom est la chaîne de caractères spécifiée
	 * et ne possédant encore aucun soin.
	 * 
	 * @requires nom != null;
	 * @ensures getNom().equals(nom);
	 * @ensures reserveEstVide();
	 * 
	 * @param nom
	 *            le nom du médecin.
	 */
	public Medecin(String nom) {
		this.nom = nom;
		mesSoins = new ArrayList<Soin>();
	}

	/**
	 * Initialise un Medecin dont le nom est la chaîne de caractères spécifiée
	 * et la réserve de Soin est constituée à partir du tableau spécifié. La
	 * réserve de Soin est initialisée pour contenir les nbSoins premiers
	 * éléments du tableau spécifié.
	 * 
	 * @requires nom != null;
	 * @requires tabSoins != null;
	 * @requires (nbSoins >= 0) && (nbSoins <= tabSoins.length);
	 * @requires (\forall int i; i >= 0 && i < nbSoins; tabSoins[i] != null);
	 * @ensures getNom().equals(nom);
	 * @ensures getNbSoins() == nbSoins;
	 * @ensures (\forall int i; i >= 0 && i < nbSoins; possede(tabSoins[i]));
	 * @ensures getPuissance() == (\sum int i; i >= 0 && i < nbSoins;
	 *          tabSoins[i].getValeur());
	 * 
	 * @param nom
	 *            le nom du médecin.
	 * @param tabSoins
	 *            tableau contenant les Soin à ajouter à la réserve de ce
	 *            Medecin
	 * @param nbSoins
	 *            nombre d'éléments du tableau à ajouter à la réserve de ce
	 *            Medecin
	 */
	public Medecin(String nom, Soin[] tabSoins, int nbSoins) {
		this.nom = nom;
		mesSoins = new ArrayList<Soin>(nbSoins);
		for (int i = 0; i < nbSoins; i++) {
			mesSoins.add(tabSoins[i]);
		}
	}

	/**
	 * Renvoie le nom de ce médecin.
	 * 
	 * @return le nom de ce médecin.
	 * 
	 * @pure
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Renvoie <code>true</code> si la réserve de soins de ce Medecin est vide.
	 * 
	 * @ensures \result <==> (getNbSoins() == 0);
	 * 
	 * @return <code>true</code> si ce Medecin ne possède aucun soin ;
	 *         <code>false</code> sinon.
	 * 
	 * @pure
	 */
	public boolean reserveEstVide() {
		return mesSoins.isEmpty();
	}

	/**
	 * Renvoie le nombre de soins restant à ce médecin.
	 * 
	 * @return le nombre de soins restant à ce médecin.
	 * 
	 * @pure
	 */
	public int getNbSoins() {
		return mesSoins.size();
	}

	/**
	 * Renvoie le Soin qui sera utilisé par ce Medecin lors du prochain appel à
	 * la méthode soigne. Renvoie un Soin de valeur nulle si la réserve de Soin
	 * de ce Medecin est vide.
	 * 
	 * @ensures \result != null;
	 * @ensures !reserveEstVide() <==> possede(\result);
	 * @ensures reserveEstVide() ==> (\result.getValeur() == 0)
	 * 
	 * @return le prochain Soin administré par ce Medecin.
	 * 
	 * @pure
	 */
	public Soin prochainSoin() {
		if (this.reserveEstVide()) {
			return Medecin.PLACEBO;
		}
		return mesSoins.get(mesSoins.size() - 1);
	}

	/**
	 * Renvoie une nouvelle instance de tableau contenant tous les Soin de la
	 * réserve de Soin de ce Medecin.
	 * 
	 * @ensures \result != null;
	 * @ensures \result.length == getNbSoins();
	 * @ensures (\forall int i; i >= 0 && i < getNbSoins();
	 *          possede(\result[i]));
	 * @ensures getPuissance() == (\sum int i; i >= 0 && i < getNbSoins();
	 *          \result[i].getValeur());
	 * 
	 * 
	 * @return un tableau contenant tous les Soin de ce Medecin.
	 * 
	 * @pure
	 */
	public Soin[] getTabSoins() {
		return mesSoins.toArray(new Soin[0]);
	}

	/**
	 * Administre un des soins détenus par ce Medecin au Joueur spécifié. En
	 * conséquence, un soin equals au soin administré est supprimé de l'ensemble
	 * des soins détenus par ce Medecin. L'ordre dans lequel les soins sont
	 * administré n'est pas spécifié, il est par contre garanti que chaque soin
	 * ajouté sera administré avant que la réserve de ce Medecin ne devienne
	 * vide. Si ce Medecin ne détient plus aucun soin ou si le Joueur spécifié
	 * est mort la valeur renvoyée est false; sinon la valeur renvoyée est true.
	 * 
	 * @requires unJoueur != null;
	 * @ensures \result <==> (!\old(reserveEstVide()) &&
	 *          \old(unJoueur.estVivant()));
	 * @ensures unJoueur.getCapitalVie() >= \old(unJoueur.getCapitalVie());
	 * @ensures !\result <==> (getNbSoins() == \old(getNbSoins()));
	 * @ensures !\result ==> (unJoueur.getCapitalVie() ==
	 *          \old(unJoueur.getCapitalVie()));
	 * @ensures !\result ==> (getPuissance() == \old(getPuissance()));
	 * @ensures \result <==> (getNbSoins() == (\old(getNbSoins()) - 1));
	 * @ensures \result <==> (possedeCombienDe(\old(prochainSoin())) ==
	 *          (\old(possedeCombienDe(prochainSoin())) - 1));
	 * @ensures \result ==> (unJoueur.getCapitalVie() ==
	 *          \old(unJoueur.getCapitalVie() + prochainSoin().getValeur()));
	 * @ensures \result ==> (getPuissance() == \old(getPuissance() -
	 *          prochainSoin().getValeur()));
	 * 
	 * @return true si un soin a été administré à ce Joueur; false sinon.
	 */
	public boolean soigne(Joueur unJoueur) {
		if (mesSoins.isEmpty() || !unJoueur.estVivant()) {
			return false;
		}
		Soin unSoin = prochainSoin();
		unJoueur.recoitSoin(unSoin);
		mesSoins.remove(unSoin);
		return true;
	}

	/**
	 * Renvoie true si ce médecin possède un soin equals au soin spécifié.
	 * 
	 * @ensures (soin == null) ==> !\result;
	 * @ensures \result <==> (possedeCombienDe(soin) > 0);
	 * 
	 * @param soin
	 *            le soin à chercher dans la réserve de ce médecin
	 * @return true si ce médecin possède le soin spécifié ; false sinon
	 * 
	 * @pure
	 */
	public boolean possede(Soin soin) {
		return mesSoins.contains(soin);
	}

	/**
	 * Renvoie le nombre de soins détenus par ce Medecin et equals au soin
	 * spécifié.
	 * 
	 * @ensures \result <= getNbSoins();
	 * @ensures \result >= 0;
	 * @ensures (soin == null) ==> (\result == 0);
	 * @ensures (\result > 0) <==> possede(soin);
	 * 
	 * @param unSoin
	 *            le soin à chercher dans la réserve de ce médecin
	 * @return le nombre d'occurence du soin spécifié dans la réserve de ce
	 *         Medecin.
	 * @pure
	 */
	public int possedeCombienDe(Soin unSoin) {
		if (unSoin == null) {
			return 0;
		}

		int nb = 0;

		for (Soin autreSoin : mesSoins) {
			if (unSoin.equals(autreSoin)) {
				nb++;
			}
		}
		return nb;
	}

	/**
	 * Ajoute le soin spécifié à la réserve de soins de ce médecin. Le soin
	 * spécifié doit être non <code>null</code>.
	 * 
	 * @requires soin != null;
	 * @ensures !reserveEstVide();
	 * @ensures possede(unSoin);
	 * @ensures possedeCombienDe(unSoin) == (\old(possedeCombienDe(unSoin)) +
	 *          1);
	 * @ensures getNbSoins() == (\old(getNbSoins()) + 1);
	 * @ensures getPuissance() == (\old(getPuissance()) + unSoin.getValeur());
	 * 
	 * @param unSoin
	 *            le soin à ajouter à la réserve de ce médecin.
	 */
	public void ajoute(Soin unSoin) {
		mesSoins.add(unSoin);
	}

	/**
	 * Renvoie la somme des puissances des soins détenus par ce Medecin. Les
	 * soins pouvant avoir une puissance positive, négative ou nulle, il est
	 * possible que la valeur renvoyée soit nulle alors que le Medecin possède
	 * encore plusieurs soins de puissance non nulle.
	 * 
	 * @return la puissance totale conférée à ce Medecin par les soins qu'il
	 *         détient.
	 * @pure
	 */
	public int getPuissance() {
		int puissance = 0;
		for (Soin unSoin : mesSoins) {
			puissance += unSoin.getValeur();
		}
		return puissance;
	}

	/**
	 * Compare ce Medecin avec l'objet spécifié et renvoie <code>true</code> si
	 * et seulement si cet objet est un Medecin de même nom possédant les mêmes
	 * soins (au sens de equals) et possédant chaque soin en autant
	 * d'exemplaires que ce Medecin. L'ordre dans lequel seraient invoqués les
	 * soins des deux Medecin est pris en compte dans la comparaison. Deux
	 * Medecin equals doivent invoquer leurs soins dans le même ordre.
	 * 
	 * @ensures !(obj instanceof Medecin) ==> !\result;
	 * @ensures \result ==> getNom().equals(((Medecin) obj).getNom());
	 * @ensures \result ==> (getNbSoins() == ((Medecin) obj).getNbSoins());
	 * @ensures \result ==> (getPuissance() == ((Medecin) obj).getPuissance());
	 * @ensures \result ==> (prochainSoin().equals(((Medecin) obj).prochainSoin()));
	 * @ensures \result ==> (hashCode() == ((Medecin) obj).hashCode());
	 * 
	 * @param obj
	 *            l'objet à comparer à ce Medecin en terme d'égalité
	 * @return <code>true</code> si l'objet spécifié est un Medecin de même nom
	 *         possédant les mêmes soins; <code>false</code> sinon.
	 * @pure
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Medecin)) {
			return false;
		}

		Medecin autreMedecin = (Medecin) obj;
		if (!this.getNom().equals(autreMedecin.getNom())) {
			return false;
		}

		if (this.getNbSoins() != autreMedecin.getNbSoins()) {
			return false;
		}

		return mesSoins.equals(autreMedecin.mesSoins);
	}

	/**
	 * Renvoie un nouveau Medecin equals à ce Medecin.
	 * 
	 * @ensures \result != null;
	 * @ensures \result != this;
	 * @ensures this.equals(\result);
	 * 
	 * @return un clone de ce Medecin
	 * 
	 * @pure
	 */
	@Override
	public Medecin clone() {
		Medecin m = null;
		try {
			m = (Medecin) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new InternalError();
		}
		m.mesSoins = new ArrayList<Soin>(mesSoins);
		return m;
	}

	/**
	 * Renvoie un code de hashage pour ce Medecin.
	 * 
	 * @return un code de hashage pour ce Medecin.
	 * 
	 * @pure
	 */
	@Override
	public int hashCode() {
		return nom.hashCode() + mesSoins.hashCode();
	}

	/**
	 * Renvoie une représentation concise sous forme de chaîne de caractères de
	 * ce Medecin.
	 * 
	 * @ensures \result != null;
	 * 
	 * @return une représentation de ce Medecin sous forme de chaîne de
	 *         caractères.
	 * @pure
	 */
	@Override
	public String toString() {
		return "Medecin:" + nom + mesSoins;
	}
}
