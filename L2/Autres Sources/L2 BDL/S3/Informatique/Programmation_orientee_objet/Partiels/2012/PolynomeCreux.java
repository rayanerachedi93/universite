import java.util.ArrayList;

/**
 *  Polynomes non modifiables &agrave; coefficients entiers de la forme c0 +
 *  c1*x +c2*x^2 + ...
 *
 * @invariant getDegre() >= 0;
 * @invariant (getDegre() > 0) ==> (getCoefficient(getDegre()) !=0);
 * @invariant estZero() <==> ((getDegre() == 0) && (getCoefficient(0) == 0));
 * @invariant !estZero() <==> (getCoefficient(getDegre()) !=0);
 * @invariant evaluer(0) == getCoefficient(0);
 *
 * @author     Marc Champesme
 * @version    17 mars 2012
 * @since      mars 2012
 */
public class PolynomeCreux {
        /*@
	  @ invariant getDegre() >= 0;
	  @ invariant (getDegre() > 0) ==> (getCoefficient(getDegre()) !=0);
	  @ invariant estZero() <==> ((getDegre() == 0) && (getCoefficient(0) == 0));
	  @ invariant !estZero() <==> (getCoefficient(getDegre()) !=0);
	  @ invariant evaluer(0) == getCoefficient(0);
        @*/
        private ArrayList<Monome> termes;
        private int degre;

        /**
         *  Initialise un <code>PolynomeCreux</code> nul.
         *
         * @ensures estZero();
         */
        /*@
	   @ ensures estZero();
        @*/
        public PolynomeCreux() {
                termes = new ArrayList<Monome>();
        }
        
        /**
	 * Initialise un <code>PolynomeCreux</code> composé d'un unique terme 
	 * égal au <code>Monome</code> spécifié. Le <code>Monome</code> spécifié 
	 * doit être différent de <code>null</code> et doit être différent de 
	 * zéro (i.e. <code>!m.estZero()</code>).
	 *
	 * @param  m  <code>Monome</code> correspondant au terme du nouveau <code>PolynomeCreux</code>.
	 *
	 * @requires m != null;
	 * @requires !m.estZero();
	 * @ensures !estZero();
	 * @ensures getCoefficient(getDegre()) == m.getCoefficient();
	 * @ensures (\forall int exp; exp >= 0 && exp < degre; getCoefficient(exp) == 0);
	 */
        /*@
          @ requires m != null;
          @ requires !m.estZero();
          @ ensures !estZero();
          @ ensures getDegre() == m.getDegre();
          @ ensures getCoefficient(getDegre()) == m.getCoefficient();
	  @ ensures (\forall int exp; exp >= 0 && exp < degre;
	  @		getCoefficient(exp) == 0);          
	  @*/
        public PolynomeCreux(Monome m) {
        	termes = new ArrayList<Monome>();
        	termes.add(m);
        	degre = m.getDegre();
        }
   
        /**
         *  Renvoie <code>true</code> si et seulement si tous les coefficients 
         * de ce <code>PolynomeCreux</code> sont nuls.
         *
         * @return    <code>true</code> si ce <code>PolynomeCreux</code> est le 
         *	polynome zero ; <code>false</code> sinon.
         *
         * @ensures \result <==> (\forall int exp; exp >= 0 && exp <= getDegre();
         *				getCoefficient(exp) == 0);
         * @ensures \result <==> ((getDegre() == 0) && (getCoefficient(0) == 0));
         * @pure
         */
        /*@
	  @ ensures \result <==> (\forall int exp; exp >= 0 && exp <= getDegre();
	  @				getCoefficient(exp) == 0);
	  @ ensures \result <==> ((getDegre() == 0) && (getCoefficient(0) == 0));
	  @ pure
        @*/
        public boolean estZero() {
                return (getDegre() == 0) && (getCoefficient(0) == 0);
        }

        /**
         *  Renvoie un nouveau <code>PolynomeCreux</code> correspondant &agrave; 
         * la somme de ce <code>PolynomeCreux</code> et du <code>PolynomeCreux</code> 
         * spécifié.
         *
         * @param  p  Le polynome &agrave; additionner avec ce polynome.
         * @return    La somme de ce polynome avec le polynome spécifié.
         *
         * @requires p != null;
         * @ensures \result != null;
         * @ensures (\result != this) && (\result != p);
         * @ensures p.estZero() ==> \result.equals(this);
         * @ensures this.estZero() ==> \result.equals(p);
         * @ensures (this.getDegre() != p.getDegre()) 
         *	==> (\result.getDegre() == Math.max(this.getDegre(), p.getDegre()));
         * @ensures (\forall int exp; exp >= 0 && exp <= \result.getDegre();
         *	\result.getCoefficient(exp) 
         *	== this.getCoefficient(exp) + p.getCoefficient(exp));
         * @ensures (this.getDegre() == p.getDegre())
         * && ((this.getCoefficient(this.getDegre()) 
         *      + p.getCoefficient(p.getDegre())) != 0)
         * ==> (\result.getDegre() == this.getDegre());
         * @ensures (this.getDegre() == p.getDegre())
         * && ((this.getCoefficient(this.getDegre())
         *      + p.getCoefficient(p.getDegre())) == 0)
         * ==> (\result.getDegre() < this.getDegre());
         *
         * @pure
         */
        /*@
	  @ requires p != null;
	  @ requires (\forall int exp; exp >= 0 && exp <= Math.min(this.getDegre(), p.getDegre());
	  @		((this.getCoefficient(exp) < 0) && (p.getCoefficient(exp) < 0))
	  @			==> (this.getCoefficient(exp) > (Integer.MIN_VALUE - p.getCoefficient(exp))));
	  @ requires (\forall int exp; exp >= 0 && exp <= Math.min(this.getDegre(), p.getDegre());
	  @		((this.getCoefficient(exp) > 0) && (p.getCoefficient(exp) > 0))
	  @ 			==> (this.getCoefficient(exp) < (Integer.MAX_VALUE - p.getCoefficient(exp))));
	  @ ensures \result != null;
	  @ ensures (\result != this) && (\result != p);
	  @ ensures p.estZero() ==> \result.equals(this);
	  @ ensures this.estZero() ==> \result.equals(p);
	  @ ensures (this.getDegre() != p.getDegre()) 
	  @		==> (\result.getDegre() == Math.max(this.getDegre(), p.getDegre()));
	  @ ensures (\forall int exp; exp >= 0 && exp <= \result.getDegre();
	  @			\result.getCoefficient(exp) == this.getCoefficient(exp) + p.getCoefficient(exp));
	  @ ensures ((this.getDegre() == p.getDegre()) 
	  @		&& ((this.getCoefficient(this.getDegre()) + p.getCoefficient(p.getDegre())) != 0))
	  @	==> (\result.getDegre() == this.getDegre());
	  @ ensures ((this.getDegre() == p.getDegre()) && (this.getDegre() > 0)
	  @		&& ((this.getCoefficient(this.getDegre()) + p.getCoefficient(p.getDegre())) == 0))
	  @	==> (\result.getDegre() < this.getDegre());
	  @
	  @ pure
        @*/
        public PolynomeCreux additionner(PolynomeCreux p) {
                PolynomeCreux addPoly = new PolynomeCreux();
                int indexThis = 0;
                int indexOther = 0;
                while ((indexThis < this.termes.size()) && (indexOther < p.termes.size())) {
                        Monome thisTerme = this.termes.get(indexThis);
                        Monome otherTerme = p.termes.get(indexOther);
                        if (thisTerme.getExposant() < otherTerme.getExposant()) {
                                addPoly.termes.add(thisTerme);
                                addPoly.degre = thisTerme.getExposant();
                                indexThis++;
                        } else if (thisTerme.getExposant() > otherTerme.getExposant()) {
                                addPoly.termes.add(otherTerme);
                                addPoly.degre = otherTerme.getExposant();
                                indexOther++;
                        } else {
                                //@ assume thisTerme.getExposant() == otherTerme.getExposant();
                                int sommeCoeff = thisTerme.getCoefficient() + otherTerme.getCoefficient();
                                if (sommeCoeff != 0) {
                                        addPoly.termes.add(new Monome(sommeCoeff, thisTerme.getExposant()));
                                        addPoly.degre = thisTerme.getExposant();
                                }
                                indexThis++;
                                indexOther++;
                        }
                }
                while (indexThis < this.termes.size()) {
                        Monome thisTerme = this.termes.get(indexThis);
                        addPoly.termes.add(thisTerme);
                        addPoly.degre = thisTerme.getExposant();
                        indexThis++;
                }
                while (indexOther < p.termes.size()) {
                        Monome otherTerme = p.termes.get(indexOther);
                        addPoly.termes.add(otherTerme);
                        addPoly.degre = otherTerme.getExposant();
                        indexOther++;
                }
                return addPoly;
        }

        /**
         *  Calcule et renvoie la valeur de ce <code>PolynomeCreux</code> pour 
         * la valeur du paramètre spécifié. Par exemple, si p est le polynome
         * P(x) = 2 + 5x + 3x^2 alors p.evaluer(2) renvoie la valeur 24.
         *
         * @param  x  Valeur du paramètre du polynome.
         * @return    Valeur de ce polynome pour la valeur spécifiée.
         *
         * @ensures estZero() ==> (\result == 0);
         * @ensures (x == 0) ==> (\result == getCoefficient(0));
         * @ensures (x == 1) ==> (\result == (\sum int exp; exp >= 0 && exp <= getDegre();
         *						getCoefficient(exp)))
         *
         * @pure
         */
        /*@
	  @ ensures estZero() ==> (\result == 0);
	  @ ensures (x == 0) ==> (\result == getCoefficient(0));
	  @ ensures (x == 1) ==> (\result == (\sum int exp; exp >= 0 && exp <= getDegre();
	  @						getCoefficient(exp)));
	  @
	  @ pure
        @*/
        public double evaluer(double x) {
                double somme = 0;
                for (Monome m : this.termes) {
                        somme += m.getCoefficient() * Math.pow(x, m.getExposant());
                }
                return somme;
        }

        /**
         *  Renvoie le coefficient du terme de ce <code>PolynomeCreux</code> dont 
         * l'exposant est l'entier spécifié.
         *
         * @param  exposant  L'exposant du terme dont on cherche la valeur du coefficient.
         * @return           Le coefficient du terme dont l'exposant est spécifié.
         * 
         * @requires exposant >= 0;
         * @ensures (* \result est le coefficient du terme d'exposant exposant
         *			de ce polynome *);
         * @ensures (!estZero() && (exposant == getDegre())) ==> (\result != 0);
         * @ensures estZero() ==> (\result == 0);
         * @pure
         */
        /*@
	  @ requires exposant >= 0;
	  @ ensures (* \result est le coefficient du terme d'exposant exposant
	  @			de ce polynome *);
	  @ ensures (!estZero() && (exposant == getDegre())) ==> (\result != 0);
	  @ ensures estZero() ==> (\result == 0);
	  @
	  @ pure
        @*/
        public int getCoefficient(int exposant) {
                if (exposant > degre) {
                        return 0;
                }
                for (Monome m : termes) {
                        if (m.getExposant() == exposant) {
                                return m.getCoefficient();
                        }
                        if (m.getExposant() > exposant) {
                        	return 0;
                        }
                }
                return 0;
        }

        /**
         *  Renvoie le degré de ce <code>PolynomeCreux</code>, c'est-à-dire le 
         * plus grand exposant pour lequel le terme de ce <code>PolynomeCreux</code> 
         * a un coefficient non nul. Si ce <code>PolynomeCreux</code> est constant
         * la valeur retournée est 0, y compris s'il s'agit du polynome zero 
         * (i.e. le polynome dont tous les coefficients sont nuls).
         *
         * @return    Le degré de ce <code>PolynomeCreux</code>.
         *
         * @ensures \result >= 0;
         * @ensures estZero() ==> \result == 0;
         * @ensures !estZero() ==> getCoefficient(\result) != 0;
         * @pure
         */
        /*@
	  @ ensures \result >= 0;
	  @ ensures estZero() ==> \result == 0;
	  @ ensures !estZero() ==> getCoefficient(\result) != 0;
	  @
	  @ pure
        @*/
        public int getDegre() {
                return degre;
        }

        /**
         *  Renvoie <code>true</code> si et seulement si l'objet spécifié est un 
         * <code>PolynomeCreux</code> de mêmes termes que ce <code>PolynomeCreux</code>.
         *
         * @param  o  L'objet &agrave; comparer avec ce <code>PolynomeCreux</code>.
         * @return    <code>true</code> si et seulement si l'objet spécifié est 
         *	un <code>PolynomeCreux</code> de mêmes termes que ce 
         *	<code>PolynomeCreux</code> ; <code>false</code> sinon.
         *
         * @ensures !(o instanceof PolynomeCreux) ==> !\result;
         * @ensures (o instanceof PolynomeCreux)
         * ==> (\result <==> (getDegre() == ((PolynomeCreux) o).getDegre())
         *		    && (\forall int exp; exp >= 0 && exp <= getDegre();
         *				((PolynomeCreux) o).getCoefficient(exp) 
         *				 == this.getCoefficient(exp)));
         * @ensures \result ==> (this.hashCode() == o.hashCode());
         * @pure
         */
        /*@ also
	  @ 	ensures !(o instanceof PolynomeCreux) ==> !\result;
	  @	ensures \result ==> (this.hashCode() == o.hashCode());
	  @ also 
	  @ 	requires (o instanceof PolynomeCreux);
	  @ 	ensures (\result <==> ((getDegre() == ((PolynomeCreux) o).getDegre())
	  @		    && (\forall int exp; exp >= 0 && exp <= getDegre();
	  @			((PolynomeCreux) o).getCoefficient(exp) == this.getCoefficient(exp))));
	  @ pure
        */
        public boolean equals(Object o) {
                if (! (o instanceof PolynomeCreux)) {
                        return false;
                }

                if (this == o) {
                        return true;
                }

                PolynomeCreux autrePoly = (PolynomeCreux) o;
                if (getDegre() != autrePoly.getDegre()) {
                        return false;
                }
                if (this.termes.size() != autrePoly.termes.size()) {
                	return false;
                }
                for (int index = 0; index < this.termes.size(); index++) {
                	if (!this.termes.get(index).equals(autrePoly.termes.get(index))) {
                		return false;
                	}
                }
                return true;
        }

        /**
         *  Renvoie le code de hashage de ce <code>PolynomeCreux</code>.
         *
         * @return    Le code de hashage de ce <code>PolynomeCreux</code>.
         * @pure
         */
        //@ pure
        public int hashCode() {
                int code = 1;
                for (int exp = 0; exp <= getDegre(); exp++) {
                        code = 31 * code + exp * getCoefficient(exp);
                }
                return code;
        }

        /**
         *  Renvoie une brève représentation textuelle de ce <code>PolynomeCreux</code> 
         * de la forme "PolynomeCreux: 3.X^3 + 56.X^15". Si ce <code>PolynomeCreux</code> 
         * est zero (i.e. <code>this.estZero()</code> est <code>true</code>) 
         * la chaine "Poly: zero" est retournée.
         *
         * @return    Une brève représentation textuelle de ce <code>PolynomeCreux</code>.
         *
         * @ensures \result != null;
         * @pure
         */
        /*@
	  @ ensures \result != null;
	  @
	  @ pure
        @*/
        public String toString() {
                String strRep = "Poly:";
                if (estZero()) {
                        return strRep + " zero";
                }
                int exp = 0;
                while (getCoefficient(exp) == 0) {
                        exp++;
                }
                strRep += " " + termToString(exp);
                for (exp = exp + 1; exp <= getDegre(); exp++) {
                        if (getCoefficient(exp) != 0) {
                                strRep += " + " + termToString(exp);
                        }
                }
                return strRep;
        }
	/**
	 *  Renvoie une représentation textuelle du terme de ce polynome dont
	 *  l'exposant est spécifié.
	 *
	 * @param  exp  Exposant du terme dont on souhaite obtenir la représentation
	 *      textuelle
	 * @return      Une représentation textuelle du terme spécifié de ce polynome
	 * @requires exp >= 0;
	 * @ensures \result != null;
	 * @pure
	 */
	/*@
	  @ requires exp >= 0;
	  @ ensures \result != null;
	  @ pure
	  @*/
	private String termToString(int exp) {
		return Integer.toString(getCoefficient(exp))
			 + ".X^" + Integer.toString(exp);
	}
}

