/**
 *  Polynomes non modifiables &agrave; coefficients entiers de la forme c0 +
 *  c1*x +c2*x^2 + ...
 *
 * @invariant getDegre() >= 0;
 *
 * @author     Marc Champesme
 * @version    17 mars 2012
 * @since      17 mars 2012
 */
public abstract class Polynome {
        /*@
	  @ invariant getDegre() >= 0;
        @*/
        private int degre;
        
        /**
         * Constructeur à l'usage exclusif des sous-classes permettant 
         * d'initialiser les attributs définis dans cette classe.
         *
         * @param degre Le degré du Polynome.
         *
         * @requires degre >= 0;
         * @ensures getDegre() == degre;
         */
        /*@
          @ requires degre >= 0;
          @ ensures getDegre() == degre;
          @*/
        public Polynome(int degre) {
        	this.degre = degre;
        }
        
        /**
         *  Renvoie le degré de ce polynome, c'est-&agrave;-dire le plus grand exposant
         *  de coefficient non nul. Si ce polynome est constant la valeur retournée est
         *  0, y compris s'il s'agit du polynome zero (i.e. le polynome dont tous les
         *  coefficients sont nuls).
         *
         * @return    Le degré de ce polynome
         *
         * @ensures estZero() ==> \result == 0;
         * @ensures !estZero() ==> getCoefficient(\result) != 0;
         * @pure
         */
        /*@
	  @ ensures estZero() ==> \result == 0;
	  @ ensures !estZero() ==> getCoefficient(\result) != 0;
	  @
	  @ pure
        @*/
        public int getDegre() {
                return degre;
        }


        /**
         *  Renvoie true si et seulement si tous les coefficients de ce polynome sont
         *  nuls.
         *
         * @return    true si ce polynome est le polynome zero ; false sinon
         *
         * @ensures \result <==> ((getDegre() == 0) && (getCoefficient(0) == 0));
         * @pure
         */
        /*@
	  @ ensures \result <==> ((getDegre() == 0) && (getCoefficient(0) == 0));
	  @ pure
        @*/
        public boolean estZero() {
                return (getDegre() == 0) && (getCoefficient(0) == 0);
        }

        /**
         *  Renvoie un polynome correspondant &agrave; la somme de ce polynome
         *  et du polynome spécifié.
         *
         * @param  p  Le polynome &agrave; additionner avec ce polynome.
         * @return    La somme de ce polynome avec le polynome spécifié.
         *
         * @requires p != null;
         * @requires (\forall int exp; exp >= 0 && exp <= Math.min(this.getDegre(), p.getDegre());
         *		(this.getCoefficient(exp) < (Integer.MAX_VALUE - p.getCoefficient(exp)))
         *		&& (this.getCoefficient(exp) > (Integer.MIN_VALUE - p.getCoefficient(exp)))
         *		);
         * @ensures \result != null;
         * @ensures p.estZero() ==> \result.equals(this);
         * @ensures this.estZero() ==> \result.equals(p);
         * @ensures (this.getDegre() != p.getDegre()) ==> (\result.getDegre() == Math.max(this.getDegre(), p.getDegre()));
         * @ensures (\forall int exp; exp >= 0 && exp <= \result.getDegre();
         *			\result.getCoefficient(exp) == this.getCoefficient(exp) + p.getCoefficient(exp));
         * @ensures (this.getDegre() == p.getDegre())
         * && ((this.getCoefficient(this.getDegre()) + p.getCoefficient(p.getDegre())) != 0)
         *			==> (\result.getDegre() == this.getDegre());
         * @ensures (this.getDegre() == p.getDegre())
         * && ((this.getCoefficient(this.getDegre()) + p.getCoefficient(p.getDegre())) == 0)
         *			==> (\result.getDegre() < this.getDegre());
         *
         * @pure
         */
        /*@
	  @ requires p != null;
	  @ requires (\forall int exp; exp >= 0 && exp <= Math.min(this.getDegre(), p.getDegre());
	  @		((this.getCoefficient(exp) < 0) && (p.getCoefficient(exp) < 0))
	  @			==> (this.getCoefficient(exp) > (Integer.MIN_VALUE - p.getCoefficient(exp))));
	  @ requires (\forall int exp; exp >= 0 && exp <= Math.min(this.getDegre(), p.getDegre());
	  @		((this.getCoefficient(exp) > 0) && (p.getCoefficient(exp) > 0))
	  @ 			==> (this.getCoefficient(exp) < (Integer.MAX_VALUE - p.getCoefficient(exp))));
	  @ ensures \result != null;
	  @ ensures p.estZero() ==> \result.equals(this);
	  @ ensures this.estZero() ==> \result.equals(p);
	  @ ensures (this.getDegre() != p.getDegre()) 
	  @		==> (\result.getDegre() == Math.max(this.getDegre(), p.getDegre()));
	  @ ensures (\forall int exp; exp >= 0 && exp <= \result.getDegre();
	  @			\result.getCoefficient(exp) == this.getCoefficient(exp) + p.getCoefficient(exp));
	  @ ensures ((this.getDegre() == p.getDegre()) 
	  @		&& ((this.getCoefficient(this.getDegre()) + p.getCoefficient(p.getDegre())) != 0))
	  @	==> (\result.getDegre() == this.getDegre());
	  @ ensures ((this.getDegre() == p.getDegre()) && (this.getDegre() > 0)
	  @		&& ((this.getCoefficient(this.getDegre()) + p.getCoefficient(p.getDegre())) == 0))
	  @	==> (\result.getDegre() < this.getDegre());
	  @
	  @ pure
        @*/
        public abstract Polynome additionner(Polynome p);

        /**
         *  Calcule et renvoie la valeur de ce polynome pour la valeur du paramètre
         *  spécifiée
         *
         * @param  x  Valeur du parametre du polynome
         * @return    Valeur de ce polynome pour la valeur spécifiée
         *
         * @ensures estZero() ==> (\result == 0);
         * @ensures (x == 0) ==> (\result == getCoefficient(0));
         * @ensures (x == 1) ==> (\result == (\sum int exp; exp >= 0 && exp <= getDegre();
         *						getCoefficient(exp)))
         *
         * @pure
         */
        /*@
	  @ ensures estZero() ==> (\result == 0);
	  @ ensures (x == 0) ==> (\result == getCoefficient(0));
	  @ ensures (x == 1) ==> (\result == (\sum int exp; exp >= 0 && exp <= getDegre();
	  @						getCoefficient(exp)));
	  @
	  @ pure
        @*/
        public double evaluer(double x) {
                double somme = 0;

                for (int exp = 0; exp <= getDegre(); exp++) {
                        somme += getCoefficient(exp) * Math.pow(x, exp);
                }

                return somme;
        }

        /**
         *  Renvoie le coefficient du terme de ce polynome dont l'exposant est l'entier
         *  spécifié.
         *
         * @param  exposant  L'exposant du terme cherché.
         * @return           Le coefficient du terme dont l'exposant est spécifié
         * 
         * @requires exposant >= 0;
         * @ensures (* \result est le coefficient du terme d'exposant exposant
         *			de ce polynome *);
         * @ensures (!estZero() && (exposant == getDegre())) ==> (\result != 0);
         * @ensures estZero() ==> (\result == 0);
         * @pure
         */
        /*@
	  @ requires exposant >= 0;
	  @ ensures (* \result est le coefficient du terme d'exposant exposant
	  @			de ce polynome *);
	  @ ensures (!estZero() && (exposant == getDegre())) ==> (\result != 0);
	  @ ensures estZero() ==> (\result == 0);
	  @ ensures (exposant > getDegre()) ==> (\result == 0);
	  @
	  @ pure
        @*/
        public abstract int getCoefficient(int exposant);


        /**
         *  Renvoie true si et seulement si l'objet spécifié est un Polynome de
         *  m&ecirc;mes coefficients que ce polynome.
         *
         * @param  o  L'objet &agrave; comparer avec ce polynome
         * @return    true si et seulement si l'objet spécifié est un Polynome de
         *      m&ecirc;mes coefficients que ce polynome ; false sinon.
         *
         * @ensures !(o instanceof Polynome) ==> !\result;
         * @ensures (o instanceof Polynome)
         * ==> (\result <==> (getDegre() == ((Polynome) o).getDegre())
         *		    && (\forall int exp; exp >= 0 && exp <= getDegre();
         *				((Polynome) o).getCoefficient(exp) == this.getCoefficient(exp)));
         * @ensures \result ==> (this.hashCode() == o.hashCode());
         * @pure
         */
        /*@ also
	  @ 	ensures !(o instanceof Polynome) ==> !\result;
	  @	ensures \result ==> (this.hashCode() == o.hashCode());
	  @ also 
	  @ 	requires (o instanceof Polynome);
	  @ 	ensures (\result <==> ((getDegre() == ((Polynome) o).getDegre())
	  @		    && (\forall int exp; exp >= 0 && exp <= getDegre();
	  @				((Polynome) o).getCoefficient(exp) == this.getCoefficient(exp))));
	  @ pure
        */
        public boolean equals(Object o) {
                if (! (o instanceof Polynome)) {
                        return false;
                }

                if (this == o) {
                        return true;
                }

                Polynome autrePoly = (Polynome) o;
                if (getDegre() != autrePoly.getDegre()) {
                        return false;
                }

                boolean precedentsEgaux = true;
                for (int exp = 0; exp <= getDegre() && precedentsEgaux; exp++) {
                        precedentsEgaux = (getCoefficient(exp) == autrePoly.getCoefficient(exp));
                }
                return precedentsEgaux;
        }

        /**
         *  Renvoie le code de hashage de ce polynome
         *
         * @return    le code de hashage de ce polynome
         * @pure
         */
        //@ pure
        public int hashCode() {
                int code = 1;
                for (int exp = 0; exp <= getDegre(); exp++) {
                        code = 31 * code + exp * getCoefficient(exp);
                }
                return code;
        }

        /**
         *  Renvoie une brève représentation textuelle de ce polynome de la forme
         *  "Polynome: 3.X^3 + 56.X^7". Si ce polynome est zero (i.e. this.estZero() est
         *  true) la chaine "Polynome: zero" est retournée.
         *
         * @return    une brève représentation textuelle de ce polynome
         *
         * @ensures \result != null;
         * @pure
         */
        /*@
	  @ ensures \result != null;
	  @
	  @ pure
        @*/
        public String toString() {
                String strRep = getClass() + ":";
                if (estZero()) {
                        return strRep + " zero";
                }
                int exp = 0;
                while (getCoefficient(exp) == 0) {
                        exp++;
                }
                strRep += " " + termToString(exp);
                for (exp = exp + 1; exp <= getDegre(); exp++) {
                        if (getCoefficient(exp) != 0) {
                                strRep += " + " + termToString(exp);
                        }
                }
                return strRep;
        }

        /**
         *  Renvoie une représentation textuelle du terme de ce polynome dont
         *  l'exposant est spécifié.
         *
         * @param  exp  Exposant du terme dont on souhaite obtenir la représentation
         *      textuelle
         * @return      Une représentation textuelle du terme spécifié de ce polynome
         *
         * @requires exp >= 0;
         * @ensures \result != null;
         * @pure
         */
        /*@
	  @ requires exp >= 0;
	  @ ensures \result != null;
	  @ pure
        @*/
        private String termToString(int exp) {
                return Integer.toString(getCoefficient(exp)) + ".X^" + Integer.toString(exp);
        }
}

