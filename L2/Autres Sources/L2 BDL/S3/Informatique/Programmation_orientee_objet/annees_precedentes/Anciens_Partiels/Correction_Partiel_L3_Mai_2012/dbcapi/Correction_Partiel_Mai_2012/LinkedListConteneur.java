/**
 * 
 */
package examMai2012;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * 
 * 
 * @author Marc Champesme
 * @since 12 mai 2012
 * @version 12 mai 2012
 */
public class LinkedListConteneur<E> extends Conteneur<E> {

	private LinkedList<E> contenu;

	/**
	 * Initialise un <code>LinkedListConteneur</code> vide.
	 * 
	 * @ensures estVide();
	 */
	public LinkedListConteneur() {
		contenu = new LinkedList<E>();
	}

	/**
	 * Initialise un <code>LinkedListConteneur</code> de même contenu que la
	 * <code>Collection</code> spécifié. La <code>Collection</code> spécifiée
	 * doit être non <code>null</code>.
	 * 
	 * @requires c != null;
	 * @ensures this.equals(c);
	 * 
	 * @param c
	 *            La <code>Collection</code> contenant les éléments à placer
	 *            dans ce <code>LinkedListConteneur</code>.
	 * @throws NullPointerException
	 *             si la <code>Collection</code> spécifiée est <code>null</code>
	 *             .
	 * 
	 */
	public LinkedListConteneur(Collection<? extends E> c) {
		contenu = new LinkedList<E>(c);
	}

	/**
	 * Initialise un <code>LinkedListConteneur</code> de même contenu que le
	 * <code>Conteneur</code> spécifié. Le <code>Conteneur</code> spécifié doit
	 * être non <code>null</code>.
	 * 
	 * @requires c != null;
	 * @ensures this.equals(c);
	 * 
	 * @param c
	 *            Le <code>Conteneur</code> contenant les éléments à placer dans
	 *            ce <code>LinkedListConteneur</code>.
	 * @throws NullPointerException
	 *             si le <code>Conteneur</code> spécifié est <code>null</code>.
	 * 
	 */
	public LinkedListConteneur(Conteneur<? extends E> c) {
		contenu = new LinkedList<E>();
		for (E elt : c) {
			contenu.add(elt);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see examMai2012.Conteneur#ajouter(java.lang.Object)
	 */
	@Override
	public void ajouter(E obj) {
		if (obj == null) {
			throw new NullPointerException(
					"L'élément à ajouter doit être non null");
		}
		contenu.add(obj);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see examMai2012.Conteneur#getNbObjets()
	 */
	@Override
	public int getNbObjets() {
		return contenu.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see examMai2012.Conteneur#iterator()
	 */
	@Override
	public Iterator<E> iterator() {
		return contenu.iterator();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see examMai2012.Conteneur#retirer(java.lang.Object)
	 */
	@Override
	public boolean retirer(Object obj) {
		return contenu.remove(obj);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see examMai2012.Conteneur#vider()
	 */
	@Override
	public void vider() {
		contenu.clear();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see examMai2012.Conteneur#clone()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object clone() {
		LinkedListConteneur<E> leClone = null;
		try {
			leClone = (LinkedListConteneur<E>) super.clone(); // Unchecked cast
																// inévitable
		} catch (CloneNotSupportedException e) {
			throw new InternalError("Ne devrait pas se produire");
		}
		leClone.contenu = (LinkedList<E>) contenu.clone(); // Unchecked cast
															// inévitable
		return leClone;
	}

}
