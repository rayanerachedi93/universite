import java.util.ArrayList;

/**
 * Un <code>PersonnageMobile</code> est un personnage caractérisé par son nom
 * et une collection de <code>Piece</code> parmi lesquelles il se déplace. La
 * collection de <code>Piece</code> associée à un tel personnage doit contenir
 * au minimum trois pièces distinctes de sorte que à chacun de ses déplacements
 * le personnage se situe dans une pièce différente de la pièce dans laquelle il
 * était précédement. Cette collection de pièces peut changer au cours du temps
 * dans la mesure où ces pièces sont toujours, au moins, au nombre de trois et
 * qu'elles sont toutes deux à deux distinctes (au sens de <code>equals</code>).
 * L'ordre dans lequel le <code>PersonnageMobile</code> explore les
 * différentes pièces qui lui sont associées n'est pas spécifié, cependant les
 * contraintes suivantes doivent être respectées à chaque déplacement:
 * <ul>
 * <li>Après un déplacement, la pièce dans lequel se trouve le personnage est
 * différente (au sens de <code>equals</code>) de la pièce dans laquelle il
 * était avant le déplacement.</li>
 * <li>Lorsqu'une pièce est ajoutée à la collection de pièces associée au
 * <code>PersonnageMobile</code>, le premier déplacement de ce
 * <code>PersonnageMobile</code> ayant lieu après l'ajout ne doit pas aboutir
 * dans la pièce nouvellement ajoutée.</li>
 * </ul>
 * 
 * @invariant getNbPieces() >= 3;
 * @invariant getPieceCourante() != null;
 * @invariant peutAtteindrePiece(getPieceCourante());
 * @invariant !peutAtteindrePiece(null);
 * 
 * @author Marc Champesme
 * @since 28 février 2008
 * @version 28 février 2008
 * 
 */
public class PersonnageMobile implements Cloneable {
	private String nom;
	private ArrayList<Piece> mesPieces;
	private Piece dernierAjout;
	private int indexPieceCourante;
	private Piece pieceCourante;

	/**
	 * Initialise un nouveau <code>PersonnageMobile</code> dont le nom est la
	 * chaîne de caractères spécifiée et dont la collection de
	 * <code>Piece</code> associée est composée des trois pièces spécifiées.
	 * Les trois pièces spécifiées doivent être deux à deux distinctes. Les
	 * quatre paramètres spécifiés doivent être non null.
	 * 
	 * @requires nom != null;
	 * @requires p1 != null && p2 != null && p3 != null;
	 * @requires !p1.equals(p2) && !p1.equals(p3) && !p2.equals(p3);
	 * @ensures getNom().equals(nom);
	 * @ensures getNbPieces() == 3;
	 * @ensures peutAtteindrePiece(p1) && peutAtteindrePiece(p2) && peutAtteindrePiece(p3);
	 * @ensures getPieceCourante().equals(p1) || getPieceCourante().equals(p2) 
	 *          || getPieceCourante().equals(p3);
	 * 
	 * @param nom
	 *            Le nom de ce <code>PersonnageMobile</code>
	 * @param p1
	 *            Une des pièces associées à ce <code>PersonnageMobile</code>
	 * @param p2
	 *            Une des pièces associées à ce <code>PersonnageMobile</code>
	 * @param p3
	 *            Une des pièces associées à ce <code>PersonnageMobile</code>
	 */
	public PersonnageMobile(String nom, Piece p1, Piece p2, Piece p3) {
		this.nom = nom;
		mesPieces = new ArrayList<Piece>();
		mesPieces.add(p1);
		mesPieces.add(p2);
		mesPieces.add(p3);
		pieceCourante = p1;
		indexPieceCourante = 0;
	}

	/**
	 * Initialise un nouveau <code>PersonnageMobile</code> dont le nom est la
	 * chaîne de caractères spécifiée et dont la collection de
	 * <code>Piece</code> associée est composée de toutes les pièces contenues
	 * dans le tableau spécifié. Les pièces contenues dans le tableau doivent
	 * être deux à deux distinctes. Les deux paramètres spécifiés ainsi que tous
	 * les éléments du tableau doivent être non null. La taille du tableau de
	 * <code>Piece</code> spécifié doit être au moins trois.
	 * 
	 * @requires nom != null;
	 * @requires (tabPieces != null) && (tabPieces.length >= 3);
	 * @requires (\forall int i,j; i >= 0 && i < j && j < tabPieces.length;
	 *           			(tabPieces[i] != null) && (tabPieces[j] != null) 
	 *           			&& !tabPieces[i].equals(tabPieces[j]));
	 * @ensures getNom().equals(nom);
	 * @ensures getNbPieces() == tabPieces.length;
	 * @ensures (\forall int i; i >= 0 && i < tabPieces.length;
	 *          			peutAtteindrePiece(tabPieces[i]));
	 * @ensures (\exists int i; i >= 0 && i < tabPieces.length;
	 *          			getPieceCourante().equals(tabPieces[i]));
	 * 
	 * @param nom
	 *            Le nom de ce <code>PersonnageMobile</code>
	 * @param tabPieces
	 *            Le tableau de <code>Piece</code> contenant les pièces à
	 *            associer à ce <code>PersonnageMobile</code>.
	 */
	public PersonnageMobile(String nom, Piece[] tabPieces) {
		this.nom = nom;
		mesPieces = new ArrayList<Piece>(tabPieces.length);
		for (Piece p : tabPieces) {
			mesPieces.add(p);
		}
		pieceCourante = tabPieces[0];
		indexPieceCourante = 0;
	}

	/**
	 * Renvoie le nom de ce <code>PersonnageMobile</code>.
	 * 
	 * @ensures \result != null;
	 * 
	 * @return Le nom de ce <code>PersonnageMobile</code>.
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Renvoie la <code>Piece</code> dans laquelle se trouve ce
	 * <code>PersonnageMobile</code> au moment de l'appel de cette méthode.
	 * 
	 * @ensures \result != null;
	 * @ensures peutAtteindrePiece(\result);
	 * 
	 * @return La <code>Piece</code> dans laquelle se trouve ce
	 *         <code>PersonnageMobile</code>.
	 */
	public Piece getPieceCourante() {
		return pieceCourante;
	}

	/**
	 * Renvoie le nombre de <code>Piece</code> parmi lesquelles se déplace ce
	 * <code>PersonnageMobile</code>.
	 * 
	 * @ensures \result >= 3;
	 * 
	 * @return Le nombre de <code>Piece</code> parmi lesquelles se déplace ce
	 *         <code>PersonnageMobile</code>.
	 */
	public int getNbPieces() {
		return mesPieces.size();
	}

	/**
	 * Renvoie <code>true</code> si la <code>Piece</code> spécifiée est une
	 * des <code>Piece</code> que ce <code>PersonnageMobile</code> peut
	 * atteindre au cours de ses déplacements.
	 * 
	 * @ensures (p == null) ==> !\result;
	 * @ensures getPieceCourante().equals(p) ==> \result;
	 * 
	 * @param p
	 *            <code>Piece</code> dont on cherche à savoir si elle fait
	 *            partie du parcours de ce <code>PersonnageMobile</code>.
	 * 
	 * @return <code>true</code> si la <code>Piece</code> spécifiée peut
	 *         être atteinte par ce <code>PersonnageMobile</code> ;
	 *         <code>false</code> sinon.
	 */
	public boolean peutAtteindrePiece(Piece p) {
		return mesPieces.contains(p);
	}

	/**
	 * Ajoute la <code>Piece</code> spécifiée aux pièces que ce
	 * <code>PersonnageMobile</code> peut atteindre lors de ses déplacements.
	 * La <code>Piece</code> spécifiée ne sera ajoutée effectivement que si
	 * cette <code>Piece</code> ne fait pas déjà partie du parcours de ce
	 * <code>PersonnageMobile</code> et que ce paramètre est non
	 * <code>null</code>.
	 * 
	 * @ensures ((p != null) && !\old(peutAtteindrePiece(p))) <==> \result;
	 * @ensures !\result <==> (getNbPieces() == (\old(getNbPieces())));
	 * @ensures \result <==> peutAtteindrePiece(p);
	 * @ensures \result <==> (getNbPieces() == (\old(getNbPieces()) + 1);
	 * @ensures getPieceCourante().equals(\old(getPieceCourante()));
	 * 
	 * @param p
	 *            La <code>Piece</code> à ajouter au parcours de ce
	 *            <code>PersonnageMobile</code>.
	 * @return <code>true</code> si la <code>Piece</code> a pu être ajoutée ;
	 *         <code>false</code> sinon.
	 */
	public boolean ajouterPiece(Piece p) {
		if (mesPieces.contains(p)) {
			return false;
		}
		dernierAjout = p;
		return mesPieces.add(p);
	}

	/**
	 * Retire la <code>Piece</code> spécifiée des pièces que ce
	 * <code>PersonnageMobile</code> peut atteindre lors de ses déplacements.
	 * La <code>Piece</code> spécifiée ne sera retirée effectivement que si
	 * cette <code>Piece</code> fait déjà partie du parcours de ce
	 * <code>PersonnageMobile</code>, qu'il ne s'agit pas de la
	 * <code>Piece</code> dans laquelle se trouve actuellement ce
	 * <code>PersonnageMobile</code>, que le parcours de ce
	 * <code>PersonnageMobile</code> comprends plus de trois pièces et que ce
	 * paramètre est non <code>null</code>.
	 * 
	 * @ensures \result <==> (\old(peutAtteindrePiece(p)) 
	 * 			&& !\old(getPieceCourante().equals(p)) 
	 * 			&& (\old(getNbPieces()) > 3));
	 * @ensures \result <==> (getNbPieces() == (\old(getNbPieces()) - 1));
	 * @ensures \result ==> !peutAtteindrePiece(p);
	 * @ensures peutAtteindrePiece(p) <==> (\old(getPieceCourante().equals(p)) 
	 * 			|| ((\old(getNbPieces()) == 3) 
	 * 			    && \old(peutAtteindrePiece(p)));
	 * @ensures !\result <==> (getNbPieces() == (\old(getNbPieces())));
	 * @ensures getPieceCourante().equals(\old(getPieceCourante()));
	 * 
	 * @param p
	 *            <code>Piece</code> à retirer du parcours de ce
	 *            <code>PersonnageMobile</code>.
	 * 
	 * @return <code>true</code> si la <code>Piece</code> spécifiée a été
	 *         retirée du parcours de ce <code>PersonnageMobile</code> ;
	 *         <code>false</code> sinon.
	 */
	public boolean retirerPiece(Piece p) {
		if (getNbPieces() > 3) {
			if (p.equals(dernierAjout)) {
				dernierAjout = null;
			}
			return mesPieces.remove(p);
		} else {
			return false;
		}
	}

	/**
	 * Déplace ce <code>PersonnageMobile</code> vers une autre
	 * <code>Piece</code> choisie parmi la collection de <code>Piece</code>
	 * associées à ce <code>PersonnageMobile</code>. La <code>Piece</code>
	 * dans laquelle se trouve ce <code>PersonnageMobile</code> après
	 * déplacement est différente (au sens de <code>equals</code>) de celle
	 * dans laquelle il se trouvait avant déplacement. Si une <code>Piece</code>
	 * a été ajoutée à son parcours depuis son dernier déplacement, il est
	 * garantit que le <code>PersonnageMobile</code> ne se déplacera pas vers
	 * cette <code>Piece</code> récement ajoutée.
	 * 
	 * @ensures !getPieceCourante().equals(\old(getPieceCourante()));
	 */
	public void deplacer() {
		int nextIndex = indexPieceCourante;
		Piece prochainePiece;
		do {
			nextIndex = (nextIndex + 1) % mesPieces.size();
			prochainePiece = mesPieces.get(nextIndex);
		} while (prochainePiece.equals(pieceCourante)
				|| prochainePiece.equals(dernierAjout));
		indexPieceCourante = nextIndex;
		pieceCourante = prochainePiece;
		dernierAjout = null;
	}

	/**
	 * Renvoie un tableau contenant l'ensemble des pièces figurant dans le
	 * parcours actuel de ce <code>PersonnageMobile</code>. La taille du
	 * tableau renvoyé est égale au nombre de pièces faisant actuellement partie
	 * du parcours de ce <code>PersonnageMobile</code>.
	 * 
	 * @ensures \result != null;
	 * @ensures \result.length == getNbPieces();
	 * @ensures (\forall int i; i >= 0 && i < \result.length;
	 *		peutAtteindrePiece(\result[i]));
	 * 
	 * @return Un tableau dont la taille est le nombre de pièces du parcours de
	 *         ce <code>PersonnageMobile</code> et contenant toutes les pièces
	 *         de son parcours.
	 */
	public Piece[] getTabPieces() {
		return mesPieces.toArray(new Piece[mesPieces.size()]);
	}

	/**
	 * Compare ce <code>PersonnageMobile</code> avec l'objet spécifié et
	 * renvoie <code>true</code> si et seulement si cet objet est un
	 * <code>PersonnageMobile</code> de même nom possédant les mêmes pièces
	 * (au sens de <code>equals</code>) et se trouvant actuellement dans la même
	 * <code>Piece</code>. L'ordre dans lequel seraient parcourues les pièces
	 * par les deux personnages n'est pas pris en compte dans la comparaison. Il
	 * est donc possible que deux <code>PersonnageMobile</code> <code>equals</code>
	 * parcourent les pièces qui leur sont associées dans des ordres différents.
	 * 
	 * @ensures !(o instanceof PersonnageMobile) ==> !\result;
	 * @ensures \result <==> ((o instanceof PersonnageMobile) 
	 * 		&& (getNbPieces() == ((PersonnageMobile) o).getNbPieces())
	 * 		&& getPieceCourante().equals(((PersonnageMobile) o).getPieceCourante()) 
	 * 		&& (\forall Piece p; peutAtteindrePiece(p);
	 *				((PersonnageMobile) o).peutAtteindrePiece(p)));
	 * @ensures \result ==> (this.hashCode() == o.hashCode());
	 * 
	 * @param o
	 *            L'objet à comparer en terme d'égalité avec ce
	 *            <code>PersonnageMobile</code>
	 * @return <code>true</code> si l'objet spécifié est un
	 *         <code>PersonnageMobile</code> de même nom et auquel les mêmes
	 *         pièces sont associées que ce <code>PersonnageMobile</code>.
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o) {
		if (!(o instanceof PersonnageMobile)) {
			return false;
		}
		PersonnageMobile m = (PersonnageMobile) o;
		if (!getNom().equals(m.getNom()) || (getNbPieces() != m.getNbPieces())) {
			return false;
		}
		if (!getPieceCourante().equals(m.getPieceCourante())) {
			return false;
		}
		for (Piece p : mesPieces) {
			if (!m.peutAtteindrePiece(p)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Renvoie une nouvelle instance de
	 * <code>PersonnageMobile</code> <code>equals</code> à ce
	 * <code>PersonnageMobile</code>. Comme indiqué dans le contrat de la
	 * méthode <code>equals</code> le clone de ce
	 * <code>PersonnageMobile</code> porte le même nom, se trouve dans la même
	 * <code>Piece</code> et est associé à la même collection de pièces. Il
	 * n'est cependant pas garantie que la nouvelle instance renvoyée parcourt
	 * ces pièces dans le même ordre.
	 * 
	 * @ensures \result != null;
	 * @ensures \result != this;
	 * @ensures \result.equals(this);
	 * @ensures \result.getClass().equals(this.getClass());
	 * 
	 * @return Un clone de ce <code>PersonnageMobile</code>.
	 * 
	 * @see java.lang.Object#clone()
	 */
	@SuppressWarnings("unchecked")
	public PersonnageMobile clone() {
		PersonnageMobile leClone = null;
		try {
			leClone = (PersonnageMobile) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new InternalError("Clone devrait fonctionner");
		}
		leClone.mesPieces = (ArrayList<Piece>) mesPieces.clone(); // Unchecked
		// cast
		// inévitable
		return leClone;
	}

	/**
	 * Renvoie un code de hashage pour ce <code>PersonnageMobile</code>.
	 * 
	 * @return Un code de hashage pour ce <code>PersonnageMobile</code>.
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		int leCode = 31 * nom.hashCode();
		leCode = 31 * (leCode + getPieceCourante().hashCode());
		for (Piece p : mesPieces) {
			leCode += p.hashCode();
		}
		return leCode;
	}

	/**
	 * Renvoie une représentation concise sous forme de chaîne de caractères de
	 * ce <code>PersonnageMobile</code>.
	 * 
	 * @ensures \result != null;
	 * 
	 * @return Une représentation de ce <code>PersonnageMobile</code> sous
	 *         forme de chaîne de caractères.
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "PersonnageMobile:" + nom + mesPieces;
	}
}
