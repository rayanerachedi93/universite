import java.util.Iterator;

/**
 *  <p>
 *
 *  Version restreinte et francis�e de l'interface <code>java.util.List</code>.
 *  </p>
 *
 * @author     Marc Champesme
 * @version    11 janvier 2005
 * @since      11 janvier 2005
 * @see        java.util.List
 */

public interface Liste<E> {
	/*@
	  @ invariant taille() >= 0;
	  @ invariant estVide() <==> (taille() == 0);
	  @*/

	/**
	 *  Renvoie un it�rateur en lecture seule (op�ration remove interdite).
	 *
	 * @return    un it�rateur sur les �l�ments de cette liste.
	 */
	/*@
	  @ ensures \result != null;
	  @
	  @ pure
	  @*/
//	public abstract Iterator<E> iterator();


	/**
	 *  Renvoie l'�l�ment situ� &agrave; l'index sp�cifi� dans cette liste.
	 *
	 * @param  index  index de l'�l�ment &agrave; renvoyer
	 * @return        l'�l�ment &agrave; la position sp�cifi� dans la liste.
	 */
	/*@
	  @ requires index >= 0 && index < taille();
	  @ ensures contient(\result);
	  @ ensures indexDe(\result) <= index;
	  @ pure
	  @*/
	public E get(int index);
	
	public int taille();
	public int capacite();

	/**
	 *  Renvoie <code>true</code> si cette liste contient l'�l�ment sp�cifi�. Plus
	 *  pr�cis�ment, renvoie <code>true</code> si et seulement si cette liste
	 *  contient au moins un �l�ment <code>e</code> tel que:
	 *  <ul>
	 *    <li> <code>e == null</code> si <code>element == null</code></li>
	 *    <li> <code>element.equals(e)</code> si <code>element != null</code></li>
	 *
	 *  </ul>
	 *
	 *
	 * @param  element  �l�ment dont la pr�sence dans cette liste doit etre test�e.
	 * @return          <code>true</code> si l'�lement sp�cifi� est pr�sent ;
	 *      <code>false</code> sinon.
	 */
	/*@
	  @ ensures (element == null)
	  @		==> (\result <==> (\exists int i; i >= 0 && i < taille();
	  @							get(i) == null));
	  @ ensures (element != null)
	  @		==> (\result <==> (\exists int i; i >= 0 && i < taille();
	  @						get(i).equals(element)));
	  @
	  @ pure
	  @*/
	public boolean contient(E element);
	/**
	 *  Recherche dans cette liste de la premi�re occurence de l'�l�ment sp�cifi�.
	 *  C'est la m�thode <code>equals</code> qui est utilis�e pour tester l'�galit�
	 *  de l'�l�ment cherch� avec les �l�ments de la liste.Plus pr�cis�ment,
	 *  renvoie le plus petit index <code>i</code> tel que:
	 *  <ul>
	 *    <li> <code>get(i) == null</code> si <code>element == null</code></li>
	 *
	 *    <li> <code>element.equals(get(i))</code> si <code>element != null</code>
	 *    </li>
	 *    <li> <code>i == -1</code> si aucun index ne satisfait les conditions
	 *    pr�c�dentes</li>
	 *  </ul>
	 *
	 *
	 * @param  element  �l�ment recherch�
	 * @return          l'index de la premi�re occurence de l'�l�ment dans cette
	 *      liste ; renvoie -1 si l'objet n'est pas trouv�.
	 */
	/*@
	  @ ensures \result == -1 <==> !contient(element);
	  @ ensures contient(element) <==> (\result >= 0 && \result < taille());
	  @ ensures (contient(element) && element == null) ==>  get(\result) == null;
	  @ ensures (contient(element) && element == null) ==>
	  @			(\forall int i; i >= 0 && i < \result; get(i) != null);
	  @ ensures (contient(element) && element != null) ==> element.equals(get(\result));
	  @ ensures (contient(element) && element != null) ==>
	  @ 			(\forall int i; i >= 0 && i < \result; !element.equals(get(i)));
	  @ pure
	  @*/
	//public int indexDe(E element);


	/**
	 *  Insertion dans la liste de l'�lement sp�cifi� &agrave; la position
	 *  sp�cifi�e. Le cas �ch�ant, d�cale vers la droite l'�l�ment actuellement
	 *  &agrave; cette position ainsi que tous les �l�ments suivant.
	 *
	 * @param  index    index auquel l'�l�ment sp�cifi� va etre ins�r�
	 * @param  element  �l�ment &agrave; ins�rer
	 */
	/*@
	  @ requires 0 <= index && index <= this.taille();
	  @ ensures element == this.get(index);
	  @ ensures (\forall int i; 0 <= i && i < index;
	  @		this.get(i) == \old(this.get(i)));
	  @ ensures (\forall int i; index <= i && i < \old(taille());
	  @		this.get(i+1) == \old(this.get(i)));
	  @
	  @*/
	public void ajouter(int index, E element);


	/**
	 *  Ajoute l'�l�ment sp�cifi� &agrave; la fin de la liste.
	 *
	 * @param  element  element &agrave; ajouter &agrave; cette liste.
	 */
	/*@
	  @ ensures taille() == \old(taille()) + 1;
	  @ ensures (this.get(\old(taille())) == element);
	  @ ensures (\forall int i; 0 <= i && i < \old(taille());
	  @				this.get(i) == \old(this.get(i)));
	  @*/
	public void ajouter(E element);

	public void ajouterTout(Liste<E> l);
	
	public boolean contientTout(Liste<E> l);


	/**
	 *  Remplace l'�l�ment de cette liste plac� &agrave; la position sp�cifi�e par
	 *  l'�l�ment sp�cifi�.
	 *
	 * @param  index    index de l'�l�ment &agrave; remplacer.
	 * @param  element  �l�ment &agrave; placer &agrave; la position sp�cifi�e.
	 * @return          l'�l�ment pr�c�dement situ� &agrave; la position sp�cifi�e.
	 */
	/*@
	  @ requires index >= 0 && index < taille();
	  @ ensures taille() == \old(taille());
	  @ ensures \result == \old(get(index));
	  @ ensures get(index) == element;
	  @ ensures (\forall int i; 0 <= i && i != index && i < taille();
	  @				this.get(i) ==  \old(this.get(i)));
	  @*/
	//public E set(int index, E element);


	/**
	 *  Enl�ve de cette liste l'�l�ment situ� &agrave; la position sp�cifi�. D�cale
	 *  vers la gauche tous les �l�ments situ�s apr�s l'�l�ment supprim�.
	 *
	 * @param  index  l'index de l'�l�ment &agrave; enlever
	 * @return        l'�l�ment enlev� de la liste
	 */
	/*@
	  @ requires index >= 0 && index < taille();
	  @ ensures \result == \old(get(index));
	  @ ensures taille() == \old(taille()) - 1;
	  @ ensures (\forall int i; 0 <= i && i < index;
	  @		this.get(i) == \old(this.get(i)));
	  @ ensures (\forall int i; index < i && i < \old(taille());
	  @		this.get(i-1) == \old(this.get(i)));
	  @*/
	//public E retirer(int index);


	/**
	 *  Enl�ve tous les �l�ments de cette liste.
	 */
	/*@
	  @ ensures estVide();
	  @*/
	//public void vider();


	/**
	 *  Teste si cette liste ne contient aucun �l�ment.
	 *
	 * @return    <code>true</code> si cette liste ne contient aucun �l�ment ;
	 *      <code>false</code> sinon.
	 */
	/*@
	  @ ensures \result <==> (taille() == 0);
	  @ pure
	  @*/
	public boolean estVide();



	/**
	 *  Renvoie le nombre d'�l�ments dans cette liste.
	 *
	 * @return    le nombre d'�l�ments dans cette liste.
	 */
	/*@
	  @ ensures \result >= 0;
	  @ pure
	  @*/
	//public int taille();



	/**
	 *  Teste l'�galit� entre l'objet sp�cifi� et cette liste. Renvoie <code>true</code>
	 *  si et seulement si l'objet sp�cifi� est aussi une <code>Liste</code>
	 *  , que les deux listes ont le m&ecirc;me nombre d'�l�ments et que toutes les
	 *  paires d'�l�ments correspondants dans les deux listes sont <i>equal</i> .
	 *  En d'autres termes, deux listes sont dites <i>equal</i> si elle contiennent
	 *  les m&ecirc;mes elements dans le m&ecirc;me ordre.
	 *
	 * @param  obj  l'objet &agrave; comparer avec cette liste.
	 * @return      <code>true</code> si l'objet sp�cifi� est <i>equal </i>
	 *      &agrave; cette liste.
	 */
	/*@
	  @ also
	  @ ensures (obj instanceof Liste && this.taille() == ((Liste)obj).taille())
	  @		==> (\result
	  @			<==> (\forall int i; 0 <= i && i < this.taille();
	  @				(this.get(i) == null && ((Liste)obj).get(i) == null)
	  @				|| (this.get(i).equals(((Liste)obj).get(i)))));
	  @ ensures (this == obj) ==> \result;
	  @ ensures !(obj instanceof Liste && this.taille() == ((Liste)obj).taille())
	  @			==> !\result;
	  @ ensures (obj == null) ==> !\result;
	  @ pure
	  @*/
	//public boolean equals(Object obj);


	/**
	 *  Renvoie la valeur du code de hashage de cette liste. Le calcul de ce code
	 *  de hashage s'inspire de celui d�crit dans la documentation de l'interface
	 *  {@link java.util.List#hashCode() List} de la mani�re suivante: <pre>
	 *	hashCode = 1;
	 *	Iterator i = list.iterator();
	 *	while (i.hasNext()) {
	 *		Object obj = i.next();
	 *		hashCode = 31*hashCode + (obj==null ? 0 : obj.hashCode());
	 *	}
	 *</pre> <p>
	 *
	 *  Ce calcul assure que <code>list1.equals(list2)</code> implique que <code>list1.hashCode()==list2.hashCode()</code>
	 *  pour toutes listes, <code>list1</code> et <code>list2</code>, comme le
	 *  sp�cifie le contrat de <code>Object.hashCode</code>.</p>
	 *
	 * @return    la valeur du code de hashage pour cette liste.
	 */
	//@ pure
	//public int hashCode();

}

