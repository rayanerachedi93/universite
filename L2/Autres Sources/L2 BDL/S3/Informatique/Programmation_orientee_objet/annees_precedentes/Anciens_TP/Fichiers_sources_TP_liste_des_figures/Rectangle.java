import java.awt.Point;
/**
 *Modélise un Rectangle
 *
 * @author Valentina Dragos
 * @version 19 Mars 2007
  * Représentation de parallélogrammes dont les côtés consécutifs
 * sont perpendiculaires entre eux. Toute instance de rectangle a une
 * largeur et une hauteur strictement positive. Les côtés sont parallèles
 * aux axes de coordonnées.
 */
public class Rectangle extends Polygone
{
    private Point coinSupGauche;
    private int largeur;
    private int hauteur;
    

   /**
	 * Initialise un nouveau rectangle dont le coin supérieur gauche est
	 * le point spécifié et dont la largeur et la hauteur sont les valeurs 
	 * spécifiées. Le point spécifié doit être non null et les paramètres 
	 * largeur et hauteur doivent être strictement positifs.
	 * 
	 * @param coinSupGauche le coin supérieur gauche du nouveau rectangle
	 * @param largeur largeur du nouveau rectangle
	 * @param hauteur hauteur du nouveau rectangle
	 */
	   public Rectangle() {
			}
			
	   public Rectangle(Point coinSupGauche, int largeur, int hauteur) {
		this.coinSupGauche = coinSupGauche;
		this.largeur = largeur;
		this.hauteur = hauteur;
	}
	
	
	/** 
	 * Remplace la largeur de ce rectangle par la largeur
	 * spécifiée. Si la valeur spécifiée est inférieure ou égale
	 * à zéro, ne fait rien et laisse la largeur inchangée.
	 *
	 * @param  newLargeur nouvelle valeur de la largeur
	 */
	 
	public void setLargeur(int newLargeur) {
	
		}
		
	/** 
	 * Remplace la hauteur de ce rectangle par la hauteur
	 * spécifiée. Si la valeur spécifiée est inférieure ou égale
	 * à zéro, ne fait rien et laisse la hauteur inchangée.
	 *
	 * @param  newHauteur nouvelle valeur de la hauteur
	 */
	 
	public void setHauteur(int newHauteur) {
	
		
		}
		
		public boolean equals(Object obj) {
		if (!(obj instanceof Rectangle)) {
			return false;
		}
		return true; 
		
		//Rectangle rect = (Rectangle) obj;
		//return coinSupGauche.equals(rect.coinSupGauche)
//			&& (largeur == rect.largeur)
	//		&& (hauteur == rect.hauteur);
	}

	/** 
	 * Calcule le périmètre du rectangle 
	 */
	 
	public int getPerimetre() {
		return 2*(largeur+hauteur);
	}
	


	/** 
	 * Affiche le type  
	 */
		
	public String getType() {
		return "rectangle";
	}
}
