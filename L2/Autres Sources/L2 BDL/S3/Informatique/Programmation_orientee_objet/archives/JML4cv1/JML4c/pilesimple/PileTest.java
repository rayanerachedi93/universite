package pilesimple;


/**
 *      Classe pour tester la classe PileInt 
 *      @author Marc Champesme
 *      @version 1.5.0
 *
 */

public class PileTest {
	/**
	 *  Top Level activation Method
	 *
	 * @param  args  Parametres de la ligne de commande
	 */
	public static void main(String args[]) {
		PileInt maPile;

		maPile = new PileInt(10);
               System.out.println("Test de la classe PileInt");
                System.out.println("Empiler 5:");
                maPile.empiler(5);
                System.out.println("Sommet de pile: " + maPile.getSommet());
                System.out.println("Empiler 6:");
                maPile.empiler(6);
                System.out.println("Sommet de pile: " + maPile.getSommet());
                System.out.println("Depiler 2 fois:");
                maPile.depiler();
                maPile.depiler();
                if (maPile.estVide()) {
                    System.out.println("Pile Vide");
                }
                // Test violation pre-condition
                maPile.depiler();
                System.out.println("On vient de depiler une pile vide !");
                System.out.println("Sommet de pile ? " + maPile.getSommet());
	}
}

