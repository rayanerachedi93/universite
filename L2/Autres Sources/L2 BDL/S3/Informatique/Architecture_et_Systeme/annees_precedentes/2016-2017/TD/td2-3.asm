;;;  Licence 2 INFO - Univ Paris 13
;;;  Stefano Guerrini
;;;  AA 2015-16
;;; 
;;;  TD 2 - exercice 3
;;;  22/09/2015
	
	.ORIG x3000
	LEA R0, VAL		; R0 := &VAL, pointer à VAL
	LDR R1, R0, 0		; R1 := mem[&VAL]
	LDR R2, R0, 1		; R2 := mem[&VAL+1]
 	ADD R1, R1, R2		; R1 := mem[&VAL] + mem[&VAL+1]
	LDR R2, R0, 2		; R2 := mem[&VAL+2]
	ADD R1, R1, R2		; R2 := mem[&VAL] + mem[&VAL+1] + mem[&VAL+2]
	STR R1, R0, 3		; mem[&VAL+3] := mem[&VAL] + mem[&VAL+1] + mem[&VAL+2]
	HALT
	;; on initialise trois locations de memoire
VAL:	.FILL 7
	.FILL -13
	.FILL 8
	;;  on laisse une location pour le resultat
	.BLKW 1
	.END
