/*
 * Stefano Guerrini
 * AA 2015-16
 *  
 * TP 1 - exercice 1
 * 15/09/2015
 */

#include <stdio.h>

int main(int argc, char *argv[]) {
  int k;
  unsigned char bit;

  /* lecture des données */
  printf("Valeur qu'on veut convertir en binaire : ");
  scanf("%d", &k);
  printf("Les bits de la représentation binaire de %d\n(du plus faible au plus fort) : \n", k);
  
  /* la conversion :
   *   le bit plus faible est le reste de la division par 2 de k
   *   (bit = 1 si k impair, bit = 0 si pair)
   *   après on divise par 2 k et on obtient un nombre dont 
   *   la représentation binaire est la séquence de bits
   *   à gauche de le bit plus faible
   */
  do {
    bit = k % 2;
    k = k / 2;
    putchar('0'+bit);    /* on affiche le bit */
  } while (k != 0);
  
  putchar('\n');
}
