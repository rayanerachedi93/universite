;;;  Archi & Sys
;;;  Licence 2 INFO - Univ Paris 13
;;;  Stefano Guerrini
;;;  AA 2015-16
;;; 
;;;  TD 2 - exercice 2, point 1
;;;  22/09/2015
	
	.ORIG x3000
	;; if (R0 != 0) then ... else ... 
	ADD R0, R0, 0		; on prpeare le bit PZN pour le test
	BRZ ELSE		; R0 = 0, alors on passe au else
	;; branche then: R1 := -1
	AND R1, R1, 0
	ADD R1, R1, -1
	;;  end branche then
	BRNZP ENDITE		; on saute la branche else
	;; branche else: R1 := 1
ELSE:	AND R1, R1, 0
	ADD R1, R1, 1
	;; on continue
ENDITE:	HALT
	.END
