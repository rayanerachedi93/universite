#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>

//Incorrect?

int main()
{
	pid_t fils1, fils2;
	int fd[2], tube;

	if((tube=pipe(fd))==-1)
	{
		printf("Erreur tube\n");
		exit(1);
	}
	if((fils1=fork())==-1)
	{
		printf("Erreuf fork1\n");
		exit(1);
	}
	if(fils1==0)
	{ //Fils 1
		close(fd[0]);
		dup2(fd[1], 1);
		close(fd[1]);
		execl("/bin/cat", "cat", (char*) NULL);
		printf("Erreur execl cat\n");
		exit(1);
	}

	if((fils2=fork())==-1)
	{
		printf("Erreuf fork2\n");
		exit(1);
	}
	if(fils2==0)
	{	//Fils 2
		//close(fd[1]);
		dup2(0, fd[0]);
		//dup2(1, fd[1]);
		execl("/usr/bin/sort", "sort", (char*) NULL);
		printf("Erreur execl sort\n");
		exit(1);
	}
	close(fd[0]);
	close(fd[1]);
	wait(NULL);
	printf("Pid père : %d\n", getpid());
	printf("\tPid cat : %d\n", fils1);
	printf("\tPid sort : %d\n", fils2);
	return 0;
}
