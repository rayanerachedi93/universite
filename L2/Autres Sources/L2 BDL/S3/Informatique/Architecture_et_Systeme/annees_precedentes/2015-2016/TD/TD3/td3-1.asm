;;;  Licence 2 INFO - Univ Paris 13
;;;  Stefano Guerrini
;;;  AA 2015-16
;;; 
;;;  TD 3 - exercice 1
;;;  29/09/2015
	
	.ORIG x3000
	LD R0, C1		; RO := C1
	LD R1, C3		; R1 := C3
	NOT R1, R1
	ADD R1, R1, 1		; R1 := -C3
	ADD R0, R0, R1		; R0 := C1 - C3
	LD R1, C2		; R1 := C2
	ADD R1, R1, 9		; R1 := C2 + 9
	ADD R1, R1, R1		; R1 := 2(C2+9)
	ADD R0, R0, R1		; R0 := (C1-C3)+2(C2+9)
	LD R1, K		; R1 := -128
	ADD R0, R0, R1		; resultat
	ST R0, RES
	HALT
K:	.FILL -128
C1:	.FILL 10
C2:	.FILL -4
C3:	.FILL 6
RES:	.BLKW 1
	.END
