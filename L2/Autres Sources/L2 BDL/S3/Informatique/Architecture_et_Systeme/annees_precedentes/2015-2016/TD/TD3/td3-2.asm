;;;  Licence 2 INFO - Univ Paris 13
;;;  Stefano Guerrini
;;;  AA 2015-16
;;; 
;;;  TD 3 - exercice 2
;;;  29/09/2015

	.ORIG x3000
	LD R1, ch		; R1 := ch
	LEA R2, str		; R2 := str

	;; boucle de recherche de ch dans str
loop:	LDR R0, R2, 0		; R0 := *R2
	BRZ notfound		; si *R2 = 0 on est arriv� � la fin de str
	;; test si *R2 = ch
	NOT R0, R0
	ADD R0, R0, 1           ; R0 := - *R2
	ADD R0, R1, R0		; R0 := ch - *R2
	BRZ found		; trouv� !
	;; on passe au caract�re suivant
	ADD R2, R2, 1
	;; on rep�te la boucle
	BR loop

	;; le caract�re a �t� trouv�
	;; on calcule �a position
	;; 	R2 = adresse du caract�re
	;; 	str = d�but da la cha�ne
	;; 	R2 - str = position du caract�re
found:	LEA R0, str		; R0 := str
	NOT R0, R0
	ADD R0, R0, 1
	ADD R0, R2, R0		; R0 := R2 - str
	BR end
	
	;; le caract�re n'a pas �t� trouv�
notfound:			; on remarque que R0 = 0
	ADD R0, R0, -1		; R0 := -1

end:	HALT

ch:	.FILL x0061		; 'a'
str:	.STRINGZ "villetaneuse"
	.END
