#include <stdio.h>
#include <stdlib.h>

int dec2bin(int k, unsigned n, unsigned char bits[]) {
  int i;
  for (i = 0; i < n && k != 0; i++, k /= 2)
    bits[i] = k % 2;
  return k;
}

void comp2(unsigned n, unsigned char bits[]) {
  int i;
  for (i = 0; i < n && bits[i] == 0; i++);
  for (i++; i < n; i++)
    bits[i] = (bits[i]+1) % 2;
}

void print_bits(unsigned n, unsigned char bits[]) {
  for (int i = n-1; i >= 0; i--)
    putchar('0'+bits[i]);
}

unsigned char addbin(unsigned n, unsigned char a1[],
	    unsigned char a2[], unsigned char r[]) {
  unsigned char rip = 0;
  for (int i = 0; i < n; i++) {
    int x = a1[i]+a2[i]+rip;
    r[i] = x % 2;
    rip = x / 2;
  }
  return rip;
}

#define NUMBITS 16
#define MAX (0x7FFF + 1)

int main(int argc, char *argv[]) {
  int n, k1, k2, mk, h;
  unsigned char a1[NUMBITS], a2[NUMBITS], r[NUMBITS];
  
  printf("Premier terme de la somme (entre %d et %d) :\n", -MAX, MAX);
  do {
    scanf("%d", &k1);
    if (k1 <= -MAX || k1 >= MAX)
      printf("Entree une valeur (entre %d et < %d) :\n", -MAX, MAX);
  } while (k1 <= -MAX || k1 >= MAX);
  if (k1 < 0) {
    dec2bin(-k1, NUMBITS-1, a1);
    comp2(NUMBITS, a1);
  } else
    dec2bin(k1, NUMBITS-1, a1);
  printf("En binaire :\n  ");
  print_bits(NUMBITS, a1);
  putchar('\n');

  printf("Deuxiem terme de la somme (entre %d et %d) :\n", -MAX, MAX);
  do {
    scanf("%d", &k2);
    if (k2 <= -MAX || k2 >= MAX)
      printf("Entree une valeur (entre %d et < %d) :\n", -MAX, MAX);
  } while (k2 <= -MAX || k2 >= MAX);
  if (k2 < 0) {
    dec2bin(-k2, NUMBITS-1, a2);
    comp2(NUMBITS, a2);
  } else
    dec2bin(k2, NUMBITS-1, a2);
  printf("En binaire :\n  ");
  print_bits(NUMBITS, a2);
  putchar('\n');

  printf("Somme : %d\n", k1+k2);
  if (k1+k2 < 2) {
    dec2bin(-k1-k2, NUMBITS, r);
    comp2(NUMBITS, r);
  } else
    dec2bin(k1+k2, NUMBITS, r);    
  printf("En binaire :\n  ");
  print_bits(NUMBITS, r);

  printf("\nSomme binaire :\n  ");
  print_bits(NUMBITS, a1);
  putchar('\n');
  printf("+ ");
  print_bits(NUMBITS, a2);
  putchar('\n');
  addbin(NUMBITS, a1, a2, r);
  printf("= ");
  print_bits(NUMBITS, r);
  putchar('\n');
}
