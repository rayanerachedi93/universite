#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
  pid_t pid;

  /* père */
  printf("+ %d [%d]\n", getpid(), getppid());

  if ((pid = fork()) == -1) {
    perror("fork 1");
    return 1;
  }

  if (pid == 0) {
    /* fils 1 */
    printf("  + %d [%d]\n", getpid(), getppid());
    if ((pid = fork()) == -1) {
      perror("fork 1");
      return 1;
    }
    if (pid == 0) {
      /* petit fils 1 */
      printf("    + %d [%d]\n", getpid(), getppid());
      sleep(1);
      printf("    - %d [%d]\n", getpid(), getppid());
      return 0;
    }
    /* fils 1 continue */
    wait(NULL);
    sleep(2);
    printf("  - %d [%d]\n", getpid(), getppid());
    return 0;
  }

  /* père continue */
  if ((pid = fork()) == -1) {
    perror("fork 1");
    return 1;
  }

  if (pid == 0) {
    /* fils 2 */
    printf("  + %d [%d]\n", getpid(), getppid());
    if ((pid = fork()) == -1) {
      perror("fork 2");
      return 1;
    }
    if (pid == 0) {
      /* petit fils 2 */
      printf("    + %d [%d]\n", getpid(), getppid());
      sleep(2);
      printf("    - %d [%d]\n", getpid(), getppid());
      return 0;
    }
    /* fils 2 continue */
    wait(NULL);
    printf("  - %d [%d]\n", getpid(), getppid());
    return 0;
  }

  /* père continue */
  wait(NULL);
  wait(NULL);
  printf("- %d [%d]\n", getpid(), getppid());
  return 0;
}
