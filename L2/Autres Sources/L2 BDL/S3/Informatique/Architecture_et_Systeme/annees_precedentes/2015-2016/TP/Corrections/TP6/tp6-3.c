#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/*
 * Compiler avec l'option
 *    -DNOWAIT
 * pour une version qui n'attend pas la terminaison du fils
 */

int main(int argc, char *argv[]) {
  char path[256];
  pid_t pid;

  while (1) {
    printf("Chemin : ");
    scanf("%255s", path);

    if ((pid = fork()) == -1) {
      perror("fork échoué");
      return -1;
    }
    
    if (pid == 0) {
      /* le fils */
      execl("/bin/ls", "ls", "-l", path, NULL);
      perror("exec échoué");
    }

#ifndef NOWAIT
    /* attend la terminaison du fils */
    wait(NULL);
#endif
    
    /* le père continue la bloucle */
  }
    
  }
