#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>

/*
 * La solution proposée prévoit l'affichage de plusieurs messages
 * pour mieux suivre l'évolution du programme
 *
 * On remarque que le fils attend l'arrivée de tout signal avec un pause()
 * Pour être sûr que les messages envoyés par le père (voir kill dans la 
 * partie du père) arrivent au fils quand il est déjà en pause, on
 * introduit des attentes (voir sleep(1)). Sinon, si le fils entre en pause
 * après l'arrivée du signal qu'il le devrait réveiller, il se mettra en pause
 * et aucun signal sera lui envoyé pour le réveiller. Au même temps, le
 * père attendra la fin du fils avec un wait(). Donc, dans cette situation le
 * programme ne terminera pas.
 *
 * Pour voir le problème, compiler le programme en définissant la variable
 * NOSLEEP, e.g.
 *
 *     gcc -DNOSLEEP tp8-3 -o tp8-3-nosleep
 *
 * Il est fort probable que en exécutant ce programme vous voyez
 * l'envoi des 7 signaux par le père. Mais, vu qu'ils arrivent tous avant
 * de la première pause du fils, aucun signal réveillera le fils quand il 
 * se mettra en pause. Au même temps, le père se bloquera sur le wait() 
 * en l'attente de la terminaison du fils, qui n'arrivera jamais car le fils
 * est à son tour en attente d'un signal pour se réveiller.
 */

void sighand(int sig) {
  printf("(sighand) code signal reçu : %d\n", sig);
}

int main(int argc, char *argv[]) {
  pid_t pid;
  
  if ((signal(SIGUSR1, sighand) == SIG_ERR)) {
    perror("signal");
  }

  if ((pid = fork()) == -1) {
    perror("Pas de fork\n");
    return 1;
  }

  if (pid == 0) {
    printf("(fils) init écoute signaux\n");
    for(int numr = 0; /* nombre de signaux reçus */
	numr < 7;
	numr++) {
      printf("(fils) j'attend le signal : %d\n", numr);
      pause(); /* le fils attend un signal */
      printf("(fils) signal reçu : %d\n", numr);      
    }
    printf("(fils) fini\n");
    return 0;
  }

  /* père */
  printf("(père) init envoye signaux\n");
  for(int nume = 0; /* nombre de signaux envoyés */
      nume < 7;
      nume++) {
#ifndef NOSLEEP
    sleep(1); /* le père attend à envoyer le signal
	       * pour permettre au fils de se mettre
	       * en pause
	       */
#endif
    if (kill(pid, SIGUSR1) == -1) 
      perror("kill");
    printf("(père) envoyé signal : %d\n", nume);
  }

  /* le père attend la fin du fils */
  wait(NULL);
  printf("(père) fini\n");
  
  return 0;
}
