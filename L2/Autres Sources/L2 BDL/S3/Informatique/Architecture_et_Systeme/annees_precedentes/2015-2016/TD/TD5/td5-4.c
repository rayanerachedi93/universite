#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

#define BUFSIZE 1024

int main(int argc, char *argv[]) {
  int fd;
  unsigned char buffer[BUFSIZE];
  int nbyte;

  if (argc == 1)
    fd = 0;
  else if ((fd = open(argv[1], O_RDONLY)) == -1) {
    fprintf(stderr, "Erreur ouverture fichier \"%s\n\"", argv[1]);
    exit(-1);
  }

  while ((nbyte = read(fd, buffer, 10)) > 0) {
    write(1, buffer, nbyte);
  }
  
  close(fd);
}
