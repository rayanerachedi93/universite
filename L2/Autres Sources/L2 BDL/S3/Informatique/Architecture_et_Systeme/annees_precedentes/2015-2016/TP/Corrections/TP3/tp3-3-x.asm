	.ORIG x3000

	LEA R0, vect
	LD R1,len
	LD R2, ch
	JSR compte
	HALT

	;; routine qui compte le nonbre
	;; d'occurrences d'une valeur dans un vecteur
	;; @param R0 : l'adresse du vecteur
	;; @param R1 : longueur du vecteur
	;; @param R2 : valeur dont on compte les occurrences
	;; @return R3 : nombre d'ocurrences
compte:	AND R3, R3, 0		; on initialise le compteur à 0
	;; R1 au départ contient la longeur du vectuer
	;; dans la routine sera mis à jour et il contiendra
	;; le nombre d'élements du vectuer qui reste à vérifier
	;; R0 aussi sera modifié et contiendra toujours le pointer
	;; au prochian élément du vecteur à vérifier
	ADD R1, R1, 0	; on veut tester R1
loop:	BRZ fini	; on a fini, il ne reste plus d'élements dans le vecteur
	;; on compare l'élement pointé par R0 avec R2
	LDR R4, R0, 0		; R4 = *R0
	NOT R4, R4
	ADD R4, R4, 1		; R4 = -*R0
	ADD R4, R2, R4		; R4 = R2 - *R0
	BRNP next		; si non 0 on n'a pas trouvé une occurrence de R2
	;; on a trouvé une occurrence, on incrémente le compteur
	ADD R3, R3, 1
	;; on passe au suivant
next:	ADD R0, R0, 1
	ADD R1, R1, -1
	;; on boucle
	BR loop
fini:	RET

	;; donnée pour un exemple
vect:	.STRINGZ "pomme de reinette et pomme d'api"
len:	.FILL 32
ch:	.FILL x0070		; caractére 'p'
	.END
