#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <signal.h>
#include <sys/wait.h>

int sig = 0;
int count_v[NSIG];

void sighand(int s) {
  sig = s;
  count_v[s]++;
}

void boucle_fils(pid_t ppid) {
  int s;
  
  do {
    sleep(1);
    printf("Signal [1-%d, 2 pour terminer] : ", NSIG-1);
    while (scanf("%d", &s) == 0);
    if (s < 1 || s >= NSIG) {
      printf("Code de signal invalide. Signal %d non envoyé.\n", s);
    } else if (s == SIGSTOP || s == SIGKILL) {
      printf("Les signaux SIGSTOP (%d) et SIGKILL (%d) sont ignorés.\n",
	     SIGSTOP, SIGKILL);
    } else {
      printf("Envoye signal au père %d\n", s);
      kill(ppid, s);
    }
  } while (s != SIGINT);
}

int main(int argc, char *argv[]) {
  int s;
  
  for(s = 0; s < NSIG; s++)
    count_v[s] = 0;
  
  if (fork() == 0) {
    /* fils */
    boucle_fils(getppid());
    printf("Fils termine.\n");
    return 0;
  }

  /* père */
  for(s = 1; s < NSIG; s++) {
    signal(s, sighand);
  }

  do {
    pause();
    printf("Père a reçu le signal %d.\n", sig);
  } while (sig != SIGINT);
  
  for(s = 1; s <= NSIG; s++) {
    if (count_v[s]) 
      printf("%2d : %2d\n", s, count_v[s]);
  }

  printf("Signaux reçus :\n");
  wait(NULL);
  return 0;
}
