#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
  printf("Nombre d'arguments dans la ligne de commande : %d\n", argc-1);
  printf("Nom du programme : %s\n", argv[0]);
  for (int i = 1; i < argc; i++)
    printf("Argument %d : %s\n", i, argv[i]);
}
