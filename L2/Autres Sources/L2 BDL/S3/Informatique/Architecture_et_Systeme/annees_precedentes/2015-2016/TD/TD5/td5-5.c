#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>

#define BUFSIZE 1024


int main(int argc, char *argv[]) {
  int fd;
  unsigned char buffer[BUFSIZE];
  int nbyte, size;
  char *msg =  "*********Affichage**********\n";

  if (argc < 3) {
    fprintf(stderr, "Nombre de parmètres insuffisant.\n Donner la taille des bloques d'affigchage et le fichier!\n");
    exit(1);
  }
  size = atoi(argv[1]);
  if (size <= 0 || size > 1024)
    size = 1024;

  if ((fd = open(argv[2], O_RDONLY)) == -1) {
    fprintf(stderr, "Erreur ouverture fichier \"%s\n\"", argv[2]);
    exit(-1);
  }

  while ((nbyte = read(fd, buffer, size)) > 0) {
    //    write(1, msg, strlen(msg));
    printf("%s", msg);
    fflush(stdout);
    write(1, buffer, nbyte);
    //    write(1, "\n", 1);
    printf("\n");
    fflush(stdout);
  }
  
  close(fd);
}
