#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc, char *argv[]) {
  pid_t pid;

  if ((pid = fork()) == -1) {
    perror("Pas de fork\n");
    return 1;
  }

  if (pid == 0) {
    printf("fils\n");
    pause();
    return 0;
  }
  
  printf("père\n");
}
