#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
	
int main(int argc, char const* argv[])
{
	DIR* dir;
	char dirName[100];
	int sizeof_Buffer = 100;
	char* buffer = (char*) malloc (sizeof_Buffer * sizeof(char));	
	if (argc < 2)
	{
		printf("Entrez le nom du dossier que vous souhaitez ouvrir\n> ");
		scanf("%s", dirName);
	}
	else
	{
		strcpy(dirName,argv[1]);
	}
	struct dirent* entDir;
	printf("Essayons d'ouvrir le répertoire ayant pour nom %s\n", dirName);
	if((dir = opendir(dirName)) == NULL)
	{
		printf("Echec à l'ouverture du répertoire\n");
		return EXIT_FAILURE;
	}
	while(( entDir = readdir(dir)) != NULL)
	{
		printf("Le fichier a pour nom : %s\n", entDir->d_name);
		printf("Son i-node est : %llu\n", entDir->d_ino);
		printf("Et il est de type : %u\n", entDir->d_type);
	}	
	closedir(dir);
	return EXIT_SUCCESS;
}
