#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>


int nsig[NSIG];

void handler(int sig);

int main()
{
  int i, s;
  handler(9);
  for(i=0; i<5; i++)
    sleep(1);

  for(s=1; s<NSIG; s++)
    signal(s, handler);
  return EXIT_SUCCESS;
}



void handler(int sig)
{
  ++nsig[sig];
  printf("New %d : %d fois\n", sig, nsig[sig]);
}
