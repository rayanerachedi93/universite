#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

/*
	@Author FLOQUET Nicolas		13/10/1997	11706348
	@Author CHANDARA Alexis		04/03/1999	11702812
*/

int main()
{
  int tube, fd[2];
  pid_t F1, F2, premier, deuxieme;
  if((tube=pipe(fd)==-1))
  {
    printf("Erreur Tube\n");
    exit(1);
  }
  if((F1=fork())==-1)
  {
    printf("Erreur Fork 1\n");
    exit(1);
  }
  if(F1==0)
  { //Fils 1
    int frere;
    close(fd[1]);
    read(fd[0], &frere, sizeof(pid_t));
    close(fd[0]);
    printf("Je suis le processus fils 1, avec %d. Mon père est le processus %d. Mon frère est le processus %d\n", getpid(), getppid(), frere);
    exit(0);
  }

  if((F2=fork())==-1)
  {
    printf("Erreur Fork 2\n");
    exit(1);
  }
  if(F2==0)
  { //Fils 2
    close(fd[0]);
    pid_t frere=F1;
    pid_t pidF2 = getpid();
    write(fd[1], &pidF2, sizeof(pid_t));
    close(fd[1]);
    printf("Je suis le processus fils 2, avec %d. Mon père est le processus %d. Mon frère est le processus %d\n", getpid(), getppid(), frere);
    exit(0);
  }

  //Père
  close(fd[0]);
  close(fd[1]);

  premier = wait(NULL);
  deuxieme = wait(NULL);

  printf("Je suis le processus père de %d.\n", getpid());
  if(F1==premier)
  {
    printf("Premier fils à terminer (fils 1) : %d.\n", F1);
    printf("Deuxième fils à terminer  (fils 2) : %d.\n", F2);
  }
  else if(F1==deuxieme)
  {
    printf("Premier fils à terminer (fils 2) : %d.\n", F2);
    printf("Deuxième fils à terminer  (fils 1) : %d.\n", F1);
  }

  return EXIT_SUCCESS;
}
