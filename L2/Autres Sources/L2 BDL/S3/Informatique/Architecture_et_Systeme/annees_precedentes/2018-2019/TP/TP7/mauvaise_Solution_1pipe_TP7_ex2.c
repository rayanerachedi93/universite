/*
  @Author Nicolas Floquet
  Ce programme demande à l'utilisateur d'entrer la taille d'un tableau d'entiers qu'il va remplir.
  Après avoir fait cela, le programme utilise un tube (pipe) pour échanger les données dans le tableau entre un processus fils et un père.
  Le fils se chargera de trouver la valeur maximale dans le tableau, la valeur minimale et la valeur moyenne du tableau.
  Une fois que le fils a fini de sous-traiter les données, il renvoit au père les 3 valeurs.
  Le père finalement affiche les données et le programme se finit.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

typedef struct sequence
{
  int nbElem;
  int* tab;
} sequence;

int main()
{
  pid_t fils;
  int i, max, min, taille, fd[2];
  float  moyenne;
  printf("Nous allons calculer les valeurs minimales, maximales, et la moyenne d'une séquence\n\n");
  do
  {
    printf("Quelle est la taille (entier strictement positif) de la séquence d'entiers que vous voulez entrer?\n");
    scanf("%d", &taille);
  }
  while(taille <= 0);
  sequence* seq = (sequence*) malloc(sizeof(sequence));
  seq->tab = (int*) malloc(sizeof(int)*taille);
  seq->nbElem = taille;
  for(i=0; i<seq->nbElem; i++)
  {
    printf("Quel est l'élément numéro %d de votre séquence\n> ", i);
    scanf("%d", &seq->tab[i]);
  }
  pipe(fd);
	if((fils=fork())==-1)
	{
		perror("Erreur création fils\n");
		exit(1);
	}
	if(fils==0)
	{
    //Fils
		int somme=0, valMin, valMax, i;
    const int tailleTab = seq->nbElem;      //Juste pour ne pas assigner à un tableau une taille qui est une variable
    int tabFils[tailleTab];
		float moyenneFils;
    read(fd[0], &tabFils, sizeof(int)*tailleTab);
    close(fd[0]);    //Fermeture de l'entrée du tube
    valMax=tabFils[0];
    valMin=tabFils[0];
    somme=tabFils[0];
    for(i=1; i<tailleTab; i++)
    {
      if(tabFils[i]>valMax)
        valMax=tabFils[i];
      if(tabFils[i]<valMin)
        valMin=tabFils[i];
      somme+=tabFils[i];
    }
    moyenneFils = (float) ((float)somme) / ((float)tailleTab);
    write(fd[1], &valMin, sizeof(int));
    write(fd[1], &valMax, sizeof(int));
    write(fd[1], &moyenneFils, sizeof(float));
    close(fd[1]);
    exit(0);
	}
  //Père
  write(fd[1], seq->tab, sizeof(int)*seq->nbElem);
  close(fd[1]);
  wait(NULL);             //Attendre fin du fils
  read(fd[0], &min, sizeof(int));
  read(fd[0], &max, sizeof(int));
  read(fd[0], &moyenne, sizeof(float));
  close(fd[0]);

  printf("La séquence était :\n");
  for(i=0; i<seq->nbElem-1;i++)
    printf("[%d]->", seq->tab[i]);
  printf("[%d]\n", seq->tab[i]);

  printf("La valeur minimale de votre séquence est %d\n", min);
  printf("La valeur maximale de votre séquence est %d\n", max);
  printf("La valeur moyenne de votre séquence est %f\n", moyenne);

  return EXIT_SUCCESS;
}
