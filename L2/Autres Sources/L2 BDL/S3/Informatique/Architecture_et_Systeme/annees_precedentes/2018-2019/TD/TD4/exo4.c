#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

int main()
{
	pid_t fils1, fils2;
	
	if((fils1=fork()) != 0)
	{
		if((fils2=fork()) != 0)
		{
			printf("Je suis le père. Mes fils sont %d et %d\n", fils1, fils2);
			printf("Je suis fils1 : %d\n", fils1);
			printf("Je suis fils2 : %d\n", fils2);
		}
	}
	exit(0);
}
