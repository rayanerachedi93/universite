#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include<sys/wait.h>

void sighand(int sig);

int continuer = 1;
pid_t fils;
pid_t pere;

int main ()
{
	int i;
	pere=getpid();
	signal(SIGUSR1, sighand);

	if((fils=fork())==-1)
	{
		perror("fork");
		exit(1);
	}
	if (fils!=0)
	{
		//Père
		for (i=2; i<=100; i+=2)
		{
			if (continuer)
				pause();
			continuer = 1;
			printf("%d\n", i);
			kill(fils, SIGUSR1);
		}
	}
	else
	{
		//Fils
		for (i=1; i<=100; i+=2)
		{
			printf("%d\n", i);
			kill(pere, SIGUSR1);
			if(continuer)
				pause();
			continuer = 1;
		}
	}
	return EXIT_SUCCESS;
}


void sighand(int sig)
{
	continuer=0;
}
