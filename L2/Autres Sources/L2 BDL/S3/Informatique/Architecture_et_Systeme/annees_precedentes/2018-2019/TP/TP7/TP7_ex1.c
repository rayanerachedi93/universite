#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

pid_t pere, F1, F2;

int main()
{
  int fd[2], tube;
  pere = getpid();
  if((tube = pipe(fd))==-1)
  {
    perror("Tube\n");
    exit(1);
  }
  if((F1=fork())==-1)
  {
    perror("Erreur fork 1\n");
    exit(1);
  }
  if(F1==0)
  {
    //Fils 1
    close(fd[0]);   //Fermeture de la sortie du tube
    dup2(fd[1], 1);
	 	execl("/bin/ps","ps","-aj", (char *) NULL);
    perror("Erreur fin fils 1\n");
    exit(1);
  }
  if((F2=fork())==-1)
    {
      perror("Erreur fork 2\n");
      exit(1);
    }
    if(F2==0)
    {
      //Fils 2
      close(fd[1]); //Fermeture de l'entrée du tube
      dup2( fd[0], 0);
      char* s1="[:upper:][:lower:]";
      char* s2="[:lower:][:upper:]";
  	 	execl("/usr/bin/tr","tr",s1, s2, (char *) NULL);
      perror("Erreur fin fils 2\n");
      exit(1);
    }

    //Fermer les entrées/sorties du tube avant de faire wait
    close(fd[0]);
    close(fd[1]);
    wait(NULL);
    wait(NULL);
    printf("%d : ps -aj | tr \"[:upper:][:lower:]\" \"[:lower:][:upper:]\"\n", pere);
    printf("%d : ps -aj\n", F1);
    printf("%d : tr \"[:upper:][:lower:]\" \"[:lower:][:upper:]\"\n", F2);

  return EXIT_SUCCESS;
}
