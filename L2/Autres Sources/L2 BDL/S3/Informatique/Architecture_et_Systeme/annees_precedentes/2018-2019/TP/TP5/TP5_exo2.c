#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include <sys/types.h>	

int main()
{
	/*
	pid_t granp, pere, fils;
	
	if((granp=fork()) != 0)
	{
		if((pere=fork()) != 0)
		{
			if((fils=fork()) != 0)
			{
				printf("C'est le fils qui parle \n Mon pid est %d\n", fils);
				printf("Le pid de mon père %d\n", pere);
			}
			else
			{
		
				printf("C'est le père qui parle \n Mon pid est %d\n", pere);
				printf("Le pid de mon père %d\n", granp);
			}
		}
	}
	*/
	
	pid_t process;
	if((process=fork()) == 0)
	{
		printf("C'est le fils qui parle, mon pid est %d\n", getpid());
		printf("Le pid de mon père %d\n", getppid());
		//printf("process est %d\n", process);
		exit(0);
		
	}
	
	//printf("process est %d\n", process);
	sleep(1);
	printf("C'est le père qui parle, mon pid est %d\n", getpid());
	printf("Le PID du grand-père de mon fils est  %d\n", getppid());
	
	exit(0);
}
