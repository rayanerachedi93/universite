#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

int main(int argc, char* const argv[])
{
	int nbrFork=0, i=0;
	if(argc < 2 || atoi(argv[1])<0)
	{
		do
		{
			printf("Combien de fork (entrez un entier positif)? \n");
			scanf("%d", &nbrFork);
		}while(nbrFork<1);
	}
	else
	{
		nbrFork = atoi(argv[1]);
	}
	pid_t pidfils;
	for(i=0; i<nbrFork; i++)
	{
		pidfils = fork();
	}
	printf("%d\n", pidfils);
	printf("Hello!\n");
	exit(0);
}
