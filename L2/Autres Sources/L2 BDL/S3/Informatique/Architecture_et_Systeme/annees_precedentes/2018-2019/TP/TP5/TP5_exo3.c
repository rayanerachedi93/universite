#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<wait.h>

int main()
{
	char path[100];
	int i;
	pid_t processus;
	while(1)
	{
		system("clear");
		printf("Entrez un nouveau chemin dans lequel vous voulez exécuter ls -l\n> ");
		scanf("%s", path);
		if((processus=fork())== 0)
		{
	  		execl("/bin/ls","ls","-l",path, (char *) NULL);
		}
		int attente = wait(NULL);
		printf("Le pid du processus %d, on a donc attendu la fin du processus %d avec la fonction wait\n", processus, attente);
		for(i=10; i>0; i--)
		{
			printf("%d\n", i);
			sleep(1);
		}
		system("clear");
	}
	exit(0);
}
