#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

/*
	@Author FLOQUET Nicolas		13/10/1997	11706348
	@Author CHANDARA Alexis		04/03/1999	11702812
*/

//Taille du buffer au début du programme
#define sizeof_Buffer 256

int main()
{
	char nomFichier1[100], nomFichier2[100], nomFichier3[100], buffer[sizeof_Buffer];
	int fd1, fd2, fd3, i, charLu, taille;

	//On s'assure que le buffer n'a aucun caractères résiduels qu'on ne voudrait pas
	for(i=0; i<sizeof_Buffer; i++)
	{
		buffer[i]='\0';
	}
	printf("Je vais vous demander le nom de 3 fichiers, nous allons concatener le contenu des 2 premiers dans le troisième\n");
	printf("\nAttention!! Toutes les données du troisième seront perdues\n");

	printf("Entrez le nom du premier fichier\n> ");
	scanf("%s", nomFichier1);
	printf("Entrez le nom du deuxième fichier\n> ");
	scanf("%s", nomFichier2);
	printf("Entrez le nom du troisième fichier\n> ");
	scanf("%s", nomFichier3);

	//f1
	if((fd1=open(nomFichier1, O_RDONLY))==-1)
	{
		printf("Erreur à l'ouverture du premier fichier : %s\n", nomFichier1);
		exit(1);
	}

	//f2
	if((fd2=open(nomFichier2, O_RDONLY))==-1)
	{
		printf("Erreur à l'ouverture du deuxième fichier : %s\n", nomFichier2);
		exit(1);
	}

	//f3
	if((fd3=open(nomFichier3, O_WRONLY | O_TRUNC))==-1)
	{
		printf("Erreur à l'ouverture du troisième fichier : %s\n", nomFichier3);
		exit(1);
	}
	while((charLu=read(fd1, buffer,sizeof_Buffer))!=0);
	close(fd1);
	//On trouve la taille de ce qu'on a réellement lu
	for(taille=0; buffer[taille]!='\0'; taille++);

	//On peut ré-écrire le fichier1 entièrement dans le fichier3 (taille -1 pour ne pas aller à la ligne inutilement)
	write(fd3, buffer, taille-1);

	//On s'assure de nouveau que le buffer n'a aucun caractères résiduels qu'on ne voudrait pas
	for(i=0; i<sizeof_Buffer; i++)
	{
		buffer[i]='\0';
	}
	while((charLu=read(fd2, buffer,sizeof_Buffer))!=0);
	close(fd2);
	//On peut ré-écrire le fichier2 entièrement dans le fichier3
	for(taille=0; buffer[taille]!='\0'; taille++);
	write(fd3, buffer, taille);

	close(fd3);

	return 0;

}
