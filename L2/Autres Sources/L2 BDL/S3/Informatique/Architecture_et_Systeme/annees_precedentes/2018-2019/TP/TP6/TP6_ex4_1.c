#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

int signaux[NSIG];
void handler(int sig);


int main()
{
	for(int i=0; i<NSIG; i++)
		signaux[i]=0;

  for(int sig=0; sig<NSIG; sig++)
    if(signal(sig, handler)==SIG_ERR);
  printf("pid : %d\n", getpid());
  while(1)
  {
    sleep(5);
  }
  return EXIT_SUCCESS;
}

void handler(int sig)
{
	printf(" Signal %d reçu %d fois\n", sig, ++signaux[sig]);
}
