#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char const* argv[])
{
	int fileSize = 0;
	char fileName[100];
	int sizeof_Buffer = 100;
	char* buffer = (char*) malloc (sizeof_Buffer * sizeof(char));
	if (argc < 2)
	{
		printf("Entrez un nom pour votre fichier : ");
		scanf("%s", fileName);
	}
	else
	{
		strcpy(fileName,argv[1]);
	}
	printf("Ouvrons le fichier de nom %s\n", fileName);

	//fd = file descriptor, décrit le fichier ouvert

	int fd = open(fileName, O_RDONLY | O_CREAT , S_IRUSR  | S_IWUSR);
	if(fd == -1)
	{
		printf("Erreur à l'ouverture du fichier\n");
		return EXIT_FAILURE;
	}
	int keepReading;
	do
	{
		keepReading=read(fd, buffer+fileSize, 1);
		fileSize++;
		//Changer fileSize == 100, en autre chose dependant de la taille du buffer
		if (fileSize == 100)
		{
			sizeof_Buffer *= 2;
			buffer = (char*) realloc(buffer, sizeof_Buffer);
		}
	}
	while(keepReading);
	write(1, buffer, fileSize);

	close(fd);
	printf("\n");
	return EXIT_SUCCESS;
}
