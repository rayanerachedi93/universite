; @author: Nicolas Floquet


.ORIG x3000

	LEA R2, X	; R2 = &X
	LEA R3, Y	; R3 = &Y
	LDR R0, R2, 0	; R0=X
	LDR R1, R3, 0	; R1=Y






	JSR max
	BR max_fini
	; Sous-routine qui va calculer Int1 - Int2
	; Si le résultat est supérieur à 0, alors Int1 est supérieur à Int 2 et on va sauter dans une sous-routine qui va mettre Int1 dans R0
	; Sinon on met Int2 dans R0
	; @param un entier dans R0
	; @param un entier dans R1
	max :
	
		NOT R1, R1		; R1=non(R1)
		ADD R1, R1, 1		; R1=-Int2
		ADD R0, R0, R1		; R0=Int1-Int2
		BRzp Int1Sup		; Si Int1-Int2 > 0, alors on veut R0=Int1
		NOT R1, R1		; Comme on veut renvoyer Int2 dans R0, on doit faire R1=Int2 par complément à deux, puis R0=R1
		ADD R1, R1, 1		; R1=Int2
		ADD R0, R1, 0		; R0=R1+0=R1
		RET


Int1Sup :NOT R1, R1		; Comme on veut renvoyer Int1 dans R0 et que R0=Int1-Int2, on doit faire R0+Int2, or on peut retrouver Int2 par complément à deux de R1
	ADD R1, R1, 1		; R1=Int2
	ADD R0, R0, R1		; R0=Int1
	RET


	BR max_fini			
max_fini : HALT			; La sous-routine max est fini



X : .FILL 50		;
Y : .FILL 60		;



.END
