; Floquet Nicolas 13/10/97 11706348
; Chandara Alexis 04/03/99 11702812
; Exercice 2
; Ce programme prend la valeur absolue d'un entier contenu dans la location de mémoire x et place sa valeur absolue dans y
; @param x qui contient une valeur entière
; @return la valeur absolue x dans y


.ORIG x3000
	LEA R0, x		; R0 contient l'adresse de x
	LDR R0, R0, 0		; R0 contient la valeur x
	BRn xEstNegatif		; Si la valeur de x est négative on va faire son complément à deux pour avoir sa valeur absolue
	; Sinon, x est positif et on renvoie directement sa valeur dans y
	STI R0, y		; Si x>=0, alors on fait y=x et y contient bien la valeur absolue de x
	HALT

xEstNegatif:
	NOT R0, R0		; R0=non(x)
	ADD R0, R0, 1		; R0=-x, donc la valeur absolue de x car x était négatif
	STI R0, y		; y = -x or x était négatif, donc y contient bien la valeur absolue de x
	HALT



x : .FILL -15			; On donne une valeur quelconque à x
y : .FILL y			; 


.END
