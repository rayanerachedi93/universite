#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main()
{
  if(fork() != 0)
    fork();
  else
    printf("hello + %d\n", getpid());
  printf("hello + %d\n", getpid());
  return 0;
}

//  printf("hello + %d\n", getpid());
