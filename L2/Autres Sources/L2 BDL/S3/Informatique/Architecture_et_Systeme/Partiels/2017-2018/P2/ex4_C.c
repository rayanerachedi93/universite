#include<stdio.h>
#include<stdlib.h>
#include <unistd.h>

int main()
{
  if(fork() != 0)
  {
    printf("hello + %d\n", getpid());
  }
  else
  {
    fork();
    printf("hello + %d\n", getpid());
  }
  exit(0);
}


  //  printf("hello + %d\n", getpid());
