; Programme qui prend 1 entier qui vaut 10 et un autre entier quelconque en paramètre
;	x:=10
;	while (x > 0)
;	{
;		x:= x-1		(x--)
;		y:=y*2
;	}
;@param x=10 un entier
; Si x != 10, le programme ne fonctionnera pas comme voulu
;@param y un autre entier

.ORIG x3000
	LD R0, x		; R0 = x
loop:	ADD R0, R0, 0
	BRnz R0vaut0
	ADD R0, R0, -1		; On désincrémente R0
	ST R0, x		; x est désincrémenté aussi
	LD R1, y		; R1 = y
	ADD R1, R1, R1		; R1 = R1 + R1 (R1*2)
	ST R1, y		; y:= y*2
	BR loop			;
	
	
	BR end
	
R0vaut0:			;
	ST R1, y		;
	BR end
	

end:	HALT



x : .FILL 10
y : .FILL 100

.END
