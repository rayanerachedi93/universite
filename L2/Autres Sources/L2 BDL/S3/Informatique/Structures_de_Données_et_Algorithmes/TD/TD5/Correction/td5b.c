#include <stdlib.h>
#include <stdio.h>

typedef struct st_t{
  int val;
  struct st_t *g, *d, *p;

}node;

void   inserer   (node **, int);     /* insere un element à l'arbre */
void   afficher  (node *);           /* affiche tout l'arbre en ordre croissant des clefs */
int    feuille   (node *);           /* retourne 1 si A est une feuille, 0 sinon */
void   freetree  (node **);          /* libere toute la memoire d'un arbre */
node * min       (node *);           /* retourne le noued qui est le minimum de l'arbre */
node * succ2     (node *, node *);   /* retourne le noeud successeur d'un certain noeud */

/****************************  Programme principal ***************************************/
int main()
{
  node * A = NULL;
  node * T, * X;

  inserer(&A,20);
  inserer(&A,10);
  inserer(&A,5);
  inserer(&A,15);
  inserer(&A,12);
  inserer(&A,17);
  inserer(&A,30);
  printf("\n");
  afficher(A);

  /*******************   test pour la fonction min(A) ************************/

  /*
  T = min (A);
  printf("\nle min de A est : %d\n",T->val);
  */

  /********************* test pour la fonction succ1(A,X) *********************/

  /* Ici X peut etre : A, A->d, A->g, A->g->g, A->g->d, A->g->d->g, A->g->d->d */

  
  /* 
  X = A->g->d->d;
  T = succ2(A, X);
  if (T != NULL) printf("\nsucc de %d est %d\n",X->val, T->val);
  else printf("\n%d n'a pas de successeur\n", X->val);
  */


  /* liberation de la memoire complete de l'arbre */
  freetree(&A);

  return 0;
}

/*******************************************************************************/

void inserer (node ** A, int x)
{
  if ((*A) == NULL){
    *A = (node *) malloc (sizeof(node));
    (*A)->val = x;
    (*A)->g = NULL;
    (*A)->d = NULL;
    (*A)->p = NULL;
  }
  else{
    if (x > (*A)->val) 
    {
      if ((*A)->d != NULL) inserer (&((*A)->d), x);
      else {inserer (&((*A)->d), x); (*A)->d->p = *A;}
    }
    else {
      if ((*A)->g != NULL) inserer (&((*A)->g), x);
      else {inserer (&((*A)->g), x); (*A)->g->p = *A;}
    }
  }
}

/*******************************************************************************/

void afficher (node * A)
{
  if (A != NULL)
  {
     afficher (A->g);
     printf("%d ", A->val);
     afficher (A->d);
  }
}

/*******************************************************************************/

int feuille (node *A)
{
  if ((A != NULL) && (A->g == NULL) && (A->d == NULL)) return 1;
  else return 0;
}

/*******************************************************************************/

void freetree (node ** A)
{
  if ((*A) != NULL)
  {
      freetree(&((*A)->g));
      freetree(&((*A)->g));
      node * P = *A;
      free(P);
      (*A) = NULL;
  }
}

/*******************************************************************************/

node * min (node * A)
{
  if (feuille(A)) return (A);
  else return (min(A->g));
}

/*******************************************************************************/

node * succ2 (node * A, node * X)
{
  node * P;

  
  if ((A == NULL) || (X == NULL) || ((A == X) && (A->d == NULL))) return NULL;
  if ((A == X) && (A->d != NULL)) return min(A->d); 
  if (X == A->g) 
  {
    if (X->d == NULL) return A;
    else return min(X->d);
  }
  P = X->p;
  if ((P != NULL) && (X != P->g))
  {
     if ((X == P->d) && (X->d != NULL)) return (min(X->d));
     while ((P != NULL) && (X == P->d))
     {
      X = P;
      P = X->p;
     }
     
  }
 
  return P;
}
