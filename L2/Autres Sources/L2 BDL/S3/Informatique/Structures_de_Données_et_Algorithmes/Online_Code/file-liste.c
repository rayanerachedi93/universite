#include <stdlib.h>
#include <stdio.h>

/* déclaration de la structure file */

typedef struct cellule_st{
   int val;
   struct cellule_st * suivant;
}cell;

typedef struct file_st{
  cell * debut;
  cell * fin;
}file;

/****** déclaration des fonctionnes  ***********************/

void enfiler (file **, int);
int  defiler (file **);

void exo1 (file **, file **);

void afficher_file (file *);

/************* programme principal **********************/

int main ()
{

  /* le programme enfile dans F1 les valeurs 5,3,10,1,25 et ensuite, l'exo1 vide F1 */
  /* et laisse dans F2 les valeurs dans F1 multiples de 5                           */

  file * F1 = NULL;
  file * F2 = NULL;

  enfiler(&F1,5);
  enfiler(&F1,3);
  enfiler(&F1,10);
  enfiler(&F1,1);
  enfiler(&F1,25);

  afficher_file (F1); printf("\n");
  
  exo1(&F1, &F2);

  printf("File F1 est : ");
  afficher_file (F1); printf("\n");
  printf("File F2 est : ");
  afficher_file (F2); printf("\n");

  return 0;
}

/************ fonctionnes ****************/
/*****************************************/
void afficher_file(file * F)
{
  cell * t;
  if (F == NULL) printf("file vide\n");
  else{
   t = F->debut;
  while (t != NULL){
   printf("%d ",t->val);
   t = t->suivant;
  }
  }
}
/*****************************************/
void enfiler (file ** F, int a)
{
   cell * new;

   new = (cell *) malloc (sizeof(cell));
   new->val = a;
   new->suivant = NULL;
   if (*F == NULL){
     *F = (file *) malloc (sizeof(file));
     (*F)->debut = new ;
     (*F)->fin = new;
   }
   else{
     (*F)->fin->suivant = new;
     (*F)->fin = new;
   }
   
}
/*****************************************/
int defiler (file ** F)
{
  int x;
  cell * temp;

  if (*F == NULL || (*F)->debut == NULL){
     printf("erreur file vide\n");
     return -1;
  }
  else{
  x = (*F)->debut->val;
  temp = (*F)->debut;
  (*F)->debut = (*F)->debut->suivant;
  if ((*F)->debut == NULL) (*F)->fin = NULL;
  free(temp);
  return x;
  }
}

/****************************************/
/* exo 3.1 TD3                          */
/****************************************/
void exo1 (file ** F1, file **F2)
{
  int a;
  while ((*F1)->debut != NULL)
  {
    a = defiler(F1);                 /* remarquez que ici on passe F1 et F2 tout court */     
    if ((a % 5) == 0) enfiler(F2,a); /* car F1 et F2 sont déjà de type "file ** "      */
  }                                  
}
/****************************************/
