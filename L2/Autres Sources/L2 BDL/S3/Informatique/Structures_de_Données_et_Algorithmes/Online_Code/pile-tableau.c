#include <stdlib.h>
#include <stdio.h>

/* Declaration de la structure de la pile */

typedef struct {
   unsigned int max;
   unsigned int cur;
   int * tab;
}pile;

/*******************************************/

void push (pile *, int);     /* empiler */
int  pop  (pile *);          /* dépiler */
pile * creer_pile();
int est_vide (pile *);

void exo1 (pile *, pile *, pile *);
void afficher_pile (pile *);
void effacer_pile(pile *);  /* On recuper de la memoire */


/************* ppal **********************/

int main ()
{

  /* On empile les éléments 5,3,2,1,4 dans la pile Pile1 et l'exo1 vide la Pile1 et laisse */
  /* les entiers pairs dans la pile Pile2 et les impairs dans la pile Pile3                */

  pile * P1 = creer_pile();
  pile * P2 = creer_pile();
  pile * P3 = creer_pile();

  push(P1,5);
  push(P1,3);
  push(P1,2);
  push(P1,1);
  push(P1,4);

	for(int i=0;i<10;i++)
	{
		push(P1,i);
	}
  printf("La pile 1 initialement est : ");
  afficher_pile (P1); printf("\n"); printf("\n");

  exo1(P1, P2, P3); /* exo 1.1 du TD 2 */

  printf("La pile 1 est : ");
  afficher_pile (P1); printf("\n");
  printf("La pile 2 est : ");
  afficher_pile (P2); printf("\n");
  printf("La pile 3 est : ");
  afficher_pile (P3); printf("\n");
  
  effacer_pile(P1); effacer_pile(P2); effacer_pile(P3);
  
  return 0;
}

/************ fonctionnes ****************/

/*****************************************/
pile * creer_pile ()
{
  pile * R = (pile *) malloc (sizeof(pile));
  R->max = 1;
  R->cur = 0;
  R->tab = (int *)malloc(sizeof(int)*R->max);
  return R;
}
/*****************************************/
int est_vide (pile * P)
{
  if (P == NULL || P->cur == 0) return 1;
  return 0;
}
/*****************************************/
void push(pile * P, int val)
{
   if (P->cur == P->max) {
      P->max *= 2;
      P->tab = (int * ) realloc(P->tab, P->max*sizeof(int)); 	//Code original n'avait pas *sizeof(int) et ne fonctionnait pas     
   }
   P->tab[P->cur] = val;
   P->cur++;
}
/*****************************************/
int pop (pile * P)
{
  if (P->cur == 0) {
    printf("Pile vide !\n");
    return -1;
  }
  P->cur--;
  return P->tab[P->cur];
}
/*****************************************/
void effacer_pile (pile * P)
{
  if (P != NULL)
  {
    free(P->tab);
    free(P);
  }
}
/*****************************************/
void afficher_pile (pile * P)
{
  int i;

  for (i=0; i < P->cur; i++)
   printf("%d ",P->tab[i]);
}

/*****************************************/
/* Exo 1.1 du TD 2                       */
/*****************************************/
void exo1 (pile * P1, pile * P2, pile * P3)
{
  int r;
  while (!est_vide(P1)){
    r = pop(P1);
    if (r % 2)
      push(P3, r);      /* ici les appels de pop et push se font directement */
    else push(P2, r);   /* sur P1, P2 et P3 qui sont déjà de type "cell **"   */
  }
}

