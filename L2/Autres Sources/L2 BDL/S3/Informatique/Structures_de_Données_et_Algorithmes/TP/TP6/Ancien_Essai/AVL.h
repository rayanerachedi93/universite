#ifndef _AVL_H_
#define _AVL_H_

#include<stdio.h>
#include<stdlib.h>


//Structure Node_st qui compose l'arbre binaire

typedef struct node_st
{
	struct node_st *gauche;
	struct node_st *droite;
	int val;
	int hauteur;
	int coeffEqui;
} node;


//!!!
//Malgré ce typedef, on continue à utiliser node* par soucis de compréhensibilité
typedef node* arbre;

//Si l'arbre est vide, on renvoie -1, sinon on renvoie la donnée qu'il contient
int clef(arbre A) {return (A!=NULL)? A->val : -1;};


//Déclaration des fonctions qui manipulent des arbres binaires de recherche

//__________________________________________

arbre creer (arbre fg, arbre fd, int v);

//__________________________________________

int estABR(node* A);

//__________________________________________

int AVLouPas(arbre* A);

//__________________________________________

void inserer(int v, arbre* A);

//__________________________________________

void equilibrer(arbre* A);

//__________________________________________

void rot_G (arbre* A);

//__________________________________________

void rot_D (arbre* A);

//__________________________________________
	
void afficher_en_ordre(arbre A);

//__________________________________________

void imprimer_un_arbre(arbre A);

//__________________________________________


void read_it_all(arbre A);

//__________________________________________

int chercher(int v, arbre A);

//__________________________________________

int valeur_minimum(arbre A);

//__________________________________________

arbre noeud_minimum(arbre A);

//__________________________________________


int valeur_maximum(arbre A);

//__________________________________________

arbre noeud_maximum(arbre A);

//__________________________________________

int nombre_de_noeuds(arbre A);

//__________________________________________

int hauteur(arbre A);

//__________________________________________

node* pere(arbre A, node* x);

//__________________________________________

node* succ(arbre A, node* x);

//__________________________________________

int nombre_de_feuilles(arbre A);

//__________________________________________

int nombre_noeuds_internes(arbre A);

//__________________________________________

void miroir (arbre A);

//__________________________________________

int compare (arbre A1, arbre A2);

//__________________________________________

int k_eme (arbre A, int k);

//__________________________________________

int est_somme (arbre A);

//__________________________________________
int supprimer(int v, arbre* A);

//__________________________________________

int max(int x, int y);

//__________________________________________

int val_absolue(int x);

//__________________________________________

void free_arbre(arbre A);

//__________________________________________






#endif
