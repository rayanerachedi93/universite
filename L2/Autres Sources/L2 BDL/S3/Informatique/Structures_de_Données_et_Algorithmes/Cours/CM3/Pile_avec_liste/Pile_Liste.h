#ifndef _PILE_LISTE_
#define _PILE_LISTE_

#include<stdio.h>
#include<stdlib.h>


//Déclaration des Structures

//"Dé-Référencement" du type int
typedef int E;


//Déclaration du type pile qui a
typedef struct cell_st{
	E val;
	cell * suivant;
}cell;

//Déclaration des Fonctions


//____________________________________________________

Pile * creer_Pile();

//____________________________________________________

void free (Pile ** P);

//____________________________________________________

void push(Pile ** P, E);

//____________________________________________________

E pop(Pile * P);

//____________________________________________________

int est_Vide(Pile * P);

//____________________________________________________

void afficher_Pile(Pile * P);

//____________________________________________________

void exo1(Pile * P1, Pile * P2, Pile * P3);




#endif
