#include<unistd.h>
#include<stdlib.h>
#include<stdio.h>
#include<wait.h>


char n;

int main(void)
{
  pid_t pid;
  int status;
  if((pid=fork()) == -1) { exit(EXIT_FAILURE);}
  if(pid != 0)
  {
    n++;
    wait(&status);
    if(WIFEXITED(status)) n+=WEXITSTATUS(status);
  }
  else
    n--;
  printf("[%d] : valeur de n est %d\n", getpid(), -n);
  exit(n);
}
