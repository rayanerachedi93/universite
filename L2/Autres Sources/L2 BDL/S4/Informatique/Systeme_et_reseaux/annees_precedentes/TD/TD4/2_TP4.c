#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<fcntl.h>
#include<signal.h>
#include<sys/wait.h>

int CPT_USR1 = 0;
int VAL;

void handlerP(int signal);
void handlerF(int signal);

int main(int argc, const char* argv[] )
{
  pid_t pere, fils;
  if(argc != 2)
  {
    printf("Pas le bon nombre d'arguments, donnez moi 1 entier\n");
    exit(1);
  }
  VAL = atoi(argv[1]);
  if((pere = fork()) == -1) {perror("fork1") ; exit(1) ;}

  if(pere)
  {
    if((fils = fork()) == -1) {perror("fork2") ; exit(1) ;}
    if(fils != 0)
    {
      //Père
      signal(SIGUSR1, handlerP);
      while(CPT_USR1 < VAL)
      {
        if(CPT_USR1 >= VAL)
          kill(fils, SIGUSR2);
      }
    }
  }
  else
  {
    //Fils
    signal(SIGUSR2, handlerF);
    while(1)
    {
      sleep(1);
      kill(getppid(), SIGUSR1);
    }

  }
  return EXIT_SUCCESS;
}

void handlerP(int signal)
{
  if(signal == SIGUSR1)
    printf("SIGUSR1 : %d\n", ++CPT_USR1);
}

void handlerF(int signal)
{
  if(signal == SIGUSR2)
    exit(0);
}
