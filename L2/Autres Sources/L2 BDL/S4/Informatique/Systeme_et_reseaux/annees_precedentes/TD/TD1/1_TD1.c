#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

#define P_MAX 3

int main()
{
  int i, pid;
  for(i = 0; i < P_MAX ; i++)
  {
    printf("PID :  %d; PPID : %d\n", getpid(), getppid());
    pid = fork();
    if(pid != 0)
    {
      pid = fork();
      if(pid != 0)
        exit (EXIT_SUCCESS);
    }
  }
  printf("%d\n", getpid());
  exit (EXIT_SUCCESS);
}
