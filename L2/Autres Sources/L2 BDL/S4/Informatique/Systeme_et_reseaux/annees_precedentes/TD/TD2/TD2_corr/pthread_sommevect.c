// gcc -o pthread_sommevect pthread_sommevect.c -lpthread

#include<pthread.h>
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/syscall.h>
#include<sys/types.h>
#include<linux/unistd.h>
#include<time.h> 

#define N 2

pthread_t pthread_id[N][N];

int matriceA[N][N];
int matriceB[N][N];
int matriceC[N][N];

void imprime(int matrice[N][N])
{
	int i,j;

	for (i=0;i<N;i++)
	{
		for (j=0;j<N;j++)
			printf("%d ",matrice[i][j]);
		printf("\n");
	}
}

void generation_aleatoire(int matrice[N][N])
{
	int i,j;

	for (i=0;i<N;i++)
		for (j=0;j<N;j++)
			matrice[i][j] = rand()%100 ;	
}

void* somme(void* arg)
{
	*((int **)arg)[0] =  *((int **)arg)[1] + *((int **)arg)[2];
	return arg;
}

int main() // C = A + B
{

	void ** thread_return;

	srand(time(NULL)); 
	generation_aleatoire(matriceA);
	generation_aleatoire(matriceB);

	printf("\n matrice A:\n");
	imprime(matriceA);
	printf("\n matrice B:\n");
	imprime(matriceB);

	int i,j;
	for(i = 0; i < N; i++)
		for(j = 0; j < N; j++)
		{
			int **arg;
			arg = (int **) malloc (3 * sizeof(int *));
			arg[1] = &(matriceA[i][j]);
			arg[2] = &(matriceB[i][j]);
			arg[0] = &(matriceC[i][j]);
			if( pthread_create( &pthread_id[i][j], NULL, somme, (void *)arg) == -1)
			{
				fprintf(stderr, " Erreur de creation du pthread numero (%d %d)", i,j);
			}
	}

	sleep(1);	
	printf("\n matrice C:\n");
	imprime(matriceC);

	return EXIT_SUCCESS;
}
