#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#define N 10

pthread_t pthread_id[N][N];


void generation_aleatoire(int matrice[N][N]);
void imprime(int matrice[N][N]);
void* somme(void* arg);

int main()
{
  int matriceA[N][N], matriceB[N][N], matriceC[N][N], i, j;
  srand(time(NULL));
  generation_aleatoire(matriceA);
  printf("Matrice A :\n");
  imprime(matriceA);
  printf("\n\n\n");
  generation_aleatoire(matriceB);
  printf("Matrice B :\n");
  imprime(matriceB);
  printf("\n\n\n");

  for(i = 0; i < N; i++)
  {
    for(j = 0; j < N; j++)
    {
      int **arg;
      arg = (int **) malloc (3 * sizeof(int *));
      arg[1] = &(matriceA[i][j]);
      arg[2] = &(matriceB[i][j]);
      arg[0] = &(matriceC[i][j]);
      if(pthread_create( &pthread_id[i][j], NULL, somme, (void *)arg) == -1)
      {
        fprintf(stderr, " Erreur de creation du pthread numero (%d %d)", i,j);
      }
    }
  }

  printf("Matrice C :\n");
  imprime(matriceC);
  return EXIT_SUCCESS;
}

void generation_aleatoire(int matrice[N][N])
{
  int i, j;
  for(i = 0; i < N; i++)
  {
    for(j = 0; j < N; j++)
      matrice[i][j] = rand() % 100;
  }
}

void imprime(int matrice[N][N])
{
  int i, j;
  for(i = 0; i < N; i++)
  {
    printf("[ %3d  |", matrice[i][0]);
    for(j = 1; j < N-1; j++)
      printf(" %3d |", matrice[i][j]);
    printf(" %3d ]\n", matrice[i][N-1]);
  }
}

void* somme(void* arg)
{
  *((int **)arg)[0] = *((int **)arg)[1] + *((int **)arg)[2];
  return arg;
}
