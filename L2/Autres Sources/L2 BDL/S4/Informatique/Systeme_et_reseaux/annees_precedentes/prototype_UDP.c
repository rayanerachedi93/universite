#include <arpa/inet.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

#define MAXMSGSIZE 1024

void erreur(char *message_erreur) 
{
	perror(message_erreur) ;
	exit(1) ;
}

// Serveur

int main(int argc, char **argv)
{
	int sock ;
	struct sockaddr_in serveur_adr ;
	struct sockaddr_in client_adr ;
	int len ;
	int nb_octets ;
	char message[MAXMSGSIZE] ;
	
	sock = socket(AF_INET, SOCK_DGRAM, 0) ;
	
	if (sock < 0) erreur("Erreur de creation de la socket") ;
	
	serveur_adr.sin_family = AF_INET ;
	serveur_adr.sin_port = htons(5000) ;
	serveur_adr.sin_addr.s_addr = htonl(INADDR_ANY) ;
	bzero(&(serveur_adr.sin_zero),8) ;
	len = sizeof(struct sockaddr_in) ;
	
	if (bind(sock,(struct sockaddr *) &serveur_adr, len) == -1) erreur("Erreur attachement socket") ;
	
	printf("\n Attente client sur port 5000") ;
	fflush(stdout) ;

	while (1)
	{
		nb_octets = recvfrom(sock, message, MAXMSGSIZE, 0, (struct sockaddr *) &client_adr,(socklen_t *) &len) ;
		message[nb_octets] = ’\0’ ;
		
		printf("\n(%s",inet_ntoa(client_adr.sin_addr)) ;
		printf(",%d)",ntohs(client_adr.sin_port)) ;
		printf(" a dit : %s", message) ;
		
		fflush(stdout) ;
	}
	exit(0);
}











