#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
 
#define MAXMSGSIZE 1024

void erreur(char *message_erreur)
{
	perror(message_erreur) ;
	exit(1) ;
}

// serveur

int main(int argc, char **argv)
{
	int sock, newsock ;
	unsigned short port ;
	struct sockaddr_in serveuradr ;
	struct sockaddr_in clientadr ;
	char *client_addr ;
	char message[MAXMSGSIZE] ;
	int nb_octets ;
	int len ;
	if (argc  != 2)
	{
		fprintf(stderr, "usage : %s <port>\n", argv[0]) ;
		exit(1) ;
	}
	port = (unsigned short) atoi(argv[1]) ;
	sock = socket(AF_INET, SOCK_STREAM, 0) ;

	if (sock < 0)
		erreur("Erreur de creation de la socket");

	len = sizeof(struct sockaddr_in) ;
	bzero((char *) &serveuradr, len) ;
	serveuradr.sin_family = AF_INET ;
	serveuradr.sin_addr.s_addr = htonl(INADDR_ANY) ;
	serveuradr.sin_port = htons(port) ;

	if (bind(sock, (struct sockaddr *) &serveuradr, len) < 0)
	erreur("Erreur attachement socket") ;

	if (listen(sock, 5) < 0)
		erreur("Erreur d’ecoute sur la socket") ;

	while (1) 
	{
		newsock = accept(sock, (struct sockaddr *) &clientadr, (socklen_t *)&len);
		
		if (newsock < 0) erreur("Erreur accept");
		
		printf("connexion etablie avec %s\n", inet_ntoa(clientadr.sin_addr)) ;
		bzero(message, MAXMSGSIZE) ;
		nb_octets = read(newsock, message, MAXMSGSIZE) ;
		
		if (nb_octets < 0) erreur("Erreur de lecture sur la socket") ;
		
		printf("le serveur a recu %d octets : %s", nb_octets, message) ;
		nb_octets = write(newsock, message, strlen(message)) ;

		if (nb_octets < 0) erreur("Erreur d’ecriture sur la socket") ;
		close(newsock) ;
	}
}








