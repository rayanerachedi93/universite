// gcc -o signal_sigusr signal_sigusr.c

#include<stdio.h>
#include<stdlib.h>
#include<signal.h>

#define NMAX 3

int nb_sigusr1;
int pid_fils;

void fonction_signalpere(int sig) {
	if (sig==SIGUSR1) {
		fprintf(stderr,"Reception du signal SIGUSR1\n");
		fprintf(stderr,"nb SIGUSR1 = %d\n",++nb_sigusr1);
		fflush(stderr);
		if (nb_sigusr1==NMAX) {
			fprintf(stderr,"Envoi du signal SIGUSR2\n");
			kill(pid_fils,SIGUSR2);
			exit(0);
		}
	}
}

void fonction_signalfils(int sig) {
	if (sig==SIGUSR2) {
		fprintf(stderr,"Reception du signal SIGUSR2\n");
		fflush(stderr);
		exit(0);
	}
}

int main(int argc,char **argv) {
	switch(pid_fils=fork()) {
		case (pid_t)-1:
			perror("creation de processus");
			exit(2);
		case (pid_t)0:
			fprintf(stderr,"lancement du fils\n");
			fflush(stderr);
			if (signal(SIGUSR2,fonction_signalfils)==SIG_ERR) {
				fprintf(stderr,"Erreur lors de l'association du signal SIGUSR2\n");
				exit(-1);
			}
			while(1) {
				sleep(5);
				fprintf(stderr,"Envoi du signal SIGUSR1\n");
				fflush(stderr);
				kill(getppid(),SIGUSR1);
			}
			break;
		default:
			fprintf(stderr,"lancement du pere\n");
			fflush(stderr);
			nb_sigusr1=0;

			if (signal(SIGUSR1,fonction_signalpere)==SIG_ERR) {
				fprintf(stderr,"Erreur lors de l'association du signal SIGUSR1\n");
				exit(-1);
			}
			while(1);
	}
	return(0);
}
