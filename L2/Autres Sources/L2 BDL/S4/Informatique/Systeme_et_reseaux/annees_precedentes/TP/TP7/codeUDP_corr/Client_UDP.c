/* client_UDP.c (client UDP) */
 
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <sys/socket.h>
 
char* id = 0;
short sport = 0;
int sock = 0; /* socket de communication */

int main(int argc, char** argv) {
   struct sockaddr_in moi; /* SAP du client */
   struct sockaddr_in serveur; /* SAP du serveur */
   int nb_question = 0;
   int ret,len;
   int serveur_len = sizeof(serveur);

   if (argc != 4) {
       fprintf(stderr,"usage: %s id host sport\n", argv[0]);
       exit(1);
       }
   id = argv[1];
   sport = atoi(argv[3]);
   if ((sock = socket(AF_INET,SOCK_DGRAM,0)) == -1) {
       fprintf(stderr,"%s: socket %s\n",argv[0],strerror(errno));
       exit(1);
       }
   len = sizeof(moi);
   ret = getsockname(sock, (struct sockaddr *)&moi, (socklen_t *) &len); // retourne l'IP 0.0.0.0 dans la plupart des systèmes, car protocole UDP
   serveur.sin_family = AF_INET;
   serveur.sin_port = htons(sport);
   inet_aton(argv[2], (struct in_addr *)&serveur.sin_addr);
   while (nb_question < 3) {
          char buf_read[1<<8], buf_write[1<<8];

          sprintf(buf_write, "#%2s=%03d", id, nb_question++);
          printf("client %2s: (%s,%4d) envoie a ", id, inet_ntoa((struct in_addr)moi.sin_addr),ntohs(moi.sin_port));
          printf("(%s,%4d): %s\n", inet_ntoa(serveur.sin_addr), ntohs(serveur.sin_port), buf_write);
          ret = sendto(sock, buf_write, strlen(buf_write)+1,0, (struct sockaddr *)&serveur,sizeof(serveur));
          if (ret <= 0) {
              printf("%s: erreur dans sendto (num=%d, mess=%s)\n", argv[0],ret,strerror(errno));
              continue;
              }
          len = sizeof(moi);
          getsockname(sock,  (struct sockaddr *)&moi, (socklen_t *) &len);
          printf("client %2s: (%s,%4d) recoit de ", id, inet_ntoa(moi.sin_addr),ntohs(moi.sin_port));
          ret = recvfrom(sock,buf_read,256,0, (struct sockaddr *)&serveur, (socklen_t *) &serveur_len);
          if (ret <= 0) {
              printf("%s: erreur dans recvfrom (num=%d, mess=%s)\n", argv[0],ret,strerror(errno));
              continue;
              }
          printf("(%s,%4d) : %s\n", inet_ntoa(serveur.sin_addr), ntohs(serveur.sin_port), buf_read);
          }
     return 0;
}
