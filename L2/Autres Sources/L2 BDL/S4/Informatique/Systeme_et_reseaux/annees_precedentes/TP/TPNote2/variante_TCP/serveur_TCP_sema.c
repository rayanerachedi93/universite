#include <arpa/inet.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include<pthread.h>
#include<semaphore.h>
#include "message.h"


// Serveur

#define NB_CLIENTS_MAX 8

#define BUF_SIZE 256
#define MSG_SIZE 16

pthread_t pthread_Requetes[NB_CLIENTS_MAX];

typedef struct
{
	sem_t attenteConnexion;
	sem_t libre;
	int num_libre;
	int num_Attente_Connexion;
} ControleAccesClient;

ControleAccesClient controle;

void initialisecontrole (ControleAccesClient* controle)
{
	sem_init (&controle->libre, 0, 2);
	controle->num_libre = 1;
	sem_init (&controle->attenteConnexion, 0, 2);
	controle->num_Attente_Connexion = 1;
}

void erreur(char *message_erreur);
void* connecterClient(void* arg);
void* gererInput(void* arg);
void* ajout (void* arg);
void* lister (void* arg);
void* supprimer (void* arg);

pthread_t pthread_id[NB_CLIENTS_MAX];

int dernierID = 0;
int num_Users = 0;
int i_eme_client = 0;

char* msg = "\nVeuillez entrer :\t\n\n'AJOUT' pour ajouter un nouveau message\t\n\n'LISTER' pour envoyer la liste des messages\t\n\n'SUPPRIMER_x' pour supprimer un identifiant x de la liste des messages (q ou Q pour quitter)\n\n";
char* msg2 = "\nEntrez le message que vous voulez ajouter (256 caractères max)\n> ";

char input [MSG_SIZE + 1], buffer[BUF_SIZE + 1];

message * listeMessage;
int sock, i, len, nb_octets, identifiant, ret;
int tailleCurListe = 0;
int tailleMaxListe = MSG_SIZE;

struct sockaddr_in serveur_adr;
int client_len = sizeof(struct sockaddr_in);

int main(int argc, char **argv)
{
	listeMessage = (message *) malloc(sizeof(message) * 10);
	initialisecontrole(&controle);
	sock = socket(AF_INET, SOCK_STREAM, 0) ;
	if (sock < 0) erreur("Erreur de creation de la socket") ;

	serveur_adr.sin_family = AF_INET ;
	serveur_adr.sin_port = htons(5000) ;
	serveur_adr.sin_addr.s_addr = htonl(INADDR_ANY) ;
	bzero(&(serveur_adr.sin_zero),8) ;
	len = sizeof(struct sockaddr_in) ;

	if (bind(sock,(struct sockaddr *) &serveur_adr, len) == -1) erreur("Erreur attachement socket") ;

	printf("\nAttente client sur port 5000\n") ;
	printf("IP serveur = %s\n", inet_ntoa(serveur_adr.sin_addr));
	fflush(stdout) ;
  if (listen(sock, 5) != 0){
    fprintf(stderr,"%s: listen %s\n", argv[0],strerror(errno));
    exit(1);
  }
	while (1)
	{
		if(pthread_create (&pthread_id[i_eme_client], NULL, connecterClient, NULL))
		  erreur("pthread_create connecterClient");

	}
	exit(0);
}

void* connecterClient(void* arg)
{
	sem_wait(&controle.attenteConnexion);
	controle.num_Attente_Connexion --;
	i_eme_client ++;
	struct sockaddr_in client_adr;
	int sock_pipe; /* socket de dialogue */
	sock_pipe = accept(sock, (struct sockaddr *) &client, &len);

	if((write(sock, msg, BUF_SIZE)) == -1)
		erreur("Erreur write 1");
	nb_octets = read(sock, input, MSG_SIZE) ;
	input[nb_octets] = '\0';

	if(pthread_create (&pthread_Requetes[i_eme_client - 1], NULL, gererInput, (void*) sock_pipe))
		erreur("pthread_create gereInput");
	pthread_join(pthread_Requetes[i_eme_client - 1, NULL);

	sem_post(&controle.attenteConnexion);
	controle.num_Attente_Connexion ++;
	pthread_exit(NULL);
}

void* gererInput (void* arg)
{
	sem_wait(&controle.libre);
	controle.num_libre --;
	int sock_pipe = (int) arg;
	int estSupprimerValide = 1;
	int estNombreValide = 0;
	int nombreEstFini = 0;

	if(strlen("SUPPRIMER_") < strlen(input))
	{
		for(i = 0; i < strlen("SUPPRIMER_") && estSupprimerValide; i++)
		{
			if(input[i] != "SUPPRIMER_"[i])
				estSupprimerValide = 0;
		}
		const int tailleNum = strlen(input) - strlen ("SUPPRIMER_");
		char numASupprimer[tailleNum];

		if((input[strlen("SUPPRIMER_")] >= 48) && (input[strlen("SUPPRIMER_")] <= 57))
		{
			estNombreValide = 1;
		}
		for(i = strlen("SUPPRIMER_"); i < strlen(input) && (!nombreEstFini) ; i++)
		{
			if((input[i] < 48) ||(input[i] > 57))
			{
				nombreEstFini = 1;
			}
			else
			{
				numASupprimer[i-strlen("SUPPRIMER_")] = input[i];
			}
		}
		if(estSupprimerValide && estNombreValide)
		{
			identifiant = atoi(numASupprimer);
			strcpy(input, "SUPPRIMER");
		}
	}
	
	if((strcmp(input, "Q") != 0) && (strcmp(input, "q") != 0))
	{
		close(sock_pipe);
		pthread_exit(NULL);
	}
	if(strcmp(input, "AJOUT") == 0)
	{
		if(pthread_create (&pthread_Requetes[i_eme_client - 1], NULL, ajout, NULL))
			erreur("pthread_create ajout");
		pthread_join(pthread_id[i_eme_client - 1], NULL);
	}

	if(strcmp(input, "LISTER") == 0)
	{
		if(pthread_create (&pthread_Requetes[i_eme_client - 1], NULL, lister, NULL))
			erreur("pthread_create ajout");
		pthread_join(pthread_id[i_eme_client - 1], NULL);
	}

	if(strcmp(input, "SUPPRIMER") == 0)
	{
		if(pthread_create (&pthread_Requetes[i_eme_client - 1], NULL, supprimer, NULL))
			erreur("pthread_create ajout");
		pthread_join(pthread_id[i_eme_client - 1], NULL);
	}
	else
	{
		erreur("mauvais input");
		pthread_exit(NULL);
	}
	sem_post(&controle.libre);
	controle.num_libre ++;

}

void* ajout (void* arg)
{
	if((sendto(sock, msg2, BUF_SIZE, 0, (struct sockaddr *)&client_adr,sizeof(client_adr))) == -1)
		erreur("Erreur sendto 3");
	nb_octets = recvfrom(sock, buffer, BUF_SIZE, 0, (struct sockaddr *) &client_adr,(socklen_t *) &len) ;
	buffer[nb_octets] = '\0';
	message nouveauMessage;
	strcpy(nouveauMessage.texte, buffer);
	nouveauMessage.id = ++dernierID;

	if(tailleCurListe == tailleMaxListe)
	{
		tailleMaxListe*=2;
		listeMessage = (message*) realloc(listeMessage, sizeof(tailleMaxListe));
	}
	listeMessage[tailleCurListe] = nouveauMessage;
	tailleCurListe ++;
	pthread_exit(NULL);
}

void* lister (void* arg)
{
	if((sendto(sock, &tailleCurListe, sizeof(int), 0, (struct sockaddr *)&client_adr, client_len)) == -1)
		erreur("Erreur sendto 4");
	for(i = 0; i < tailleCurListe; i++)
	{
		if((sendto(sock, &listeMessage[i], sizeof(message), 0, (struct sockaddr *)&client_adr, client_len) == -1))
			erreur("Erreur sendto 5");
	}
	pthread_exit(NULL);
}

void* supprimer (void* arg)
{
	for(i = 0; i < tailleCurListe && listeMessage[i].id != identifiant; i++);
	if(i < tailleCurListe)
	{
		while(i < tailleCurListe)
		{
			listeMessage[i] = listeMessage[i+1];
			i++;
		}
		tailleCurListe--;
	}
	pthread_exit(NULL);
}

void erreur(char *message_erreur)
{
	perror(message_erreur) ;
	exit(1) ;
}
