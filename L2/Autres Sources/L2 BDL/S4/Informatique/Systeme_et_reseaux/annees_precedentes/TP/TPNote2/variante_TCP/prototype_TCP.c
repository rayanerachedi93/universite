#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "message.h"
 
#define MAXMSGSIZE 201

void erreur(char *message_erreur)
{
	perror(message_erreur) ;
	exit(1) ;
}

int dernierID = 0;

int num_Users = 0;

// serveur

int main(int argc, char **argv)
{
	char * msg = "\nVeuillez entrer :\t\n\n'AJOUT' pour ajouter un nouveau message\t\n\n'LISTER' pour envoyer la liste des messages\t\n\n'SUPPRIMER' pour supprimer un identifiant de la liste des messages (q ou Q pour quitter)\n\n";
	char * msg2 = "\nVeuillez entrer l'identifiant à supprimer\n";
	char * msg3 = "\nEntrez le message que vous voulez ajouter (200 caractères max)\n> ";
	
	int sock, newsock, nb_octets, identifiant, i;
	unsigned short port ;
	char input [10], buffer[201];
	struct sockaddr_in serveuradr ;
	struct sockaddr_in clientadr ;
	//char *client_addr ;
	//char messages[MAXMSGSIZE] ;
	int len = sizeof(struct sockaddr_in) ;
	
	if (argc  != 2)
	{
		fprintf(stderr, "usage : %s <port>\n", argv[0]) ;
		exit(1) ;
	}
	
	message * listeMessage = (message*) malloc(sizeof(message) * 10);
	int tailleCurListe = 0;
	int tailleMaxListe = 10;
	port = (unsigned short) atoi(argv[1]) ;
	sock = socket(AF_INET, SOCK_STREAM, 0) ;

	if (sock < 0)
		erreur("Erreur de creation de la socket");

	
	bzero((char *) &serveuradr, len) ;
	serveuradr.sin_family = AF_INET ;
	serveuradr.sin_addr.s_addr = htonl(INADDR_ANY) ;
	serveuradr.sin_port = htons(port) ;

	if (bind(sock, (struct sockaddr *) &serveuradr, len) < 0)
	erreur("Erreur attachement socket") ;
	printf("IP serveur = %d\n", serveuradr.sin_addr.s_addr); // Affiche l'ip du serveur
	
	if (listen(sock, 5) < 0)
		erreur("Erreur d’ecoute sur la socket") ;

	while (1) 
	{
		newsock = accept(sock, (struct sockaddr *) &clientadr, (socklen_t *)&len);
		if (newsock < 0) erreur("Erreur accept");
		printf("connexion etablie avec %s\n", inet_ntoa(clientadr.sin_addr)) ;
		
		char buf_read[201], buf_write[201];
		
		do{			
				nb_octets = write(newsock, msg, strlen(msg)) ;
				if (nb_octets < 0) erreur("Erreur d’ecriture sur la socket1 (msg)") ;
				
				nb_octets = read(newsock, input, 10) ;
				if (nb_octets < 0) erreur("Erreur de lecture sur la socket1") ;
				
				input[nb_octets] = '\0';
				
				if(strcmp(input, "SUPPRIMER") == 0)
				{
					nb_octets = write(newsock, msg2, strlen(msg2)) ;
					if (nb_octets < 0) erreur("Erreur d’ecriture sur la socket2 (msg1)") ;
					
					
					nb_octets = read(newsock, &identifiant, sizeof(int)) ;
					if (nb_octets < 0) erreur("Erreur de lecture sur la socket2") ;
				}
				
		} while((strcmp(input, "AJOUT") != 0) && (strcmp(input, "LISTER") != 0) && (strcmp(input, "SUPPRIMER") != 0)) ;
		
		
		
		if(strcmp(input, "AJOUT") == 0)
		{
			nb_octets = write(newsock, msg3, strlen(msg3)) ;
				if (nb_octets < 0) erreur("Erreur d’ecriture sur la socket (msg3)") ;
			bzero(buf_read, strlen(buf_read));
			nb_octets = read(newsock, buf_read, 200) ;
					if (nb_octets < 0) erreur("Erreur de lecture sur la socket2") ;
			
						
			buf_read[nb_octets] = '\0';
			message nouveauMessage;
			strcpy(nouveauMessage.texte, buf_read);
			nouveauMessage.id = ++dernierID;
		
			if(tailleCurListe == tailleMaxListe)
			{
				tailleMaxListe*=2;
				listeMessage = (message*) realloc(listeMessage, sizeof(tailleMaxListe));
			}
			listeMessage[tailleCurListe] = nouveauMessage;
			tailleCurListe ++;	
		}
		
		
		if(strcmp(input, "LISTER") == 0)
		{
			
			nb_octets = write(newsock, &tailleCurListe, sizeof(int)) ;
				if (nb_octets < 0) erreur("Erreur d’ecriture sur la socket tailleCurListe") ;
				
			for(i = 0; i < tailleCurListe; i++)
			{
				nb_octets = write(newsock, &listeMessage[i], sizeof(message)) ;
				if (nb_octets < 0) erreur("Erreur d’ecriture sur la socket tailleCurListe") ;
			}
		}
		
		
		
		if(strcmp(input, "SUPPRIMER") == 0)
		{
			for(i = 0; i < tailleCurListe && listeMessage[i].id != identifiant; i++);
			
			if(i < tailleCurListe)
			{
				while(i < tailleCurListe)
				{
					listeMessage[i] = listeMessage[i+1];
					i++;
				}
				tailleCurListe--;
			}
		}
		
	
		
		/*bzero(message, MAXMSGSIZE) ;
		nb_octets = read(newsock, message, MAXMSGSIZE) ;
		
		if (nb_octets < 0) erreur("Erreur de lecture sur la socket") ;
		
		printf("le serveur a recu %d octets : %s", nb_octets, message) ;
		nb_octets = write(newsock, message, strlen(message)) ;

		if (nb_octets < 0) erreur("Erreur d’ecriture sur la socket") ;*/
		//close(newsock) ;
	}
	exit(0);
}








