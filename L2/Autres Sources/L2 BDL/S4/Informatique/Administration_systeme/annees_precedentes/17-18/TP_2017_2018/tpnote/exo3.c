/*HAMES OUEZNA 11610524*/
/*HENOUNE ASMA  11606588*/
/*TP 10*/
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>


int main(){
	pid_t f1,f2;
	int fdp[2];
	int P = pipe(fdp);
	if (P == -1){
		perror("creation de pipe echoué:");
		exit(-1);
	}
	f1 = fork();
	if(f1 == -1){
		perror("erreur fork...!");
		exit(-1);
	} else if (f1 == 0) {
    		close(fdp[1]);
    		if (read(fdp[0],&f2,sizeof(pid_t))==-1){
    			perror("erreur read\n");
    			exit(-1);
    		}
		printf("Je suis le  f1 %d, mon pere est %d et mon frere est  %d !\n", getpid(), getppid(),f2);
	} else {
		f2=fork();
		if (f2 == -1){
			perror("erreur creation fils 2");
			exit(1);
		}
		if(f2 == 0){
			pid_t tmp = getpid();
			close(fdp[0]);
			if(write(fdp[1],&tmp, sizeof(pid_t)) == -1){
			    	perror("erreur ecriture");
	    			exit(-1);
	    		}		
		
			printf("Je suis le f2 %d, mon pere est  %d  et mon frere est le %d !\n", getpid(), getppid(), f1);
		} else {
			wait(NULL);
			wait(NULL);
			close(fdp[1]);
			close(fdp[0]);
			printf("Je suis le père %d, fils: %d et %d.\n",getpid(), f1, f2);
		}
	}
	return 1;
}
