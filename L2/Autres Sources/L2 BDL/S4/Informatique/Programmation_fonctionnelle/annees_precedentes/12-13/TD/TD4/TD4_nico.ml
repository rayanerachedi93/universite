(*
#use "TD4_nico.ml";;
*)

(* Exo1 *)
(*1*)


let liste = [9;8;7;6;5;4;3;2;1;99;77;55];;
let liste2 = [6;5;4;3;2;-99;77;55];;
let listeBool = [true; false; true; false; false];;
let listeBool2 = [false; false; true];;


let min a b = if(a < b) then a else b;;
let max a b = if(b < a) then a else b;;

let rec minList l =
  match l with
  | [] -> 0
  | h1 :: t1 -> 
		if(t1 = [])
		then
			h1
		else
			min h1 (minList t1);;

minList liste;;
minList liste2;;

let rec maxList l =
  match l with
  | [] -> 0
  | h1 :: t1 -> 
		if(t1 = [])
		then
			h1
		else
			max h1 (maxList t1);;

(* 2 *)

let rec minMaxList l =
  match l with
  | [] -> (0, 0)
  | h1 :: t1 -> 
		if(t1 = [])
		then
			(h1, h1)
		else
			((min h1 (fst(minMaxList t1))), (max h1 (snd(minMaxList t1)))) ;;

minMaxList liste;;
minMaxList liste2;;

(* Exo2 *)

let rec rmPair l = 
	match l with
	| [] -> []
	| h1 :: t1 ->
	if((h1 mod 2) = 0)
	then
		rmPair t1
	else
		h1 :: rmPair t1;;

liste;;
rmPair liste;;
liste2;;
rmPair liste2;;

(* Exo3 *)

let rec disjonction l b = 
	match l with 
	| [] -> []
	|	h1 :: t1 -> (h1 || b) :: (disjonction t1 b);;


listeBool;;
disjonction listeBool true;;
disjonction listeBool false;;
listeBool2;;
disjonction listeBool2 true;;
disjonction listeBool2 false;;

(* Exo4 *)

let disjonctionMap x = List.map (fun h -> x || h);;


listeBool;;
disjonctionMap true listeBool;;
disjonctionMap false listeBool;;
listeBool2;;
disjonctionMap true listeBool2;;
disjonctionMap false listeBool2;;

(* Exo5 *)

let minListFold l =
	match l with
	| [] -> 0
	| h1 :: t1 -> List.fold_right min (List.tl l) (List.hd l);;

liste;;
minListFold liste;;
liste2;;
minListFold liste2;;

let minMaxListFold l =
	match l with
	| [] -> (0, 0)
	| h1 :: t1 -> ((List.fold_right min (List.tl l) (List.hd l)), (List.fold_right max (List.tl l) (List.hd l)));;

liste;;
minMaxListFold liste;;
liste2;;
minMaxListFold liste2;;

(* Exo6 *)

let rmPairFold l =
  let f h l2 = if ((h mod 2) = 0) then l2 else h :: l2
  in List.fold_right f l [];;
liste;;
rmPairFold liste;;
liste2;;
rmPairFold liste2;;
  
(* Exo7 *)

let rmPairFoldLeft l =
  let f l2 h = if ((h mod 2) = 0) then l2 else l2 @ (h :: [])
  in List.fold_left f [] l;;

liste;;
rmPairFoldLeft liste;;
liste2;;
rmPairFoldLeft liste2;;


(*
#use "TD4_nico.ml";;
*)
