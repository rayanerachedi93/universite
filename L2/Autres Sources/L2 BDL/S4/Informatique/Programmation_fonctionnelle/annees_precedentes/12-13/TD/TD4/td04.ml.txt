(*********  ex. 1  *********)

(***  1.1  ***)

let rec minlist = function
  | [] -> 0
  | x :: ls -> if (ls = []) then x else min x (minlist ls);;
  
let rec minlistbis = function
  | [] -> 0
  | [x] -> x
  | h :: t -> min h (minlist t);;

(* exemples *)

let ls = [3; 4; 6; -2; 10; -1; 8];;
minlist ls;;
minlistbis ls;;
minlist [];;
minlistbis [];;

(***  1.2  ***)

let rec minmaxlist = function
  | [] -> (0, 0)
  | [x] -> (x, x)
  | h :: t -> let (x, y) = minmaxlist t
	      in (min h x, max h y);;
	     
(* exemples *)

minmaxlist ls;;
minmaxlist [];;


(*********  ex. 2  *********)

let rec elimeven = function
  | [] -> []
  | h :: t -> if (h mod 2 = 0) then (elimeven t) else h :: (elimeven t);;

(* exemples *)

elimeven [2; 3; 4; 6; 1; 13; 6];;


(*********  ex. 3  *********)

let rec mapou x = function
  | [] -> []
  | h :: t -> (h or x) :: mapou x t;;

(* exemples *)

mapou false [false; true; true; true; false];;
mapou true [false; true; true; true; false];;


(*********  ex. 4  *********)

let mapou x = List.map (fun h -> x or h);;

(* exemples *)

mapou false [false; true; true; true; false];;
mapou true [false; true; true; true; false];;


(*********  ex. 5  *********)

let minlist ls =
  if (ls = []) then 0
  else List.fold_right min (List.tl ls) (List.hd ls);; 

let minmaxlist ls = 
  if (ls = []) then (0, 0)
  else let f h (x, y) = (min h x, max h y)
       in List.fold_right f (List.tl ls) (List.hd ls, List.hd ls);; 

(* exemples *)

let ls = [3; 4; 6; -2; 10; -1; 8];;
minlist ls;;
minmaxlist ls;;


(*********  ex. 6  *********)

let elimeven ls =
  let f h l = if (h mod 2 = 0) then l else h :: l
  in List.fold_right f ls [];;

(* exemples *)

elimeven [2; 3; 4; 6; 1; 13; 6];;


(*********  ex. 7  *********)

let elimeven ls =
  let f l h = if (h mod 2 = 0) then l else l @ [h]
  in List.fold_left f [] ls;;

(* exemples *)

elimeven [2; 3; 4; 6; 1; 13; 6];;
