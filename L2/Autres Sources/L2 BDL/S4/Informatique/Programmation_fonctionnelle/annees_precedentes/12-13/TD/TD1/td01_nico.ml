(*
#use "td01_nico.ml" ;;
*)

(* Exo1 : *)

let xor x y = if((x = true && y = true) || (x = false && y = false)) then false else true ;;
let ssi x y = if((x = true && y = true) || (x = false && y = false)) then true else false ;;

xor true true ;;
xor false true ;;
xor true false ;;
xor false false ;;

ssi true true ;;
ssi false true ;;
ssi true false ;;
ssi false false ;;

(* Exo2 : *)
let p_1_3 (x,y,z) = x ;;
let p_2_3 (x,y,z) = y ;;
let p_3_3 (x,y,z) = z ;;

let triplet1 = (1,2,3);;
let triplet2 = (14,98,7);;
let addTrip a b = (p_1_3 a + p_1_3 b, p_2_3 a + p_2_3 b, p_3_3 a + p_3_3 b);;

addTrip triplet1 triplet2 ;;

p_1_3 triplet1 ;;
p_2_3 triplet1 ;;
p_3_3 triplet1 ;;

let maxTriplet a = if(p_1_3 a < p_2_3 a || p_1_3 a < p_3_3 a) then if(p_2_3 a < p_3_3 a) then p_3_3 a else p_2_3 a else p_1_3 a ;;
let minTriplet a = if(p_1_3 a > p_2_3 a || p_1_3 a > p_3_3 a) then if(p_2_3 a > p_3_3 a) then p_3_3 a else p_2_3 a else p_1_3 a ;;
let ordre a = if(p_1_3 a < p_2_3 a) then if (p_2_3 a < p_3_3 a) then 1 else 0 else if (p_1_3 a > p_2_3 a) then if (p_2_3 a > p_3_3 a) then -1 else 0 else 0 ;;

maxTriplet triplet1 ;;
minTriplet triplet1 ;;
ordre triplet1 ;;
ordre triplet2 ;;

(* Exo3 : *)

let f x = x + 0 ;;
let g x = f x ;;
f 5 ;;
g 3 ;;

(* Exo4 :*)

let f x = x * 2 ;;
f 5;;

let exo4 (f, (a,b)) = f (a+b) ;;
exo4 (f, (10, 30));;

(* Exo5 :*)

let x = 3 ;;
let f y = y + x ;;
f 2 ;;
let x = 0 ;;
f 2 ;;

(*
Le x dans la première définition de la fonction f n'est pas une référence à une variable x,
mais à la valeur x à l'instant.
Ainsi, si la valeur de x change, celà  n'affecte pas celle qui a servi pour la définition de f.
 *)

(* Exo6 *)

let x = 2 ;;
let y = x + 1
  in let x = 3
    in x + y ;;
x;;
let x = 3
  in let y = x + 1
    in x + y ;;
x;;
let x = 1 + 2 in ((function y -> y + x) (x+1)) ;;
let x = 1 + 2 in ((function x -> x + x) (x+1)) ;;


(*
#use "td01_nico.ml" ;;
*)
