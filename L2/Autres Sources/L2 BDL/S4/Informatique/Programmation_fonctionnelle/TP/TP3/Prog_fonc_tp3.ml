(* Ceci est un éditeur pour OCaml
   Entrez votre programme ici, et envoyez-le au toplevel en utilisant le
   bouton "Évaluer le code" ci-dessous. *)

 (*Définir une fonction inserer qui insère à la bonne place un élément donné dans
      une liste croissante donnée*)
let rec inserer elem liste = match liste with 
  |[] -> [elem]               
  |tete::queue -> if elem <= tete
      then elem::liste 
      else
        tete ::(inserer elem queue);;

(*En déduire une fonction tri_insertion qui utilise 
la fonction précédente et qui
trie selon l’ordre croissant une liste donnée 
    en utilisant la méthode du tri par
insertion*)
let rec tri_insertion liste = match liste with 
  | [] -> []
  |tete::queue ->
      inserer tete (tri_insertion queue)
;;
(*Définir une fonction decouper qui prend en
  entrée un élément donné e et une
liste donnée l et qui 
renvoie en sortie le couple formé de la liste des éléments de l
qui sont plus petits que e et de la liste
des éléments de l qui sont plus grands que e.*)
let couple_de_liste = ([], [])
  
  
 
let rec decouper_pas_efficace elem liste  = match liste with 
  | [] -> ([], [])
  | tete::queue -> if tete <= elem 
      then (tete:: (fst(decouper_pas_efficace elem queue)),
            tete::(snd(decouper_pas_efficace elem queue)))
      else (fst(decouper_pas_efficace elem queue),
            tete::(snd(decouper_pas_efficace elem queue)))
;;
                  
                  
            
            
            
let rec decouper elem liste = match liste with 
  | [] -> ([], [])
  |tete::queue -> 
      let decoupage_queue = decouper elem queue in 
      if tete <= elem 
      then (tete::(fst decoupage_queue), snd decoupage_queue)
      else (fst decoupage_queue, tete::(snd decoupage_queue));;
                                  
let rec concatener liste1 liste2 = match liste1 with 
  |[] -> liste2
  |tete1::queue1 -> tete1 :: (concatener queue1 liste2)
                             
                             
let rec tri_rapide liste = match liste with 
  |[] -> []
  |tete::queue ->
      let decoupage = decouper tete queue in 
      let moitie_gauche_triee = tri_rapide (fst decoupage) in 
      let moitie_droite_triee = tri_rapide (snd decoupage) in 
      concatener moitie_gauche_triee (tete moitie_droite_triee)
        
        

let rec decouper_bis liste = match liste with 
  |[] -> ([],[])
  |tete::queue -> 
      let decoupage_queue = decouper_bis queue in 
      (tete::(snd decoupage_queue), fst decoupage_queue)
      
let rec fusion liste1 liste2 =
  match (liste1, liste2) with 
  |([], []) -> []
  |(tete1::queue1, [])-> liste1
  |([], tete2::queue2)-> liste2
  |(tete1::queue1, tete2::queue2)->
      if tete1 <= tete2 
      then 
        tete1::(fusion queue1 liste2)
      else
        tete2::(fusion liste1 queue2)
               

let rec tri_fusion liste = match liste with 
  |[] -> []
  |tete::queue -> (
      if queue = []
      then liste 
      else 
        let decoupage = decouper_bis liste in 
        (* on cacule decouper_bis liste et on le mémorise dans la variable
           decoupage*)
        fusion (tri_fusion (fst decoupage)) (tri_fusion (snd decoupage))
          
    );;

(* ensuite : on fait le tp 3 *)

type arbre = Vide | Noeud of int * arbre * arbre ;;
(*Ceci signifie qu'un arbre  est :
  *soit Vide
  *soit Noeud (x,y,z) où x est un int 
                         y est arbre 
                         z est arbre 
                  par exemple  x = 8, y= Vide, z=Vide *)






let exemple_arbre = Noeud (5, Noeud (1, Vide, Vide), Noeud (8, Vide, Vide));;
      
(*              5
                /\
                1 8  *) 
(* Définir une fonction appartient de type int -> arbre -> bool qui indique si l’entier
appartient ou non à l’ABR *)

let rec appartient element arbre = match arbre with 
  |Vide -> false
  |Noeud (racine, fils_gauche, fils_droit) ->
      if element = racine 
      then true 
      else if element < racine 
      then appartient element  fils_gauche 
      else 
        appartient element fils_droit ;;


(* Définir une fonction arbre_2_liste de type arbre -> int list 
   qui transforme un ABR en une liste triée *)
(* question 2 : conversion ABR vers une liste trié 
   l'idée st de faire un parcours infixe 
   on pourra utiliser sur une fonction de concatenation des (cf tp1 tp2 ) *)

let rec arbre_2_liste arbre = match arbre with 
  |Vide -> [] 
  | Noeud (racine, fils_gauche, fils_droit) ->
      concatener (arbre_2_liste fils_gauche)
        (racine::(arbre_2_liste fils_droit));; 






(*Un première manière d’insérer un entier n dans un ABR 
a qui ne le contient pas consiste à
parcourir a jusqu’à l’endroit où devrait 
se trouver n et à l’y insérer.
Définir une telle fonction inserer_bas qui 
aura pour type int -> arbre -> arbre.*)
let rec inserer_bas element arbre = match arbre with 
  |Vide -> Noeud (element, Vide, Vide)
  |Noeud(racine, fils_gauche, fils_droit) ->
      if element < racine 
      then  Noeud(racine, inserer_bas element fils_gauche, fils_droit)
      else 
        Noeud(racine, fils_gauche, inserer_bas element fils_droit)


(*  Déduire de tout ce qui précède une fonction de tri 
   de type int list -> int list qui utilise
un ABR. (On construira d’abord un ABR
   à partir de la liste initiale en utilisant l’une ou l’autre des
deux fonctions d’insertion 
avant de transformer cet ABR en une liste triée grâce à arbre_2_liste).*)
let rec liste_pas_triee_vers_ABR liste = match liste with 
  |[] -> Vide
  |tete::queue -> inserer_bas tete (liste_pas_triee_vers_ABR queue);;

let tri_abr liste = arbre_2_liste (liste_pas_triee_vers_ABR liste ) ;;

































































