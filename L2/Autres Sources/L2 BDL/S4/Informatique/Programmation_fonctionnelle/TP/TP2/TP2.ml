(*
#use "TP2.ml" ;;
*)

(*Tri Rapide*)
(*1 avec découpage ordonné*)

let liste = [9;8;7;6;5;4;3;2;1;99;77;55];;
let listeSimple = [4;3;2;1];;

let rec longueur l =
  match l with
  | [] -> 0
  | e :: t -> 1 + longueur t;;
(*__________________________________________________*)

let rec coll l1 l2 =
        match (l1, l2) with
        |([], _) -> l2
        |(_, []) -> l1
        |(t1::q1, l2) -> t1 :: coll q1 l2;;
(*__________________________________________________*)

let rec decoupage_1 l e =
  match l with
  | [] -> [],[]
  | x :: ll -> let a,b = decoupage_1 ll e
  							in if(x < e)
  								then 
  									(x::a), b
  								else
  									a, (x::b);;
(*__________________________________________________*)

let rec tri_1 tab =
  match tab with
  | [] -> []
  | h :: t -> let l1, l2 = decoupage_1 t h  in
  coll (tri_1 l1) (h::(tri_1 l2));;
(*__________________________________________________*)

tri_1 liste;;


(*Tri Fusion*)
(*2 avec découpage hasard*)

(*__________________________________________________*)

let rec decoupage_2 l = 
	match l with
	| [] -> [], []
	| h :: t -> 
		(match t with
			| [] -> (h :: []), []
			| ht :: tt -> 
				let l1, l2 = decoupage_2 tt in
  					(h :: l1), (ht :: l2)
  	)
  	;;


(*__________________________________________________*)

(* Coll 2  *)
let rec coll_2 (l1, l2) = 
	match (l1, l2) with
	| [], [] -> []
	| h1 :: t1, [] -> l1
	| [], h2 :: t2 -> l2
	| h1 :: t1, h2 :: t2 ->
		if(h2 < h1)
		then
			h2 :: coll_2 (l1, t2)
		else
			h1 :: coll_2 (t1, l2)
	;;

	
(*__________________________________________________*)

(*
	let rec tri_2 tab =
  match tab with
  | [] -> []
  | [e]-> [e]
  | _ -> 
  
  		let l1, l2 = decoupage_2 tab in
  				coll_2((tri_2 l1),(tri_2 l2)) 

  ;;*)

let rec tri_2 tab =
  match tab with
  | [] -> []
  | h :: t -> 
  	if(t = [])
  	then
  		h :: []
  	else
  		let l1, l2 = decoupage_2 tab in
  			let l3 = tri_2 l1 in
  			let l4 = tri_2 l2 in
  			coll_2 (l3, l4)

  ;;


liste;;
tri_2 liste;;
listeSimple;;
tri_2 listeSimple;;


(*
#use "TP2.ml" ;;
*)

