(*
#use "P2_2018.ml";;
*)


(* Exo 4 *)
type couleur = BLEUE | BLANCHE | ROUGE ;;
type boule = Boule of couleur * int ;;

let bleu5 = Boule (BLEUE, 5);;
let rouge6 = Boule (ROUGE, 6);;


let mesBoules =
  [ Boule (BLEUE,10);
    Boule (BLANCHE,12);
    Boule (BLANCHE,2);
    Boule (ROUGE,3);
    Boule (BLANCHE,2);
    Boule (BLEUE,2);
    Boule (ROUGE,15);
    Boule (BLANCHE,20);
    Boule (ROUGE, 10)] ;;

let dijkstra listeBoules =
  let rec insertionCroissante liste (elem : boule) =
  match liste, elem with
  | [], Boule(_, _) -> [elem]
  | Boule(couleur, valeur) as tete :: queue, Boule(couleurElem, valeurElem)->
    if(valeur < valeurElem)
    then
      tete :: (insertionCroissante queue elem)
    else
      elem :: liste in
    let rec insertionDecroissante liste elem =
    match liste, elem with
    | [], Boule(_, _) -> [elem]
    | Boule(couleur, valeur) as tete :: queue, Boule(couleurElem, valeurElem)->
      if(valeur > valeurElem)
      then
        tete :: (insertionDecroissante queue elem)
      else
        elem :: liste in

  let rec trouveBoulesRouges boules =
  match boules with
  | [] -> []
  | Boule(couleur, valeur) :: resteBoules ->
    if(couleur = ROUGE)
    then
      insertionCroissante (trouveBoulesRouges resteBoules) (Boule(couleur, valeur))
    else
      trouveBoulesRouges resteBoules in

  let rec trouveBoulesBlanches boules =
  match boules with
  | [] -> []
  | Boule(couleur, valeur) :: resteBoules ->
    if(couleur = BLANCHE)
    then
      insertionDecroissante (trouveBoulesBlanches resteBoules) (Boule(couleur, valeur))
    else
      trouveBoulesBlanches resteBoules in

  let rec trouveBoulesBleues boules =
  match boules with
  | [] -> []
  | Boule(couleur, valeur) :: resteBoules ->
    if(couleur = BLEUE)
    then
      insertionCroissante (trouveBoulesBleues resteBoules) (Boule(couleur, valeur))
    else
      trouveBoulesBleues resteBoules in
  let bRouges = trouveBoulesRouges listeBoules in
  let bBlanches = trouveBoulesBlanches listeBoules in
  let bBleues = trouveBoulesBleues listeBoules in
  bRouges @ bBlanches @ bBleues ;;

dijkstra mesBoules;;

(*
#use "P2_2018.ml";;
*)
