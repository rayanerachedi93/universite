(*
#use "P1_2018.ml";;
*)


(* Exo 4 *)

let a = [(7, 8)] ;;

let b = not true in b = not b ;;

let c x y z = 1 ;;


(* Exo 5 *)

(* 1 *)
let listeInt1 = 1 :: 2 :: 3 :: 4 :: 5 :: 6 :: 7 :: 8 :: 9 :: [];;
let listeInt2 = [10; 20; 30; 40; 50; 60; 70; 80; 90];;
let listeIntPair = [2;4;6;8;10;12;14;16;18;20];;
let liste = [9;8;7;6;5;4;3;2;1;99;77;55];;
let liste2 = [6;5;4;3;2;-99;77;55];;

let rec appartient e l =
	match l with
	| [] -> false
	| h::t -> h==e || appartient e t ;;


let rec delta l1 l2 =
	match l1,l2 with
	| [], l2 -> l2
	| l1, [] -> l1
	| h1::t1, h2::t2 ->
		if((appartient h1 l2) || (appartient h2 l1))
		then
			if((appartient h1 l2) && (appartient h2 l1))
			then
				delta t1 t2
			else
				if(not (appartient h1 l2))
				then
					h1::(delta t1 l2)
				else
					h2::(delta l1 t2)
		else
			h1::(delta t1 l2) ;;

liste;;
liste2;;
delta liste liste2;;
liste;;
listeInt1;;
delta liste listeInt1;;
liste;;
listeInt2;;
delta liste listeInt2;;
listeInt1;;
listeInt2;;
delta listeInt1 listeInt2;;

(* 2 *)

(* l1 l2 ordonnés*)
let rec delta2 l1 l2 =
	match l1,l2 with
	| l1, [] -> l1
	| [], l2 -> l2
	| h1::t1, h2::t2 ->
		if(h1 == h2)
		then
			delta t1 t2
		else
			if(h1 < h2)
			then
				h1::(delta t1 l2)
			else
				h2::(delta l1 t2);;


listeInt1;;
listeInt2;;
delta2 listeInt1 listeInt2;;
listeInt1;;
listeIntPair;;
delta2 listeInt1 listeIntPair;;
listeIntPair;;
listeInt2;;
delta2 listeIntPair listeInt2;;


(* Exo 6 *)

(* 1 *)

let rec suite n = 
	match n with
	| 0 -> [0]
	| _ -> (suite (n-1)) @ [n];;

suite 5;;
suite 15;;
suite 1;;
suite 0;;

(* 2 *)

let rec appli f l =
	match l with
	| [] -> []
	| h::t -> (f h)::(appli f t) ;;

suite 5;;
appli (function x -> x-1) [2;5];;
appli (function x -> x-1) (suite 5);;

(* 3 *)

let suite_bis n = appli (function x -> x*x+1) (suite n);;

suite_bis 5;;
suite_bis 15;;
suite_bis 1;;
suite_bis 0;;

(*
#use "P1_2018.ml";;
*)
