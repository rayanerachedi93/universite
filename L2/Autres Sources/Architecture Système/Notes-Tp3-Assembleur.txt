-JSR modifie le registre 7 (IN OUT etc ...)

-C'est mieux de mettre les label a la fin et de mettre dans les BR : BR "si la condition est fausse" label, si on met label juste après BR ca n'aurait aucun sens puisqu'on lirait comme meme le code de label, BR EST UNE INSTRUCTION DE SAUT !!!

-Le OUT va afficher le CODE ASCII de la valeur qui est dans R0

Exo 1 (dans l'exo il demande de mettre -1 dans R0 pas de l'afficher ):
.ORIG x3000
 IN
 LD R1,op1
 LD R2,op2
 ADD R0,R0,R1 
 BRN label
ADD R3,R0,R2
BRP label
HALT
label LD R0,coco
OUT
LD R0,coca
OUT
HALT
op1 .FILL #-48
op2 .FILL #-9
coco .STRINGZ "-"
coca .STRINGZ "1"
.END