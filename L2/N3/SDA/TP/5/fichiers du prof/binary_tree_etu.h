#ifndef BINARY_TREE_H
#define BINARY_TREE_H

typedef char element_t;
struct binary_tree_s {
  element_t label; /* label of the root */
  struct binary_tree_s *left; /* NULL if no left subtrees */
  struct binary_tree_s *right; /* NULL if no right subtree */
};
typedef struct binary_tree_s binary_tree_t;

/********************************************************************/
/*  In the sequel, "tree" always stands for "(rooted) binary tree"  */
/********************************************************************/

/** "Creates" an empty tree and returns its address */
/* (could require that memory be allocated) */
binary_tree_t * empty_tree();

/* Constructs a binary tree from a label and two binary trees */
/* (allocates memory and returns the allocated block's address) */
binary_tree_t * cons_binary_tree(element_t, binary_tree_t *, binary_tree_t *);

/* Frees all memory allocated to a tree ans its subtrees */
void delete_binary_tree(binary_tree_t **);

/** Returns the left subtree of a tree */
/* (requires that the tree be non-empty) */
binary_tree_t * left(const binary_tree_t *);

/** Returns the right subtree of a tree */
/* (requires that the tree be non-empty) */
binary_tree_t * right(const binary_tree_t *);

/** Returns the label of the root of a tree */
/* (requires that the tree be non-empty) */
element_t get_binary_tree_root(const binary_tree_t *);

/** Tests a tree for emptyness */
int is_empty_binary_tree(const binary_tree_t *);

/** Computes recursively and returns the size of a tree */
int size_binary_tree(const binary_tree_t *);

/** Prints a tree (rotated by -\pi/2) */
void show_binary_tree(const binary_tree_t *, int);

#endif
