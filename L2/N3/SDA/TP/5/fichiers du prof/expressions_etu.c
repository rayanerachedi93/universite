#include "binary_tree_char.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef binary_tree_t * link;

link parse(const char *, int);
int eval_tree (const link);
