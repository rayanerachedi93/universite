#include <stdio.h>
#include <stdlib.h>
#include <time.h>
typedef int element_t;

/*************************************************************/
/*   Warning: as this program calls heapsort with an         */
/*   array-based implementation of heaps,                    */
/*   the values in arrays correspond to indices >= 1         */
/*   (the value of index 0 will be considered a fake value)  */
/*************************************************************/

void fix_up_heap(element_t *, int);
void fix_down_heap(element_t *, int, int);
void heap_sort(element_t *, int, int);
void random_array(element_t *, int);
int keyboard_array(element_t *, int);
void print_array(element_t *, int, int);

int root();
int parent(int);
int has_parent(int);
int left_child(int);
int right_child(int);
int has_node(element_t*, int);

void swap(element_t * x, element_t * y) {
  element_t tmp = *x;
  *x = *y;
  *y = tmp;
}


/** Initializes an array of n values (pseudo-random generation of values) */
void random_array(element_t *tab, int n) {
  int j;
  element_t v;
  srand(time(NULL));
  printf("Random generation of the array\n");
  for (j = 0; j < n; ++j) {
    v = 1000*(1.0*rand()/RAND_MAX);
    tab[j+1] = v;
  }
}

/** Initializes an array of at most n values (keyboard capture) */
/* Returns the number of values */
int keyboard_array(element_t * tab, int n) {
  int j = 0;
  element_t v;
  printf("Enter values one by one, enter non-digit to end capture\n");
  while (j < n && scanf("%d", &v) == 1) {
    tab[++j] = v;
  }
  return j-1;
}

/** Prints the values of an array between indices l and r */
void print_array(element_t * tab, int l, int r) {
  int i;
  for (i = l; i <= r; ++i) {
    printf("%3d ", tab[i]);
  }
  printf("\n");
}

int root() {
	return 1;
}

int parent(int n) {
	return n / 2;
}

int has_parent(int n) {
	return n != root();
}

int left_child(int n) {
	return 2*n;
}

int right_child(int n) {
	return 2*n + 1;
}

int has_node(element_t* t, int n) {
	return n <= t[0];
}

void fix_up_heap(element_t* t, int n) {
	
}
