#include "tree.h"
#include <stdio.h>
#include <stdlib.h>

binary_tree_t* parse(char* e, int i) {
	char op = e[i];

	if ((op - '0') >= 0 && (op - '0') < 10) {
		return cons_binary_tree(op, empty_tree(), empty_tree());
	}

	if (op == '+' || op == '*') {
		binary_tree_t* l = parse(e, i + 1);
		binary_tree_t* r = parse(e, i + 1 + binary_tree_size(l));
		return cons_binary_tree(op, l, r);
	} else {
		perror("parsing error");
		exit(1);
	}
}

int eval_tree(binary_tree_t* t) {
	if (get_binary_tree_root(t) == '+') {
		return eval_tree(left(t)) + eval_tree(right(t));
	} else if (get_binary_tree_root(t) == '*') {
		return eval_tree(left(t)) * eval_tree(right(t));
	} else {
		return get_binary_tree_root(t) - '0';
	}
}

int main(int argc, char** argv) {
	binary_tree_t* t = parse(argv[1], 0);
	printf("%d\n", eval_tree(t));
	return 0;
}
