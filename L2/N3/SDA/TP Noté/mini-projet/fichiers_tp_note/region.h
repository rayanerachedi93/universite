#ifndef __REGION__
#define __REGION__

typedef struct region_s  region_t;

/*-----------------------------------------------------------------------------*
 * role            : donner acces au code de la region
 * pre-condition   : (NULL != p)
 * post-conditions : aucune
 *-----------------------------------------------------------------------------*/
int region_get_code(const region_t * p);

/*-----------------------------------------------------------------------------*
 * role            : donner acces au nom du departement
 * pre-condition   : (NULL != p)
 * post-conditions : aucune
 *-----------------------------------------------------------------------------*/
const char * region_get_nom(const region_t * p);

#endif
