#ifndef __INFO_GEO__
#define __INFO_GEO__

#include "commune.h"
#include "departement.h"
#include "academie.h"
#include "region.h"


typedef struct information_geo_s  information_geo_t;

/*-----------------------------------------------------------------------------*
 * role            : charger les informations geographiques utilisees (liste
 *                   des academies, communes, departements et regions)
 * pre-conditions  : aucune
 * valeur renvoyee : pointeurs vers les informations geographiques
 * post-conditions : les informations geographiques ont ete chargees
 *-----------------------------------------------------------------------------*/
information_geo_t * load_information_geo();

/*-----------------------------------------------------------------------------*
 * role            : liberer la memoire allouee par l'appel a load_data_ge pour
 *                   stocker les informations geographiques utilisees (liste
 *                   des academies, communes, departements et regions)
 * pre-conditions  : (p != NULL) et (*p != NULL)
 * post-conditions : la memoire est liberee et (*p == NULL)
 *-----------------------------------------------------------------------------*/
void delete_information_geo(information_geo_t **p);

/*-----------------------------------------------------------------------------*
 * role            : afficher les informations geographiques (liste
 *                   des academies, communes, departements et regions)
 * pre-conditions  : (p != NULL)
 * post-conditions : les informations sont affichees sur la sortie standard
 *-----------------------------------------------------------------------------*/
void show_information_geo(const information_geo_t * p);

/*-----------------------------------------------------------------------------*
 * role            : rechercher une academie a partir de son code
 * pre-conditions  : . (p != NULL)
 *                   . les informations geographiques ont ete chargees
 * valeur renvoyee : NULL si l'academie n'a pas ete trouvee
 * post-conditions : aucune
 *-----------------------------------------------------------------------------*/
const academie_t * search_academie(const information_geo_t * p, int code_acad);

/*-----------------------------------------------------------------------------*
 * role            : rechercher une commune a partir de son code INSEE
 * pre-conditions  : . (p != NULL)
 *                   . (code_com != NULL)
 *                   . (strlen(code_com) == 5)
 *                   . les informations geographiques ont ete chargees
 * valeur renvoyee : NULL si la commune n'a pas ete trouvee
 * post-conditions : aucune
 *-----------------------------------------------------------------------------*/
const commune_t * search_commune(const information_geo_t * p, const char * code_com);

/*-----------------------------------------------------------------------------*
 * role            : rechercher une departement a partir de son code
 * pre-conditions  : . (p != NULL)
 *                   . (code_com != NULL)
 *                   . (strlen(code_dep) == 3)
 *                   . les informations geographiques ont ete chargees
 * valeur renvoyee : NULL si le departement n'a pas ete trouve
 * post-conditions : aucune
 *-----------------------------------------------------------------------------*/
const departement_t * search_departement(const information_geo_t * p, const char * code_dep);

/*-----------------------------------------------------------------------------*
 * role            : rechercher une region a partir de son code
 * pre-conditions  : . (p != NULL)
 *                   . les informations geographiques ont ete chargees
 * valeur renvoyee : NULL si la region n'a pas ete trouvee
 * post-conditions : aucune
 *-----------------------------------------------------------------------------*/
const region_t * search_region(const information_geo_t * p, int code_reg);

#endif
