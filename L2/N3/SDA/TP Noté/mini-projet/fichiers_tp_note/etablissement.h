#ifndef __ETABLISSEMENT__
#define __ETABLISSEMENT__

#include "information_geo.h"

typedef enum {
	STATUT_PUBLIC,
	STATUT_PRIVATE,
	STATUT_NB
} statut_t;

typedef enum {
	NIVEAU_COLLEGE,
	NIVEAU_LYCEE,
	NIVEAU_NB
} niveau_t;

typedef struct etablissement_s etablissement_t;

/*-----------------------------------------------------------------------------*
 * role            : ouvre un flux sur le fichier qui contient les informations
 *                   relatives aux etablissements
 * valeur renvoyee : NULL en cas d'erreurs d'ouverture du fichier
 * post-conditions : la position de lecture est au début de la 2ème du fichier
 *                   (la 1ere ligne indique le nom des colonnes)
 *-----------------------------------------------------------------------------*/
FILE * open_data();

/*-----------------------------------------------------------------------------*
 * role            : charger un enregistrement depuis flux sur le fichier qui
 *                   contient les informations relatives aux etablissements
 * pre-conditions  : . (NULL != fic) && (NULL != data_geo)
 *                   . fic est un flux ouvert avec open_data sur lequel la
 *                     fonction skip_header
 *                   . data_geo a ete initialise par un appel a load_data_geo
 * valeur renvoyee : si la lecture s'est déroulée sans erreur, un pointeur vers
 *                   les informations relatives à l'établissement correspondant
 *                   à la position courante sur le flux avant l'appel, NULL en
 *                   d'erreur.
 * post-conditions : la position dans le flux a été avancée d'un enregistrement
 *-----------------------------------------------------------------------------*/
etablissement_t * load_etablissement(FILE * fic, information_geo_t * data_geo);

/*-----------------------------------------------------------------------------*
 * role            : liberer la memoire allouee par l'appel a load_etablissement
 * pre-conditions  : (p != NULL) et (*p != NULL)
 * valeur renvoyee : aucune
 * post-conditions : la memoire est liberee et (*p == NULL)
 *-----------------------------------------------------------------------------*/
void delete_etablissement(etablissement_t **p);

/*-----------------------------------------------------------------------------*
 * role            : donner acces a l'UAI de l'etablissement
 * pre-condition   : (NULL != p)
 * post-conditions : aucune
 *-----------------------------------------------------------------------------*/
const char * etablissement_get_uai(const etablissement_t * p);

/*-----------------------------------------------------------------------------*
 * role            : donner acces au nom l'etablissement
 * pre-condition   : (NULL != p)
 * post-conditions : aucune
 *-----------------------------------------------------------------------------*/
const char * etablissement_get_nom(const etablissement_t * p);

/*-----------------------------------------------------------------------------*
 * role            : donner acces a la commune de l'etablissement
 * pre-condition   : (NULL != p)
 * post-conditions : aucune
 *-----------------------------------------------------------------------------*/
const commune_t * etablissement_get_commune(const etablissement_t * p);

/*-----------------------------------------------------------------------------*
 * role            : donner acces a la longitude de l'etablissement
 * pre-condition   : (NULL != p)
 * post-conditions : aucune
 *-----------------------------------------------------------------------------*/
double etablissement_get_longitude(const etablissement_t * p);

/*-----------------------------------------------------------------------------*
 * role            : donner acces a la latitude de l'etablissement
 * pre-condition   : (NULL != p)
 * post-conditions : aucune
 *-----------------------------------------------------------------------------*/
double etablissement_get_latitude(const etablissement_t * p);

/*-----------------------------------------------------------------------------*
 * role            : donner acces au statut (public/prive) de l'etablissement
 * pre-condition   : (NULL != p)
 * post-conditions : aucune
 *-----------------------------------------------------------------------------*/
statut_t etablissement_get_statut(const etablissement_t * p);

/*-----------------------------------------------------------------------------*
 * role            : donner acces au niveau (college/lycee) de l'etablissement
 * pre-condition   : (NULL != p)
 * post-conditions : aucune
 *-----------------------------------------------------------------------------*/
niveau_t etablissement_get_niveau(const etablissement_t * p);

#endif
