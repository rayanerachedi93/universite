#ifndef __ACADEMIE__
#define __ACADEMIE__

#include "region.h"

typedef struct academie_s  academie_t;

/*-----------------------------------------------------------------------------*
 * role            : donner acces au code de l'academie
 * pre-condition   : (NULL != pc)
 * post-conditions : aucune
 *-----------------------------------------------------------------------------*/
int academie_get_code(const academie_t * p);

/*-----------------------------------------------------------------------------*
 * role            : donner acces au nom de l'academie
 * pre-condition   : (NULL != pc)
 * post-conditions : aucune
 *-----------------------------------------------------------------------------*/
const char * academie_get_nom(const academie_t * p);

/*-----------------------------------------------------------------------------*
 * role            : donner acces a la region l'academie
 * pre-condition   : (NULL != pc)
 * post-conditions : aucune
 *-----------------------------------------------------------------------------*/
const region_t * academie_get_region(const academie_t * p);

#endif
