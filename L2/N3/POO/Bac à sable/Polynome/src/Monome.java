
/**
 *  Représentation des monomes à coefficients entiers, c'est-à-dire, les
 *  polynomes élémentaires de la forme coeff.x^(degre) où coeff et degre sont
 *  des entiers et degre est un entier positif ou nul.
 *
 * @author     Marc Champesme
 * @version    6 mars 2010
 * @since      31 janvier 2006
 */
public class Monome {
	// Ajouter ici la définition des attributs

	/**
	 *  Initialise un monome nul, c'est-à-dire un monome de degré et de coefficient
	 *  0.
	 *
	 */
	public Monome() {
		// A implémenter
	}


	/**
	 *  Initialise un monome à partir du coefficient et du degre spécifié. Le
	 *  coefficient doit être non nul, et le degré spécifié doit être positif.
	 *
	 * @param  coefficient  L'entier non nul correspondant au coefficient du monome
	 * @param  exposant     L'entier positif correspondant au degré du monome
	 *
	 */
	public Monome(int coefficient, int exposant) {
		// A implémenter
	}


	/**
	 *  Renvoie true si ce monome est le monome nul (i.e. monome de coefficient 0).
	 *
	 * @return true si ce monome est le monome nul (i.e. monome de coefficient
	 *      0) ; false sinon.
	 */
	public boolean estZero() {
		return false; 	// A remplacer par une implémentation correcte
	}


	/**
	 *  Renvoie le coefficient du terme de ce monome (considéré ici comme un
	 *  polynome) dont l'exposant est spécifié.
	 *
	 * @param  exp  L'exposant du terme dont on cherche le coefficient
	 * @return      le coefficient du terme de ce monome dont l'exposant est
	 *      spécifié
	 *
	 */
	public int getCoefficient(int exp) {
		return 0;	// A remplacer par une implémentation correcte
	}


	/**
	 *  Renvoie le coefficient de ce monome.
	 *
	 * @return    le coefficient de ce monome
	 *
	 */
	public int getCoefficient() {
		return 0;	// A remplacer par une implémentation correcte
	}


	/**
	 *  Renvoie le degré de ce monome.
	 *
	 * @return    Le degré de ce monome
	 *
	 */
	public int getDegre() {
		return 0;	// A remplacer par une implémentation correcte
	}


	/**
	 *  Renvoie l'exposant de ce monome.
	 *
	 * @return    l'exposant de ce monome
	 *
	 */
	public int getExposant() {
		return 0;	// A remplacer par une implémentation correcte
	}


	/**
	 *  Renvoie la valeur du monome pour la valeur spécifiée du paramètre.
	 *
	 * @param  x  valeur du parametre pour laquelle est evalué le monome
	 * @return    valeur du monome en x
	 *
	 */
	public double evaluer(double x) {
		return 0;	// A remplacer par une implémentation correcte
	}


	/**
	 *  Renvoie une nouvelle instance de PolynomeDense dont la valeur correspond à
	 *  l'addition du monome spécifié à ce monome.
	 *
	 * @param  m  Le monome (de meme degré que ce monome) à additionner à ce monome
	 * @return    Polynome somme de ce monome avec le monome spécifié
	 */
	public PolynomeDense additionner(Monome m) {
		return null;	// A remplacer par une implémentation correcte
	}


	/**
	 *  Renvoie true si et seulement si l'objet spécifié est un Monome de
	 *  m&ecirc;me coefficient et meme degre que ce monome.
	 *
	 * @param  o  L'objet &agrave; comparer avec ce monome
	 * @return    true si et seulement si l'objet spécifié est un Monome de
	 *      m&ecirc;me coefficient et meme degre que ce monome ; false sinon.
	 */
	public boolean equals(Object o) {
		return false;	// A remplacer par une implémentation correcte
	}


	/**
	 *  Renvoie le code de hashage de ce monome.
	 *
	 * @return    le code de hashage de ce monome
	 */
	public int hashCode() {
		return 0;	// A remplacer par une implémentation correcte
	}


	/**
	 *  Renvoie une brève représentation textuelle de ce polynome de la forme
	 *  "Monome: 3.X^3 + 56.X^7". Si le polynome est zero (i.e. this.estZero() est
	 *  true) la chaine "Monome: zero" est retournée.
	 *
	 * @return    une brève représentation textuelle de ce polynome
	 */
	public String toString() {
		return null;	// A remplacer par une implémentation correcte
	}

}

