import java.util.ArrayList;

/**
 *  Polynomes non modifiables &agrave; coefficients entiers de la forme c0 +
 *  c1*x +c2*x^2 + ...
 *
 * @invariant getDegre() >= 0;
 * @invariant (getDegre() > 0) ==> (getCoefficient(getDegre()) !=0);
 * @invariant estZero() <==> ((getDegre() == 0) && (getCoefficient(0) == 0));
 * @invariant !estZero() <==> (getCoefficient(getDegre()) !=0);
 * @invariant evaluer(0) == getCoefficient(0);
 *
 * @author     Marc Champesme
 * @version    6 mars 2011
 * @since      31 janvier 2006
 */
public class PolynomeDense {
	/*@
	  @ invariant getDegre() >= 0;
	  @ invariant (getDegre() > 0) ==> (getCoefficient(getDegre()) !=0);
	  @ invariant estZero() <==> ((getDegre() == 0) && (getCoefficient(0) == 0));
	  @ invariant !estZero() <==> (getCoefficient(getDegre()) !=0);
	  @ invariant evaluer(0) == getCoefficient(0);
	  @*/

	private ArrayList<Integer> coefficients;

	/**
	 *  Initialise un polynome nul.
	 *
	 * @ensures estZero();
	 */
	 /*@
	   @ ensures estZero();
	   @*/
	public PolynomeDense() {
		coefficients = new ArrayList<Integer>();
	}


	/**
	 *  Initialise un polynome composé d'un unique terme d'exposant exp et de
	 *  coefficient coeff non nul.
	 *
	 * @param  coeff  Coefficient du terme d'exposant exp du nouveau polynome
	 * @param  exp    exposant de l'unique terme de ce polynome
	 *
	 * @requires coeff != 0;
	 * @requires exp >= 0;
	 * @ensures getDegre() == exp;
	 * @ensures getCoefficient(exp) == coeff;
	 * @ensures !estZero();
	 */
	/*@
	  @ requires coeff != 0;
	  @ requires exp >= 0;
	  @ ensures getDegre() == exp;
	  @ ensures getCoefficient(exp) == coeff;
	  @ ensures !estZero();
	  @*/
	public PolynomeDense(int coeff, int exp) {
		coefficients = new ArrayList<Integer>(exp + 1);

		for (int i = 0; i < exp; i++) {
			coefficients.set(i, 0);
		}

		coefficients.set(exp, coeff);
	}

	/**
	 *  Renvoie true si et seulement si tous les coefficients de ce polynome sont
	 *  nuls.
	 *
	 * @return    true si ce polynome est le polynome zero ; false sinon
	 *
	 * @ensures \result <==> (\forall int exp; exp >= 0 && exp <= getDegre();
	 *				getCoefficient(exp) == 0);
	 * @ensures \result <==> ((getDegre() == 0) && (getCoefficient(0) == 0));
	 * @pure
	 */
	/*@
	  @ ensures \result <==> (\forall int exp; exp >= 0 && exp <= getDegre();
	  @				getCoefficient(exp) == 0);
	  @ ensures \result <==> ((getDegre() == 0) && (getCoefficient(0) == 0));
	  @ pure
	  @*/
	public boolean estZero() {
		return getDegre() == 0;
	}


	/**
	 *  Renvoie un nouveau polynome correspondant &agrave; la somme de ce polynome
	 *  et du polynome spécifié.
	 *
	 * @param  p  Le polynome &agrave; additionner avec ce polynome.
	 * @return    La somme de ce polynome avec le polynome spécifié.
	 *
	 * @requires p != null;
	 * @ensures \result != null;
	 * @ensures (\result != this) && (\result != p);
	 * @ensures p.estZero() ==> \result.equals(this);
	 * @ensures this.estZero() ==> \result.equals(p);
	 * @ensures (this.getDegre() != p.getDegre()) ==> (\result.getDegre() == Math.max(this.getDegre(), p.getDegre()));
	 * @ensures (\forall int exp; exp >= 0 && exp <= \result.getDegre();
	 *			\result.getCoefficient(exp) == this.getCoefficient(exp) + p.getCoefficient(exp));
	 * @ensures (this.getDegre() == p.getDegre()) 
	 * && ((this.getCoefficient(this.getDegre()) + p.getCoefficient(p.getDegre())) != 0)
	 *			==> (\result.getDegre() == this.getDegre());
	 * @ensures (this.getDegre() == p.getDegre()) 
	 * && ((this.getCoefficient(this.getDegre()) + p.getCoefficient(p.getDegre())) == 0)
	 *			==> (\result.getDegre() < this.getDegre());
	 *
	 * @pure
	 */
	/*@
	  @ requires p != null;
	  @ ensures \result != null;
	  @ ensures (\result != this) && (\result != p);
	  @ ensures p.estZero() ==> \result.equals(this);
	  @ ensures this.estZero() ==> \result.equals(p);
	  @ ensures (this.getDegre() != p.getDegre()) 
	  @		==> (\result.getDegre() == Math.max(this.getDegre(), p.getDegre()));
	  @ ensures (\forall int exp; exp >= 0 && exp <= \result.getDegre();
	  @			\result.getCoefficient(exp) == this.getCoefficient(exp) + p.getCoefficient(exp));
	  @ ensures (this.getDegre() == p.getDegre()) 
	  @	&& ((this.getCoefficient(this.getDegre()) + p.getCoefficient(p.getDegre())) != 0)
	  @			==> (\result.getDegre() == this.getDegre());
	  @ ensures (this.getDegre() == p.getDegre()) 
	  @	&& ((this.getCoefficient(this.getDegre()) + p.getCoefficient(p.getDegre())) == 0)
	  @			==> (\result.getDegre() < this.getDegre());
	  @
	  @ pure
	  @*/
	public PolynomeDense additionner(PolynomeDense p) {
		PolynomeDense r = new PolynomeDense();
		
		for (int i = 0; i <= Math.max(getDegre(), getDegre()); i++) {
			r.coefficients.set(i, getCoefficient(i) + p.getCoefficient(i));
		}
		
		return r;
	}


	/**
	 *  Calcule et renvoie la valeur de ce polynome pour la valeur du paramètre
	 *  spécifiée
	 *
	 * @param  x  Valeur du parametre du polynome
	 * @return    Valeur de ce polynome pour la valeur spécifiée
	 *
	 * @ensures estZero() ==> (\result == 0);
	 * @ensures (x == 0) ==> (\result == getCoefficient(0));
	 * @ensures (x == 1) ==> (\result == (\sum int exp; exp >= 0 && exp <= getDegre();
	 *						getCoefficient(exp)))
	 *
	 * @pure
	 */
	/*@
	  @ ensures estZero() ==> (\result == 0);
	  @ ensures (x == 0) ==> (\result == getCoefficient(0));
	  @ ensures (x == 1) ==> (\result == (\sum int exp; exp >= 0 && exp <= getDegre();
	  @						getCoefficient(exp)));
	  @
	  @ pure
	  @*/
	public double evaluer(double x) {
		int r = getCoefficient(0);

		for (int i = 1; i <= getDegre(); i++) {
			r += getCoefficient(0) * Math.pow(x, i);
		}

		return r;
	}


	/**
	 *  Renvoie le coefficient du terme de ce polynome dont l'exposant est l'entier
	 *  spécifié.
	 *
	 * @param  exposant  L'exposant du terme cherché.
	 * @return           Le coefficient du terme dont l'exposant est spécifié

	 * @requires exposant >= 0;
	 * @ensures (* \result est le coefficient du terme d'exposant exposant
	 *			de ce polynome *);
	 * @ensures (!estZero() && (exposant == getDegre())) ==> (\result != 0);
	 * @ensures estZero() ==> (\result == 0);
	 * @pure
	 */
	/*@
	  @ requires exposant >= 0;
	  @ ensures (* \result est le coefficient du terme d'exposant exposant
	  @			de ce polynome *);
	  @ ensures (!estZero() && (exposant == getDegre())) ==> (\result != 0);
	  @ ensures estZero() ==> (\result == 0);
	  @
	  @ pure
	  @*/
	public int getCoefficient(int exposant) {
		return exposant < coefficients.size() ? coefficients.get(exposant) : 0;
	}


	/**
	 *  Renvoie le degré de ce polynome, c'est-&agrave;-dire le plus grand exposant
	 *  de coefficient non nul. Si ce polynome est constant la valeur retournée est
	 *  0, y compris s'il s'agit du polynome zero (i.e. le polynome dont tous les
	 *  coefficients sont nuls).
	 *
	 * @return    Le degré de ce polynome
	 *
	 * @ensures \result >= 0;
	 * @ensures estZero() ==> \result == 0;
	 * @ensures !estZero() ==> getCoefficient(\result) != 0;
	 * @pure
	 */
	/*@
	  @ ensures \result >= 0;
	  @ ensures estZero() ==> \result == 0;
	  @ ensures !estZero() ==> getCoefficient(\result) != 0;
	  @
	  @ pure
	  @*/
	public int getDegre() {
		int i = coefficients.size() - 1;

		for (; i >= 0; i--) {
			if (getCoefficient(i) != 0) {
				break;
			}
		}

		if (i < 0) i = 0;

		return i;
	}


	/**
	 *  Renvoie true si et seulement si l'objet spécifié est un PolynomeDense de
	 *  m&ecirc;mes coefficients que ce polynome.
	 *
	 * @param  o  L'objet &agrave; comparer avec ce polynome
	 * @return    true si et seulement si l'objet spécifié est un PolynomeDense de
	 *      m&ecirc;mes coefficients que ce polynome ; false sinon.
	 *
	 * @ensures !(o instanceof PolynomeDense) ==> !\result;
	 * @ensures (o instanceof PolynomeDense) 
	 * ==> (\result <==> (getDegre() == ((PolynomeDense) o).getDegre())
	 *		    && (\forall int exp; exp >= 0 && exp <= getDegre();
	 *				((PolynomeDense) o).getCoefficient(exp) == this.getCoefficient(exp)));
	 * @ensures \result ==> (this.hashCode() == o.hashCode());
	 * @pure
	 */
	/*@
	  @ ensures !(o instanceof PolynomeDense) ==> !\result;
	  @ ensures (o instanceof PolynomeDense) 
	  @	==> (\result <==> (getDegre() == ((PolynomeDense) o).getDegre())
	  @		    && (\forall int exp; exp >= 0 && exp <= getDegre();
	  @				((PolynomeDense) o).getCoefficient(exp) == this.getCoefficient(exp)));
	  @ ensures \result ==> (this.hashCode() == o.hashCode());
	  @ ensures \result ==> (this.toString() == o.toString());
	  @ pure
	  @*/
	public boolean equals(Object o) {
		return false;	// A remplacer par une implémentation correcte
	}


	/**
	 *  Renvoie le code de hashage de ce polynome
	 *
	 * @return    le code de hashage de ce polynome
	 * @pure
	 */
	//@ pure
	public int hashCode() {
		return 0;	// A remplacer par une implémentation correcte
	}


	/**
	 *  Renvoie une brève représentation textuelle de ce polynome de la forme
	 *  "Poly: 3.X^3 + 56.X^7". Si ce polynome est zero (i.e. this.estZero() est
	 *  true) la chaine "PolynomeDense: zero" est retournée.
	 *
	 * @return    une brève représentation textuelle de ce polynome
	 *
	 * @ensures \result != null;
	 * @pure
	 */
	/*@
	  @ ensures \result != null;
	  @
	  @ pure
	  @*/
	public String toString() {
		return null;	// A remplacer par une implémentation correcte
	}
}

