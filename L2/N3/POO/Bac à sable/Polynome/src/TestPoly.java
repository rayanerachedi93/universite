/**
 *  Une classe de tests superficiels qui teste essentiellement les opérations
 *  d'addition.
 *
 * @author     Marc Champesme
 * @since      21 mai 2006
 * @version    21 mai 2006
 */
public class TestPoly {
	/**
	 *  The main program for the TestPoly class
	 *
	 * @param  args  The command line arguments
	 */
	public static void main(String[] args) {
		PolynomeDense[] tabP;
		PolynomeDense p1;
		PolynomeDense p2;
		tabP = new PolynomeDense[5];
		Monome[] tabM;
		tabM = new Monome[5];

		tabP[0] = new PolynomeDense();
		tabP[1] = new PolynomeDense(12, 2);
		tabP[2] = new PolynomeDense(56, 6);
		tabP[3] = new PolynomeDense(23, 0);
		tabP[4] = new PolynomeDense(-56, 6);
		tabM[0] = new Monome();
		tabM[1] = new Monome(56, 6);
		tabM[2] = new Monome(-23, 0);
		tabM[3] = new Monome(-56, 6);
		tabM[4] = new Monome(44, 3);
		p1 = tabP[0].additionner(tabP[1]);
		System.out.println(tabP[0] + " + " + tabP[1] + " = " + p1);
		p2 = p1.additionner(tabP[2]);
		System.out.println(p1 + " + " + tabP[2] + " = " + p2);
		p1 = p2.additionner(tabP[3]);
		System.out.println(p2 + " + " + tabP[3] + " = " + p1);
		p2 = p1.additionner(tabP[4]);
		System.out.println(p1 + " + " + tabP[4] + " = " + p2);

		p1 = tabM[0].additionner(tabM[1]);
		System.out.println(tabM[0] + " + " + tabM[1] + " = " + p1);
		p2 = tabM[1].additionner(tabM[2]);
		System.out.println(tabM[1] + " + " + tabM[2] + " = " + p2);
		p1 = tabM[2].additionner(tabM[3]);
		System.out.println(tabM[2] + " + " + tabM[3] + " = " + p1);
		p2 = tabM[3].additionner(tabM[1]);
		System.out.println(tabM[3] + " + " + tabM[1] + " = " + p2);
	}
}

