public class Soigneur extends Magicien {
	public Soigneur(String nom, int pv, int niveau, int pmMax, int spi) {
		super(nom, pv, niveau, pmMax, spi);
	}

	public Soigneur(String nom, int pv, int pmMax, int spi) {
		super(nom, pv, pmMax, spi);
	}

	public boolean soigner(Perso p) {
		if (p.estVivant() && payerMagie(5)) {
			p.ajouterVie(getNiveau());
			return true;
		}
		return false;
	}

	public boolean ressussiter(Perso p) {
		if (!p.estVivant() && payerMagie(50)) {
			p.ajouterVie(1);
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "Soigneur " + getNom()
		+ " de niveau " + getNiveau()
		+ ", de vie " + getVie() + "/" + PV_MAX
		+ "hp, de magie " + getPointsMagie() + "/" + getMaxPointsMagie()
		+ "pm et d'esprit " + getEsprit();
	}
}