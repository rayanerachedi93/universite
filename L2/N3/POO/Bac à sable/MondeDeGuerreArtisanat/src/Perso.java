public class Perso {
	private String nom;
	private int niveau;
	private int pv;
	public static final int PV_MAX = 100;

	public Perso(String nom, int pv, int niveau) {
		this.nom = nom;
		this.pv = pv;
		this.niveau = niveau;
	}

	public Perso(String nom, int pv) {
		this(nom, pv, 1);
	}

	public Perso(String nom) {
		this(nom, PV_MAX);
	}

	public String getNom() {
		return nom;
	}

	public int getVie() {
		return pv;
	}

	public int getNiveau() {
		return niveau;
	}

	public boolean estVivant() {
		return (pv > 0);
	}

	public void levelUp() {
		niveau++;
	}

	public void ajouterVie(int vie) {
		pv += vie;
		if (pv > PV_MAX) pv = PV_MAX;
	}

	public void prendreDegats(int deg) {
		pv -= deg;
		if (pv < 0) pv = 0;
	}

	@Override
	public String toString() {
		return "Perso " + nom + " de niveau " + niveau + " et de vie " + pv + "/" + PV_MAX + "hp";
	}

	public void seReposer() {
		if (estVivant())
			ajouterVie(2);
	}
}