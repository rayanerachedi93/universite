public class Barbare extends Guerrier {
	public Barbare(String nom, int pv, int niveau, int end, int atq) {
		super(nom, pv, niveau, end, atq);
	}

	public Barbare(String nom, int pv, int end, int atq) {
		super(nom, pv, end, atq);
	}

	public boolean moiTaper(Perso p) {
		if (getEndurance() < 3)
			return false;
		p.prendreDegats(getAttaque() * 2);
		setEndurance(getEndurance() - 3);
		return true;
	}

	@Override
	public String toString() {
		return "Barbare " + getNom()
		+ " de niveau " + getNiveau()
		+ ", de vie " + getVie() + "/" + PV_MAX
		+ "hp, d'endurance " + getEndurance() + "/" + END_MAX
		+ " et d'attaque " + getAttaque();
	}
}