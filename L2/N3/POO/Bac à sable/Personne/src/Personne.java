public class Personne {
	private String prenom;
	private String nom;
	private Date naissance;
	private String adresse;

	public Personne(String p, String n, Date na, String a) {
		prenom = p;
		nom = n;
		naissance = na;
		adresse = a;
	}

	public String getPrenom() {
		return prenom;
	}

	public String getNom() {
		return nom;
	}
	
	public Date getDateDeNaissance() {
		return naissance;
	}
	
	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String a) {
		adresse = a;
	}

	public void afficher() {
		System.out.printf("%s %s est née le ", prenom, nom);
		naissance.afficher();
		System.out.printf(" et son adresse est « %s ».", adresse);
	}

	public static void main(String[] args) {
		Personne lyes = new Personne("Lyes", "Saadi", new Date(18, 4, 2002), "localhost");
		lyes.afficher();
	}
}