import java.util.Scanner;

public class Cat {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		while (sc.hasNext()) {
			String s = sc.nextLine();
			System.out.print(s);
		}

		sc.close();
	}
}