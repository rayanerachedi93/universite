public class PileBornee<COCO> {
	private Object[] vals;
	private int hauteur;
	private int hauteurMax;

	public static void main(String[] args) {
		PileBornee<Integer> pile = new PileBornee<>(20);

		for (int i = 0; i < 20; i++) {
			pile.push(i);
		}

		pile.retourner();

		for (int i = 0; i < 20; i++) {
			System.out.println(pile.pop());
		}
	}

	public PileBornee(int hauteurMax) {
		vals = new Object[hauteurMax];
		this.hauteurMax = hauteurMax;
		hauteur = 0;
	}

	public int getHauteur() {
		return hauteur;
	}

	public int getHauteurMax() {
		return hauteurMax;
	}

	public void push(COCO e) {
		vals[hauteur++] = e;
	}

	@SuppressWarnings("unchecked")
	public COCO pop() {
		return (COCO) vals[--hauteur];
	}

	public boolean estVide() {
		return hauteur == 0;
	}

	public void retourner() {
		int x = 0;
		int y = hauteur - 1;
		Object t;

		while (x < y) {
			t = vals[x];
			vals[x] = vals[y];
			vals[y] = t;
			x++; y--;
		}
	}
}