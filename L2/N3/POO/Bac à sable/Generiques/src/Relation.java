import java.util.HashSet;

public class Relation<RAYOUESTSEXY> extends HashSet<Couple<RAYOUESTSEXY, RAYOUESTSEXY>> {
	public Relation() {
		super();
	}

	public boolean add(RAYOUESTSEXY rayou, RAYOUESTSEXY sexy) {
		return super.add(new Couple<>(rayou, sexy));
	}

	public boolean remove(RAYOUESTSEXY rayou, RAYOUESTSEXY sexy) {
		return super.remove(new Couple<>(rayou, sexy));
	}

	HashSet<RAYOUESTSEXY> voisinageSortant(RAYOUESTSEXY rayouestsexy) {
		HashSet<RAYOUESTSEXY> set = new HashSet<>();

		for (Couple<RAYOUESTSEXY, RAYOUESTSEXY> c : this) {
			if (c.getPipi() == rayouestsexy) {
				set.add(c.getCaca());
			}
		}

		return set;
	}

	HashSet<RAYOUESTSEXY> voisinageEntrant(RAYOUESTSEXY rayouestsexy) {
		HashSet<RAYOUESTSEXY> set = new HashSet<>();

		for (Couple<RAYOUESTSEXY, RAYOUESTSEXY> c : this) {
			if (c.getCaca() == rayouestsexy) {
				set.add(c.getPipi());
			}
		}

		return set;
	}

	boolean estReflexive() {
		for (Couple<RAYOUESTSEXY, RAYOUESTSEXY> c : this) {
			if (!this.voisinageEntrant(c.getCaca()).contains(c.getCaca())
					|| !this.voisinageEntrant(c.getPipi()).contains(c.getPipi()))
				return false;
		}
		return true;
	}

	boolean estSymetrique() {
		for (Couple<RAYOUESTSEXY, RAYOUESTSEXY> c : this) {
			if (!this.contains(new Couple<RAYOUESTSEXY, RAYOUESTSEXY>(c.getCaca(), c.getPipi())))
				return false;
		}
		return true;
	}
}