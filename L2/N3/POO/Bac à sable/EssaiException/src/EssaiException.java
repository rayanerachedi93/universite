public class EssaiException {
	public static void main(String args[]) {
		int[] t = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

		try {
			for (int i = 0; i <= 15; i++) {
				System.out.println(t[i]);
			}
		} catch (RuntimeException e) {
			System.err.println("Exception levée : dépassement du tableau");
		}
	}
}