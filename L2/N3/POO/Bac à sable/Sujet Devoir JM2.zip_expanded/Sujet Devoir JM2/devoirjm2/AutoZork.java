package devoirjm2;

import devoirjm2.test.DataProvider;

/**
 * Simule une partie du jeu Zork à partir de données aléatoires. Le JoueurMortel
 * gagne la partie lorsqu'il réussi à rester vivant après avoir effectuer
 * MAX_DEPLACEMENT déplacements.
 *
 * @author Marc Champesme
 *
 */
public class AutoZork extends DataProvider {
	public static void main(String[] args) {
		int nbPlays = 10;
		int nbVictoires = 0;
		for (int i = 0; i < nbPlays; i++) {
			AutoZork t = new AutoZork();
			if (t.play()) {
				nbVictoires++;
			}

		}
		System.out.println("===============================");
		System.out.println("**** " + nbVictoires + " victoires pour " + nbPlays + " parties");
		System.out.println("===============================");
	}

	public static final int MAX_DEPLACEMENT = 15;
	public static final int MAX_NB_ACT = 5;

	private JoueurMortel leJoueur;

	/**
	 * Initialise une partie du jeu AutoZork par la création d'une nouvelle instance
	 * de JoueurMortel, d'un ensemble de pièces reliées entre-elles et la
	 * répartition d'Aliments entre les Piece et le JoueruMortel.
	 */
	public AutoZork() {
		leJoueur = newJoueurMortel();
	}

	/**
	 * Lance une partie du jeu AutoZork. La partie se termine lorsque le
	 * JoueurMortel a réussi a effectuer MAX_DEPLACEMENT déplacements ou bien
	 * lorsque le JoueurMortel est mort.
	 *
	 * Chaque partie est une répétition de la séquence d'actions suivante:
	 * <ol>
	 * 	<li>Le joueur ramasse de 0 à MAX_NB_ACT Aliment choisis dans la
	 * 		pièce courante</li>
	 * 	<li>Le Joueur mange tant que son garde-manger n'est pas vide et que son
	 * 		capital vie est inférieur au MAX_CAPITAL_VIE</li>
	 * 	<li>Le Joueur se déplace en empruntant une des sorties de la pièce
	 * 		courante</li>
	 * </ol>
	 */
	public boolean play() {
		int deplacements = 0;
		while (leJoueur.estVivant() && deplacements < MAX_DEPLACEMENT) {
			System.out.println("======================");
			Aliment al;
			afficherJoueur();
			// Ramasser
			int nbActions = randInt(MAX_NB_ACT);
			while (leJoueur.estVivant() && !leJoueur.getPieceCourante().isEmpty() && nbActions > 0) {
				al = choixAliment(leJoueur.getPieceCourante());
				leJoueur.ramasser(al);
				System.out.println(leJoueur.getNom() + " a ramassé un " + al);
				nbActions--;
			}
			if (leJoueur.getPieceCourante().isEmpty()) {
				System.out.println("La pièce " + leJoueur.getPieceCourante().descriptionCourte() + " est vide ");
			}
			// Manger
			while (leJoueur.estVivant() && !leJoueur.isEmpty() && leJoueur.getCapitalVie() < JoueurMortel.MAX_CAPITAL_VIE) {
				al = leJoueur.getAliment();
				leJoueur.manger();
				System.out.println(leJoueur.getNom() + " a mangé un " + al);
			}
			// Bouger
			if (leJoueur.estVivant()) {
				Direction d = choixSortie(leJoueur.getPieceCourante());
				leJoueur.deplacer(d);
				System.out.println(leJoueur.getNom() + " s'est déplacé.e vers " + d + " !");
				deplacements++;
			}
		}
		System.out.println("===============================");
		if (!leJoueur.estVivant()) {
			System.out.println(leJoueur.getNom() + " est mort.e !");
		} else {
			System.out.println("Victoire !!! " + leJoueur.getNom() + " a gagné.e avec " + leJoueur.getCapitalVie() + " points de vie !");
		}
		System.out.println(leJoueur.getNom() + " s'est déplacé.e " + deplacements + " fois");
		System.out.println("===============================");
		return leJoueur.estVivant();
	}

	/**
	 * Choisit un Aliment dans la pièce spécifiée.
	 *
	 * @param p la pièce dans laquelle on choisit l'élément
	 * @return l'élément choisit
	 *
	 * @requires !p.isEmpty();
	 * @ensures \result != null;
	 * @ensures p.contains(\result);
	 *
	 * @pure
	 */
	public Aliment choixAliment(Piece p) {
		return getRandomElt(p);
	}

	/**
	 * Affiche les principales caractéristiques du JoueurMortel.
	 *
	 * @pure
	 */
	public void afficherJoueur() {
		System.out.println("Nom:" + leJoueur.getNom());
		System.out.println("Capital vie:" + leJoueur.getCapitalVie());
		System.out.println(leJoueur.getPieceCourante().descriptionLongue());
		System.out.println("La pièce courante contient: " + leJoueur.getPieceCourante());
		System.out.println("Mon garde-manger:" + leJoueur);
	}
}
