package devoirjm2.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullSource;

import devoirjm2.Aliment;
import devoirjm2.LinkedListCollection;

/**
 * Test class for LinkedListCollection.
 *
 * Implémentation de l'interface Collection utilisant une LinkedList pour
 * mémoriser les éléments de la collection. Cette implémentation permet de :
 * représenter des collections modifiables ne pouvant pas contenir la valeur
 * null. Ajout de quelques méthodes permettant de gérer les occurences multiples
 * des éléments appartenant à une Collection.
 */
public class TestLinkedListCollection extends DataProvider {
	private Collection<Aliment> state;

	private void saveState(LinkedListCollection<Aliment> self) {
		// Put here the code to save the state of self:
		state =  new LinkedList<Aliment>(self);
	}

	private void assertPurity(LinkedListCollection<Aliment> self) {
		// Put here the code to check purity for self:
		assertTrue(self.contentEquals(state));
	}

	public void assertInvariant(LinkedListCollection<Aliment> self) {
		// Put here the code to check the invariant:
		// @invariant !contains(null);
		assertFalse(self.contains(null));
		// @invariant (\forall E obj; contains(obj); frequency(obj) <= size());
		for (Aliment al : self) {
			assertTrue(self.frequency(al) <= self.size());
		}
	}

	/**
	 * Test method for constructor LinkedListCollection
	 *
	 * Initialise une Collection vide.
	 */
	@ParameterizedTest
	@NullSource
	public void testLinkedListCollection(Object useless) {

		// Pré-conditions:

		// Oldies:

		// Exécution:
		LinkedListCollection<Aliment> result = new LinkedListCollection<Aliment>();

		// Post-conditions:
		assertTrue(result.isEmpty());

		// Invariant:
		assertInvariant(result);
	}

	/**
	 * Test method for constructor LinkedListCollection
	 *
	 * Initialise une Collection contenant les éléments de la Collection spécifiée.
	 * 
	 * @throws NullPointerException si la Collection spécifiée est null ou contient
	 *                              null
	 */
	@ParameterizedTest
	@MethodSource("collectionProvider")
	public void testLinkedListCollection(Collection<? extends Aliment> c) {

		// Pré-conditions:
		// @requires c != null;
		// @requires !c.contains(null);
		if (c == null || c.contains(null)) {
			assertThrows(NullPointerException.class, () -> new LinkedListCollection<Aliment>(c));
			return;
		}

		// Oldies:

		// Exécution:
		LinkedListCollection<Aliment> result = new LinkedListCollection<Aliment>(c);

		// Post-conditions:
		// @ensures containsAll(c) && c.containsAll(this);
		assertTrue(result.containsAll(c));
		assertTrue(c.containsAll(result));
		// @ensures size() == c.size();
		assertEquals(c.size(), result.size());

		// Invariant:
		assertInvariant(result);
	}

	/**
	 * Test method for method frequency
	 *
	 * Renvoie le nombre d'occurences de l'objet spécifié dans cette Collection.
	 */
	@ParameterizedTest
	@MethodSource("instanceAndEltProvider")
	public void testfrequency(LinkedListCollection<Aliment> self, Object o) {
		assumeTrue(self != null);

		// Invariant:
		assertInvariant(self);

		// Pré-conditions:

		// Save state for purity check:
		saveState(self);

		// Oldies:

		// Exécution:
		int result = self.frequency(o);

		// Post-conditions:
		// @ensures \result >= 0 && \result <= size();
		assertTrue(result >= 0 && result <= self.size());
		// @ensures \result > 0 <==> contains(o);
		assertEquals(self.contains(o), result > 0);

		// Assert purity:
		assertPurity(self);

		// Invariant:
		assertInvariant(self);
	}

	/**
	 * Test method for method selectDuplicates
	 *
	 * Renvoie un ensemble contenant tous les éléments de cette Collection qui sont
	 * présents en plusieurs exemplaires.
	 */
	@ParameterizedTest
	@MethodSource("LinkedListCollProvider")
	public void testselectDuplicates(LinkedListCollection<Aliment> self) {
		assumeTrue(self != null);

		// Invariant:
		assertInvariant(self);

		// Pré-conditions:

		// Save state for purity check:
		saveState(self);

		// Oldies:

		// Exécution:
		Set<Aliment> result = self.selectDuplicates();

		// Post-conditions:
		// @ensures \result != null;
		assertNotNull(result);
		// @ensures \result.size() <= size() / 2;
		assertTrue(result.size() <= self.size() / 2);
		// @ensures (\forall E elt; \result.contains(elt); frequency(elt) > 1);
		for (Aliment al : result) {
			assertTrue(self.frequency(al) > 1);
		}
		// @ensures (\forall E elt; frequency(elt) > 1; \result.contains(elt));
		for (Aliment al : self) {
			if (self.frequency(al) > 1) {
				assertTrue(result.contains(al));
			}
		}

		// Assert purity:
		assertPurity(self);

		// Invariant:
		assertInvariant(self);
	}

	/**
	 * Test method for method selectUniques
	 *
	 * Renvoie un ensemble contenant tous les éléments de cette Collection qui sont
	 * présents en un seul exemplaire.
	 */
	@ParameterizedTest
	@MethodSource("LinkedListCollProvider")
	public void testselectUniques(LinkedListCollection<Aliment> self) {
		assumeTrue(self != null);

		// Invariant:
		assertInvariant(self);

		// Pré-conditions:

		// Save state for purity check:
		saveState(self);

		// Oldies:

		// Exécution:
		Set<Aliment> result = self.selectUniques();

		// Post-conditions:
		// @ensures \result != null;
		assertNotNull(result);
		// @ensures \result.size() <= size();
		assertTrue(result.size() <= self.size());
		// @ensures (\forall E elt; \result.contains(elt); frequency(elt) == 1);
		for (Aliment al : result) {
			assertTrue(self.frequency(al) == 1);
		}
		// @ensures (\forall E elt; frequency(elt) == 1; \result.contains(elt));
		for (Aliment al : self) {
			if (self.frequency(al) == 1) {
				assertTrue(result.contains(al));
			}
		}

		// Assert purity:
		assertPurity(self);

		// Invariant:
		assertInvariant(self);
	}

	/**
	 * Test method for method removeAll
	 *
	 * Retire de cette Collection toutes les occurences de l'objet spécifié.
	 */
	@ParameterizedTest
	@MethodSource("instanceAndEltProvider")
	public void testremoveAll(LinkedListCollection<Aliment> self, Aliment e) {
		assumeTrue(self != null);

		// Invariant:
		assertInvariant(self);

		// Pré-conditions:

		// Oldies:
		// old in:@ensures \result == \old(frequency(e));
		int oldFreq = self.frequency(e);
		// old in:@ensures size() == \old(size() - frequency(e));
		int oldSize = self.size();

		// Exécution:
		int result = self.removeAll(e);

		// Post-conditions:
		// @ensures \result == \old(frequency(e));
		assertEquals(oldFreq, result);
		// @ensures !contains(e);
		assertFalse(self.contains(e));
		// @ensures size() == \old(size() - frequency(e));
		assertEquals(oldSize - oldFreq, self.size());

		// Invariant:
		assertInvariant(self);
	}

	/**
	 * Test method for method addNCopiesOf
	 *
	 * Ajoute à cette collection le nombre d'occurences spécifié de l'objet
	 * spécifié.
	 * 
	 * @throws IllegalArgumentException si l'entier spécifié est strictement négatif
	 * @throws NullPointerException     si l'objet spécifié est null
	 */
	@ParameterizedTest
	@MethodSource("instanceAndIntAndEltProvider")
	public void testaddNCopiesOf(LinkedListCollection<Aliment> self, int n, Aliment e) {
		assumeTrue(self != null);

		// Invariant:
		assertInvariant(self);

		// Pré-conditions:
		// @requires e != null;
		// @requires n >= 0;
		if (e == null || n < 0) {
			Throwable ex = assertThrows(Throwable.class, () -> self.addNCopiesOf(n, e));
			if (ex instanceof IllegalArgumentException) {
				assertTrue(n < 0);
			} else if (ex instanceof NullPointerException) {
				assertNull(e);
			} else {
				fail("Exception inattendue" + ex);
			}
			return;
		}

		// Oldies:
		// old in:@ensures size() == \old(size()) + n;
		int oldSize = self.size();
		// old in:@ensures frequency(e) == \old(frequency(e)) + n;
		int oldFreq = self.frequency(e);

		// Exécution:
		self.addNCopiesOf(n, e);

		// Post-conditions:
		// @ensures n > 0 ==> contains(e);
		if (n > 0) {
			assertTrue(self.contains(e));
		}
		// @ensures size() == \old(size()) + n;
		assertEquals(oldSize + n, self.size());
		// @ensures frequency(e) == \old(frequency(e)) + n;
		assertEquals(oldFreq + n, self.frequency(e));

		// Invariant:
		assertInvariant(self);
	}

	/**
	 * Test method for method contentEquals
	 *
	 * Renvoie true si la Collection spécifiée contient les même éléments que cette
	 * Collection.
	 * 
	 * @throws NullPointerException si la Collection spécifiée est null
	 */
	@ParameterizedTest
	@MethodSource("instanceAndCollProvider")
	public void testcontentEquals(LinkedListCollection<Aliment> self, Collection<?> c) {
		assumeTrue(self != null);

		// Invariant:
		assertInvariant(self);

		// Pré-conditions:
		// @requires c != null;
		if (c == null) {
			assertThrows(NullPointerException.class, () -> self.contentEquals(c));
			return;
		}

		// Save state for purity check:
		saveState(self);

		// Oldies:

		// Exécution:
		boolean result = self.contentEquals(c);

		// Post-conditions:
		// @ensures \result ==> (size() == c.size() && containsAll(c) && c.containsAll(this));
		if (result) {
			assertEquals(c.size(), self.size());
			assertTrue(self.containsAll(c));
			assertTrue(c.containsAll(self));
		}
		// @ensures isEmpty() && c.isEmpty() ==> \result;
		if (self.isEmpty() && c.isEmpty()) {
			assertTrue(result);
		}

		// Assert purity:
		assertPurity(self);

		// Invariant:
		assertInvariant(self);
	}
} // End of the test class for LinkedListCollection
