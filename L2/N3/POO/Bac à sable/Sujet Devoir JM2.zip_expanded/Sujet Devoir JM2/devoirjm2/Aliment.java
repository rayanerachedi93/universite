package devoirjm2;

/**
 * Un Aliment dans le jeu d'aventure Zork. Un Aliment est caractérisé par son
 * nom et sa valeur nutritive.
 * 
 * @invariant getNom() != null;
 * @invariant getValeurNutritive() > 0;
 *
 * @author Marc Champesme
 * @since 6 mars 2013
 * @version 29 noveambre 2022
 */
public class Aliment {
	private String nom;
	private int valeurNutritive;

	/**
	 * Initialise un Aliment dont le nom et la valeur nutritive sont les valeurs
	 * spécifiées.
	 * 
	 * @param nom             le nom de cet Aliment
	 * @param valeurNutritive la valeur nutritive de cet Aliment
	 * 
	 * @requires nom != null;
	 * @requires valeurNutritive > 0;
	 * @ensures getNom().equals(nom);
	 * @ensures getValeurNutritive() == valeurNutritive;
	 * 
	 * @throws NullPointerException si le nom spécifié est null
	 * @throws IllegalArgumentException si l'entier spécifié est inférieur ou égal à zéro
	 */
	public Aliment(String nom, int valeurNutritive) {
		if (nom == null) {
			throw new NullPointerException("Le nom spécifié ne doit pas être null");
		}
		this.nom = nom;
		if (valeurNutritive <= 0) {
			throw new IllegalArgumentException("La valeur nutritive doit être > 0");
		}
		this.valeurNutritive = valeurNutritive;
	}

	/**
	 * Renvoie le nom de cet Aliment.
	 * 
	 * @return le nom de cet Aliment
	 * 
	 * @pure
	 */
	public String getNom() {
		return this.nom;
	}

	/**
	 * Renvoie la valeur nutritive de cet Aliment.
	 * 
	 * @return la valeur nutritive de cet Aliment
	 * 
	 * @pure
	 */
	public int getValeurNutritive() {
		return this.valeurNutritive;
	}

	/**
	 * Renvoie true si l'objet spécifié est un Aliment possèdant le même nom et la
	 * même valeur nutritive que cet Aliment.
	 * 
	 * @return true si l'objet spécifié est un Aliment possèdant le même nom et la
	 *         même valeur nutritive que cet Aliment; renvoie false sinon
	 *         
	 * @ensures \result <==> (obj instanceof Aliment) 
	 * 						&& getNom().equals(((Aliment) obj).getNom())
	 * 						&& getValeurNutritive() == ((Aliment) obj).getValeurNutritive();
	 * @ensures \result ==> hashCode() == obj.hashCode();
	 * 
	 * @pure
	 */
    @Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Aliment)) {
			return false;
		}

		Aliment unAliment = (Aliment) obj;
		if (!this.getNom().equals(unAliment.getNom())) {
			return false;
		}

		return this.getValeurNutritive() == unAliment.getValeurNutritive();
	}

	/**
	 * Renvoie un hashcode pour cet Aliment.
	 * 
	 * @return un hashcode pour cet Aliment
	 * 
	 * @pure
	 */
    @Override
	public int hashCode() {
		return this.getNom().hashCode() * 31 + this.getValeurNutritive();
	}

	/**
	 * Renvoie une représentation de cet Aliment sous forme de chaîne de caractères.
	 * 
	 * @return une représentation de cet Aliment sous forme de chaîne de caractères
	 * 
	 * @pure
	 */
    @Override
	public String toString() {
		return "Aliment:" + this.getNom() + ":" + this.getValeurNutritive();
	}
}
