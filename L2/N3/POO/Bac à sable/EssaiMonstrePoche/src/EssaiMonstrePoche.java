public class EssaiMonstrePoche {
	public static void main(String[] args) {
		System.out.println("hi!");
		
		MonstrePoche pa = new MonstrePoche("pique-atchoum", 10, 50, 1);
		MonstrePoche bb = new MonstrePoche("bulle-bizarre", 15, 35, 1);
		MonstrePoche tc = new MonstrePoche("tente-à-couille", 5, 100, 1);
		MonstrePoche sm = new MonstrePoche("sallam-wesh", 13, 48, 1);

		pa.afficher();
		bb.afficher();
		pa.combatAMortAvec(bb);
		pa.afficher();
		bb.afficher();

		tc.afficher();
		sm.afficher();
		tc.combatAMortAvec(sm);
		tc.afficher();
		sm.afficher();
	}
}