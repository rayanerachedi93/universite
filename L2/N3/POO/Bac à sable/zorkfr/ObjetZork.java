public class ObjetZork {
	private String nom;
	private boolean transportable;
	private int poids;

	public ObjetZork(String n, boolean t, int p) {
		nom = n;
		transportable = t;
		poids = p;
	}

	public String getNom() {
		return nom;
	}
	
	public boolean isTransportable() {
		return transportable;
	}
	
	public int getPoids() {
		return poids;
	}

	public boolean equals(Object o) {
		if (!(o instanceof ObjetZork)) {
			return false;
		}
		
		ObjetZork objet = (ObjetZork) o;

		return (this.nom == objet.nom)
				&& (this.transportable == objet.transportable)
				&& (this.poids == objet.poids);
	}

	public int hashCode() {
		return ((nom + (transportable ? "OUI BAGUETTE" : "NON, RÉVOLUTION")).hashCode() / 2) + (poids / 2); 
	}
}