public class JeuDeCartes {
	public static void main(String[] args) {
		PileDeCartes jeu = new PileDeCartes();

		for (Famille f : Famille.values()) {
			for (Rang r : Rang.values()) {
				jeu.empiler(new Carte(r, f));
			}
		}

		jeu.empiler(Carte.Joker());
		jeu.empiler(Carte.Joker());

		Carte c;

		while ((c = jeu.piocher()) != null) {
			if (c.isJoker())
				System.out.println("JOKER");
			else
				System.out.println(c.getRang() + " de " + c.getFamille());
		}
	}
}