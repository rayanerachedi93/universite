public class PileDeCartes {
	private Carte[] vals;
	private int hauteur;
	private final static int HAUTEUR_MAX = 54;

	public PileDeCartes() {
		vals = new Carte[HAUTEUR_MAX];
		hauteur = 0;
	}

	public int getHauteur() {
		return hauteur;
	}

	public int getHauteurMax() {
		return HAUTEUR_MAX;
	}

	public void empiler(Carte e) {
		if (hauteur < HAUTEUR_MAX)
			vals[hauteur++] = e;
	}

	public Carte piocher() {
		if (hauteur == 0)
			return null;
		return vals[--hauteur];
	}

	public boolean estVide() {
		return hauteur == 0;
	}
}