package devoirjm;
/**
 * Un Aliment est entièrement caractérisé par son nom (une chaîne de caractères
 * non null de longueur strictement supérieure à 0) et sa valeur nutritive
 * (un entier strictement positif).
 * La classe Aliment doit être implémentée de façon à ce que, une fois créées,
 * les instances de cette classe ne puissent pas être modifiées (i.e. classe
 * non modifiable ou immutable) : les caractéristiques nom et valeur nutritive
 * sont fixées lors de la création de l’instance et ne peuvent pas être modifiées
 * par la suite.
 * Deux instances de la classe Aliment seront considérées comme equals si et s
 * eulement si elles possèdent le même nom et la même valeur nutritive.
 * 
 * @invariant getNom() != null;
 * @invariant getValeurNutritive() > 0;
 * @author Lyes Saadi 12107246
 */
public class Aliment {
	private String nom;
	private int valeurNutritive;

	/**
	 * Initialise un nouvel {@code Aliment}.
	 * 
	 * @param nom le nom de cet {@code Aliment}.
	 * @param valeurNutritive la valeur nutritive de cet {@code Aliment}.
	 * 
	 * @requires nom != null;
	 * @requires valeurNutritive > 0;
	 * @ensures getNom().equals(nom);
	 * @ensures getValeurNutritive() == valeurNutritive;
	 */
	public Aliment(String nom, int valeurNutritive) {
		this.nom = nom;
		this.valeurNutritive = valeurNutritive;
	}
	
	/**
	 * Renvoie le nom de cet {@code Aliment}.
	 * 
	 * @return le nom de cet {@code Aliment}.
	 * 
	 * @ensures \result != null;
	 * 
	 * @pure
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Renvoie la valeur nutritive de cet {@code Aliment}.
	 * 
	 * @return la valeur nutritive de cet {@code Aliment}.
	 * 
	 * @ensures \result > 0;
	 * 
	 * @pure
	 */
	public int getValeurNutritive() {
		return valeurNutritive;
	}

	/**
	 * Fait la comparaison d'un {@code Aliment} avec un objet.
	 * 
	 * @param o l'objet à comparer.
	 * @return si l'objet est égale ou pas.
	 * 
	 * @ensures !(o instanceof Aliment) ==> !\result;
	 * @ensures (o instanceof Aliment) ==>
	 * 			(\result <==> (nom.equals(((Aliment) o).getNom())
	 *				&& (valeurNutritive == ((Aliment) o).getValeurNutritive())));
	 * @ensures \result ==> this.hashCode() == ((Aliment) o).hashCode()
	 * 
	 * @pure
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Aliment)) {
			return false;
		}

		Aliment aliment = (Aliment) o;

		return nom.equals(aliment.getNom()) && (valeurNutritive == aliment.getValeurNutritive());
	}

	/**
	 * Retourne le hash de l'{@code Aliment}.
	 * 
	 * @return le hashage de l'{@code Aliment}.
	 * 
	 * @pure
	 */
	@Override
	public int hashCode() {
		return (nom + valeurNutritive).hashCode();
	}

	/**
	 * Convertit l'{@code Aliment} en {@code String}.
	 * 
	 * @return l'{Aliment} converti en {@code String}.
	 * 
	 * @ensures \result != null;
	 * 
	 * @pure
	 */
	@Override
	public String toString() {
		return nom + " (" + valeurNutritive + " kcal)";
	}
}
